// const autoprefixer = require('autoprefixer')
// const tailwindcss = require('tailwindcss')
module.exports = {
  purge: { content: ['./public/**/*.html', './src/**/*.vue'] },
  plugins: {
    // require('postcss-import'),
    // require('tailwindcss'),
    // require('autoprefixer'),
    tailwindcss: {},
    autoprefixer: {},
  },
}
