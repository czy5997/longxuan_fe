// import info from './version.json'

module.exports = {
  runtimeCompiler: true,
  configureWebpack: {
    externals: {
      CKEDITOR: 'window.CKEDITOR'
    }
  },
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          modifyVars: {
            'primary-color': '#00AB94',
            'link-color': '#00AB94',
            'border-radius-base': '2px',
            'heading-color': '#282828',
            'text-color': '#686868',
            'text-color-secondary': '#A0A6AB',
            'input-placeholder-color': '#A0A6AB',
            'disabled-color': '#A0A6AB',
            'disabled-bg': '#F2F6F8',
            'border-color-base': '#EFF3F6',
            // 'select-border-color': '#D2D6D9',
            // 'btn-default-borde': '#D2D6D9',
            'dropdown-selected-color': '#F2F6F8',
            'select-item-selected-bg': '#F2F6F8',
            'select-item-active-bg': '#F2F6F8',
            'item-hover-bg': '#F2F6F8',
            'table-header-bg': '#F2F6F8',
            'border-color-split': '#F2F6F8',
            'pagination-item-bg-active': '#00AB94',
            'btn-disable-bg': '#A0A6AB',
            'btn-disable-color': '#fffff'
            // 'table-row-hover-bg':'#F2F6F8'
          },
          javascriptEnabled: true
        }
      }
    }
  },
  pluginOptions: {
    electronBuilder: {
      builderOptions: {
        appId: 'xiaoniujianxun',
        productName: '小牛建讯', //项目名，也是生成的安装文件名，即screen.exe
        copyright: '龙洵技术 Copyright © 2021', //版权信息
        artifactName: '资质服务商都在用的营销系统 小牛建讯 ${version}.${ext}',
        directories: {
          output: './dist_electron' //输出文件路径
        },
        // win: {
        //   //win相关配置
        //   icon: './build/icon.ico', // 图标256*256
        //   target: [
        //     {
        //       target: 'dir', // 不打包为安装程序
        //       arch: [
        //         // 'x64' //64位
        //         'ia32', //32位
        //       ],
        //     },
        //   ],
        // },
        win: {
          //  requestedExecutionLevel: 'requireAdministrator',  权限问题
          publish: [
            {
              provider: 'generic',
              url: 'http://cdn.public.xnjx666.com/download/' //更新服务器地址,可为空
            }
          ]
        },
        nsis: {
          oneClick: false, // 是否一键安装
          allowElevation: true, // 允许请求提升。 如果为false，则用户必须使用提升的权限重新启动安装程序。
          allowToChangeInstallationDirectory: false, // 允许修改安装目录
          installerIcon: './build/icons/icon.ico', // 安装图标
          uninstallerIcon: './build/icons/icon.ico', //卸载图标
          installerHeaderIcon: './build/icons/icon.ico', // 安装时头部图标
          createDesktopShortcut: true, // 创建桌面图标
          createStartMenuShortcut: true, // 创建开始菜单图标
          shortcutName: '小牛建讯', // 图标名称
          uninstallDisplayName: '小牛建讯${version}',
          include: './installer.nsh'
        }
      },
      chainWebpackMainProcess: config => {
        // Chain webpack config for electron main process only
      },
      chainWebpackRendererProcess: config => {
        // Chain webpack config for electron renderer process only
        // The following example will set IS_ELECTRON to true in your app
        config.plugin('define').tap(args => {
          args[0]['IS_ELECTRON'] = true
          return args
        })
      }
      // Use this to change the entrypoint of your app's main proces
    }
  }
}
// 13982045431 18482143241
