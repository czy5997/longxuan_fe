import Vue from 'vue'
import router from './router/index'
import { userModule } from './store/modules/user'
import { message, Modal } from 'ant-design-vue'
let whiteList = [] as any
import Cookie from 'js-cookie'

const $ = Vue.prototype

function getUserInfo(next: any, to: any, from: any) {
  // console.log('to', to)
  const userAgent = navigator.userAgent.toLowerCase()
  let userId = '' as any
  if (userAgent.indexOf(' electron/') > -1) {
    userId = localStorage.getItem('userId')
  } else {
    userId = Cookie.get('userId')
  }

  $.$httpRequester
    .get('member/get', { id: userId })
    .then((r: any) => {
      userModule.setUserInfo(r)
      const haveAuth = r.menuVOList.find(
        (e: any) => e.menuName === to.meta.title
      )
      // console.log(haveAuth, to.meta.needAuth)
      // next()
      if (haveAuth || !to.meta.needAuth) {
        document.title = to.meta.title || '小牛建讯'
        next({ query: { companyId: r.companyId } })
      } else {
        message.warning('您没有权限访问此页面')
        next(false)
      }
      //如果本地 存在 token 则 不允许直接跳转到 登录页面
      if (to.path === '/login') {
        // console.log('登录页面')
        if (userId) {
          next({
            path: from.path
          })
        } else {
          next()
        }
      }
      // r.m
    })
    .catch((err: any) => {
      if (err && location.hash.indexOf('login') < 0) {
        router.push('/login')
      } else {
        if (err.notLogin) {
          message.error('登录已过期，请重新登录')
          localStorage.getItem('loginInfo')
          localStorage.getItem('isRemember')
          localStorage.getItem('JSESSIONID')
          setTimeout(() => {
            location.reload()
          }, 800)
        }
        next(false)
      }
    })
}

router.beforeEach((to, from, next) => {
  let userId = '' as any
  const userAgent = navigator.userAgent.toLowerCase()
  if (userAgent.indexOf(' electron/') > -1) {
    userId = localStorage.getItem('userId')
  } else {
    userId = Cookie.get('userId')
  }

  // console.log(to.name)
  if (!userId) {
    whiteList = [
      'login',
      'registered',
      'share',
      'download',
      'vip',
      'receipt',
      'shipment',
      'qrMain',
      'sell'
    ]
  } else {
    // localStorage.setItem('userId', userId)
    whiteList = ['login', 'registered', 'share', 'download', 'vip']
  }
  Modal.destroyAll()
  if (
    !whiteList.find((e: any) => {
      return e === to.name
    })
  ) {
    const haveUserId = userId || !!userModule.userId
    if (!haveUserId) {
      // next()
      message.warning('您还未登录，请先登录')
      setTimeout(() => {
        next('/login')
      }, 1000)
    } else {
      if (haveUserId) {
        if (to.matched.length === 0) {
          localStorage.setItem('canNotFindPage', from.path)
          next('/cannotfind') // 判断此跳转路由的来源路由是否存在，存在的情况跳转到来源路由，否则跳转到404页面
        } else {
          getUserInfo(next, to, from)
          // next() // 如果匹配到正确跳转
        }
      }
    }
  } else {
    next()
  }
})
