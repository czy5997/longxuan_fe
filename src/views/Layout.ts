import { Layout, Menu } from 'ant-design-vue'
import { Component, Vue } from 'vue-property-decorator'
import { universalModule } from '@/store/modules/universal'
import { talentsModule } from '@/store/modules/talents'
import { shareholderModule } from '@/store/modules/shareholder'

import TopMenu from '../components/TopMenu.vue'
import TopTools from '../components/TopTools.vue'
import FeedBack from '@/components/Feedback.vue'
import bgImage from '@/assets/image/bg_2.png'
import backTop from '@/assets/image/back_top.png'
import SideBillboard from '@/components/SideBillboard.vue'
import CallPhone from '@/components/CallPhone.vue'
import leftSideTool from '@/components/LeftSideTool.vue'
import rootHeader from '@/components/RootHeader.vue'
import XiaoniuMenu from '@/components/XiaoniuMenu.vue'
@Component({
  components: {
    aLayout: Layout,
    aLayoutHeader: Layout.Header,
    aLayoutSider: Layout.Sider,
    aLayoutContent: Layout.Content,
    aLayoutFooter: Layout.Footer,
    aMenu: Menu,
    aMenuItem: Menu.Item,
    TopMenu,
    TopTools,
    FeedBack,
    SideBillboard,
    CallPhone,
    leftSideTool,
    rootHeader,
    XiaoniuMenu
  }
})
export default class RootLayout extends Vue {
  bgImage = bgImage
  backTop = backTop
  $refs!: {
    feedBackRef: any
    xiaoniuMenu: any
  }
  isHomePage = false
  right = 0
  tableFilter: any = null
  // 没有广告的页面
  notAdRouter = [
    'UCBasicInfo',
    'UCAttention',
    'UCResources',
    'changePassword',
    'userSetting',
    'department',
    'menuSetting',
    'announcementSetting',
    'adviceSetting',
    'EnterpriseFocusStatistics',
    'roleSetting',
    'quoteSetting',
    'organizationSetting',
    'operalogSetting'
  ]
  get showAd() {
    return !this.notAdRouter.includes(this.$route.name as any)
  }
  get historyList() {
    // console.log)

    const list = localStorage.getItem('history')
      ? JSON.parse(localStorage.getItem('history') as any)
      : []
    return universalModule.historyList.legnth
      ? universalModule.historyList
      : list
  }
  goToHistoryItem(item: any) {
    // console.log(item)
    if (item.name === this.$route.name) {
      return
    }
    // console.log(item)
    // const history = { name: item.name, title: item.title }
    // universalModule.changeHistoryListItem(history)
    this.$router.push({ name: item.name, query: item.query })
    this.$nextTick(() => {
      this.$refs.xiaoniuMenu.changeMenuItemSelect(item.name)
    })
  }
  removeThisItem(key: any) {
    universalModule.removeThisTab(key)
  }
  showFeedBackModal() {
    this.$refs.feedBackRef.showModal()
  }
  backToTop() {
    this.$nextTick(() => {
      const appDom = document.getElementById('lc') as any
      // console.log(appDom)
      //   document.body.scrollTop == 0 ? document.documentElement : document.body
      appDom.scrollTop = 0
    })
  }
  setHomePageStyle() {
    this.tableFilter = document.getElementById('tbFilter') as any
    this.right = this.tableFilter.getBoundingClientRect().right - 40 - 7
  }
  // @Watch('$route', { immediate: true, deep: true })
  // onPersonChanged(val: any, oldVal: any) {
  //   this.$nextTick(() => {
  //     this.isHomePage = val.name === 'homePage'
  //     // if (this.isHomePage) {
  //     //   const self = this as any
  //     //   this.setHomePageStyle()
  //     //   window.onresize = function() {
  //     //     self.setHomePageStyle()
  //     //   }
  //     // }
  //   })
  // }
  mounted() {
    const lastUserId = localStorage.getItem('lastUserId') as any
    const userId = localStorage.getItem('userId') as any
    if (lastUserId !== userId) {
      localStorage.removeItem('history')
      localStorage.removeItem('talentHistory')
      this.$nextTick(() => {
        const history = localStorage.getItem('history') || '[]'
        const historyList = JSON.parse(history)
        if (!historyList.length) {
          universalModule.changeHistoryListItem({
            title: '收货查询',
            name: this.$route.name
          })
        }
        universalModule.clearHistoryList()
        talentsModule.clearTalentHistoryList()
        shareholderModule.clearShareholderHistoryList()
      })
    }
  }
}
