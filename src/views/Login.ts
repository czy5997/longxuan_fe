import { JSEncrypt } from 'jsencrypt'
import {
  DatePicker,
  Input,
  FormModel,
  Button,
  Checkbox,
  Modal,
  Spin,
  Carousel
} from 'ant-design-vue'
import { Component, Vue, Watch } from 'vue-property-decorator'
import logo from '@/assets/svg/logo.svg'
import verifyCode from '@/components/verifyCode.vue'
import verifyImage from '../components/VerifyImage.vue'
import Cookie from 'js-cookie'
import { detailModule } from '@/store/modules/detail'
import BgImage from '@/assets/image/loginBg.png'
// import loginFrom from '@/assets/image/loginFrom.png'
import WechatLogin from './Bind/WechatLogin.vue'
import registerComponment from './Bind/Register.vue'
import hadPhone from './Bind/HadPhone.vue'
import passwordLogin from './Bind/PasswordLogin.vue'
import useVerifyCode from './Bind/UseVerifyCode.vue'
import userServiceText from '@/components/userServiceText.vue'
import defaultPng from '@/assets/image/login/bg_login.png'
import bgPng from '@/assets/image/login/bg_build.png'
@Component({
  components: {
    aDatePicker: DatePicker,
    aInput: Input,
    aButton: Button,
    aFormModel: FormModel,
    aCheckbox: Checkbox,
    aFormModelItem: FormModel.Item,
    aModal: Modal,
    aSpin: Spin,
    ACarousel: Carousel,
    verifyImage,
    verifyCode,
    WechatLogin,
    registerComponment,
    hadPhone,
    passwordLogin,
    useVerifyCode,
    userServiceText
  }
})
export default class Login extends Vue {
  $refs!: {
    loginForm: any
    codeForm: any
    verifyInput: any
  }
  wechatLogin = true // 使用微信登录
  isShowOtherLogin = false //是否展示其他登录
  isShowUserLogin = false
  isShowReg = false
  isUsePassword = false // 使用密码登录
  isUserVerifyCode = false // 使用验证码登录
  loginLoading = false
  accountShow = false
  passwordShow = false
  accountCodeShow = false
  isLogin = true // 判断是登录还是注册
  logo = logo
  bgColor = 'verificationBtn'
  loginStyle = 'loginStyle'
  BgImage = BgImage
  picList: any = []
  // loginFrom = loginFrom
  bgPng = bgPng
  bindBtns = false
  showRegister = false
  showPhoneBind = false
  wechatCode = ''
  isElectron = false
  bindHadAccount() {
    this.showPhoneBind = true
    this.bindBtns = false
    this.showRegister = false
  }
  bindNewAccount() {
    this.bindBtns = false
    this.showPhoneBind = false
    this.showRegister = true
  }

  showBindMobileBtns(code: string) {
    // console.log('code',code)
    this.wechatCode = code
    if (this.isElectron) {
      this.bindBtns = true
      this.wechatLogin = false
      this.isShowOtherLogin = true
    } else {
      this.bindBtns = false
      this.wechatLogin = false
      this.isShowOtherLogin = false
      this.bindHadAccount()
    }
  }
  showWechatLogin() {
    //  使用微信登录
    this.wechatLogin = true
    this.isShowOtherLogin = true
    this.isUsePassword = false
    this.isUserVerifyCode = false
    this.isShowOtherLogin = true
  }
  showVerifyLogin() {
    // 使用验证码登录
    this.wechatLogin = false
    this.showRegister = false
    this.showPhoneBind = false
    this.bindBtns = false
    this.isShowOtherLogin = false
    this.isUsePassword = false
    this.isUserVerifyCode = true
  }
  showPasswordLogin() {
    // 使用密码登录
    this.wechatLogin = false
    this.showRegister = false
    this.showPhoneBind = false
    this.bindBtns = false
    this.isShowOtherLogin = false
    this.isUsePassword = true
    this.isUserVerifyCode = false
  }
  userPasswordLogin() {
    this.isUsePassword = true
    this.accountShow = false
    this.passwordShow = false
    // this.resetLoginForm()
    this.$nextTick(() => {
      this.$refs.loginForm.clearValidate()
    })
  }

  userVerifyCodeLogin() {
    this.isUsePassword = false
    this.accountCodeShow = false
    // this.resetLoginForm()
    this.$nextTick(() => {
      this.$refs.codeForm.clearValidate()
    })
  }

  reallyPwd = ''
  rememberPwd = false
  agreement = false
  publicKey = ''
  alphaBet = [
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'J',
    'K',
    'L',
    'M',
    'N',
    'P',
    'Q',
    'R',
    'S',
    'T',
    'U',
    'V',
    'W',
    'X',
    'Y',
    'Z'
  ]
  formData: any = {
    account: '',
    password: '',
    verifyCode: '',
    isAgree: false
  }

  identifyCode = ''

  codeLoginData: any = {
    username: '',
    verifycode: ''
  }

  resetLoginForm() {
    this.formData = {
      account: '',
      password: '',
      verifyCode: ''
    }
    // this.codeLoginData = {
    //   username: '',
    //   verifycode: '',
    // }
  }

  onChange(e: any) {
    console.log(e)
  }

  changeAccount() {
    this.rememberPwd = false
    this.reallyPwd = ''
    this.formData.password = ''
    this.codeLoginData.verifycode = ''
    if (this.$refs.verifyInput) {
      this.$refs.verifyInput.clearValue()
    }
    this.clearCurrentCookie()
  }

  changePassword() {
    // console.log('pwd')
    this.rememberPwd = false
    this.clearCurrentCookie()
    this.reallyPwd = ''
  }

  clearCurrentCookie() {
    localStorage.removeItem('loginInfo')
    localStorage.removeItem('isRemember')
    localStorage.removeItem('JSESSIONID')
    // Cookie.remove('loginInfo')
    // Cookie.remove('isRemember')
    // // localStorage.clear()
    // Cookie.remove('JSESSIONID')
  }

  changePwdRememberStatus() {
    this.rememberPwd = !this.rememberPwd
  }

  agreeServiveList() {
    this.formData.isAgree = !this.formData.isAgree
  }

  verifyInputCode(code: any) {
    // console.log('code', code)
    this.codeLoginData.verifycode = code
  }

  wechatLoginSuccess(r: any) {
    // console.log('成功', r)
    localStorage.setItem('userId', r.id)
    localStorage.setItem(r.id.toString(), r.todayFirst ? '1' : '0')
    Cookie.set('JSESSIONID', r.tokeId, { expires: 7 })
    Cookie.set('userId', r.id, { expires: 7 })
    this.loginLoading = false
    setTimeout(() => {
      this.$router.push('./longna/receipt')
    }, 1000)
  }
  userLogin(params: any) {
    this.loginLoading = true
    this.$httpRequester
      .post('user/login', params, 'application/x-www-form-urlencoded')
      .then((r: any) => {
        localStorage.setItem('userId', r.id)
        localStorage.setItem(r.id.toString(), r.todayFirst ? '1' : '0')
        this.$message.success('登录成功')
        this.loginLoading = false
        Cookie.set('JSESSIONID', r.tokeId, { expires: 7 })
        Cookie.set('userId', r.id, { expires: 7 })
        if (params.rememberPwd) {
          // 记住密码
          localStorage.setItem('isRemember', params.rememberPwd ? '1' : '0')

          const userInfo = JSON.stringify(params)
          localStorage.setItem('loginInfo', userInfo)
        } else {
          // 不记住就删除cookie里面的
          localStorage.removeItem('isRemember')
          localStorage.removeItem('loginInfo')
        }
        clearInterval()
        // console.log('?')
        const fromUrl = this.$route.query.fromUrl as any
        if (fromUrl) {
          location.href = fromUrl
        } else {
          this.$router.push('./longna/receipt')
        }
        this.loginLoading = false
      })
      .catch((err: any) => {
        if (err.code === '2005') {
          detailModule.setExpriedModalShow(true)
        }
        this.loginLoading = false
        this.randomIdentifyCode()
      })
      .finally(() => {
        this.loginLoading = false
      })
  }

  // recordingFirstInfo() {
  //   const timeData = this.$moment().add(1, 'days').format('yyyy-MM-DD 00:00:00')
  //   const UTCDate = new Date(timeData)
  //   // console.log(new Date(timeData).toUTCString())
  //   Cookie.set('我离开', '1', { expires: UTCDate })
  // }
  goToFindPassword() {
    this.$router.push({
      name: 'registered',
      query: { isFindPwd: '1' }
    })
  }

  goToFindRegister() {
    this.$router.push({
      name: 'registered'
    })
    this.isLogin = false
    this.isShowReg = true
  }

  setPasswordRSA() {
    const encryptor = new JSEncrypt()
    // 设置公钥
    encryptor.setPublicKey(this.publicKey)
    // 加密数据
    return encryptor.encrypt(this.formData.password)
  }

  randomIdentifyCode() {
    const one = Math.floor(Math.random() * this.alphaBet.length)
    const two = Math.floor(Math.random() * this.alphaBet.length)
    const three = Math.floor(Math.random() * this.alphaBet.length)
    const four = Math.floor(Math.random() * this.alphaBet.length)
    // const five = Math.floor(Math.random() * 36)
    this.identifyCode =
      this.alphaBet[one] +
      this.alphaBet[two] +
      this.alphaBet[three] +
      this.alphaBet[four]
    // console.log(this.identifyCode)
  }

  // created() {
  //   //  const isRemember = Cookie.get('isRemember')
  //   const isRemember = localStorage.getItem('isRemember')
  //   // Cookie.get('isRemember')
  //   if (isRemember === '1') {
  //     const loginInfo = JSON.parse(localStorage.getItem('loginInfo') as any)
  //     this.formData.account = loginInfo.username
  //     this.formData.password = '**********'
  //     this.reallyPwd = loginInfo.password
  //     this.rememberPwd = true
  //   } else {
  //     localStorage.removeItem('loginInfo')
  //     localStorage.removeItem('isRemember')
  //     // Cookie.remove('loginInfo')
  //     // Cookie.remove('isRemember')
  //   }
  //   // localStorage.clear()
  //   Cookie.remove('JSESSIONID')
  // }

  showArgeement() {
    this.agreement = true
  }

  closeModal() {
    this.agreement = false
  }

  getPublicKey() {
    this.publicKey =
      'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoUSnVj2D7J0WECHHF/rrjbO/x692gNJUhtYD/6Te2NKOW+0Hjq5lkk6lz5zO80Bikv9aCk2bDTDkh2xuUugkP26d2ZYcQCKibc2+di1AX6M56IJlKdnyY5gXM+L5bv6urGfpLqf1X1XJNoJP+WAUliA5RtBI7S1f+4zwycmm86HQazPVsARQGX2Bm8fvxNMlYxRM5rSzrZjbCYwL70/KVpow5mXr/n9N+zk+WnkXWq8UlkBd2KS/bsQbXJ3my/fkk2nWOZvpEyBdekYykrv4iw9x/9v/4bloRJvy6+Dbb4TZo0TxsGEUHIhHJ8kQ5gKPih32TdCSPGyUwSgYcWLSHwIDAQAB'
    // const p = this.$httpRequester
    //   .post('security/getPublicKey')
    //   .then((r: any) => {
    //     this.publicKey = r
    //   })
    //   .catch((e: any) => {
    //     console.log(e)
    //     // if (e.serviceErr) {
    //     //     location.reload()
    //     // }
    //   })
    // return p
  }

  advData() {
    // advertisement/list
    this.$httpRequester
      .post('advertisement/releaseList', { advertisementType: 'LOGIN_PAGE' })
      .then((res: any) => {
        if (res.length) {
          res.forEach((item: any, index: number) => {
            this.picList.push({ key: index, value: item.url })
          })
        } else {
          this.picList.push({ key: 1, value: defaultPng })
        }
      })
  }

  passwordBlur(pwd: any) {
    if (pwd) {
      this.passwordShow = false
    } else {
      this.passwordShow = true
    }
  }

  @Watch('$route', { immediate: true, deep: true })
  onPersonChanged(val: any) {
    this.$nextTick(() => {
      console.log('当前的路由', val.name)
      if (val.name !== 'login') {
        this.isLogin = false
        this.isShowReg = true
      } else {
        this.isLogin = true
        this.isShowReg = false
      }
    })
  }

  mounted() {
    this.$nextTick(() => {
      this.advData()
    })
    this.getPublicKey()
    this.randomIdentifyCode()
    this.loginLoading = false
    const userAgent = navigator.userAgent.toLowerCase()
    if (userAgent.indexOf(' electron/') > -1) {
      // console.log('??')
      this.isElectron = true

      // Electron-specific code
    } else {
      this.isElectron = false
    }
  }
  beforeRouteEnter(to: any, from: any, next: any) {
    // localStorage.clear()
    next()
  }
}
