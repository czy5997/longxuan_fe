import {Component, Vue} from 'vue-property-decorator'
import moment from 'moment'
import {
    Input,
    Table,
    Button,
    Divider,
    Pagination,
    Drawer,
    Radio,
    Select,
    FormModel,
    Form,
    Modal,
    Switch,
    DatePicker
} from 'ant-design-vue'
// import 'ant-design-vue/dist/antd.css'
import Operate from '@/components/Operate.vue'
import {userModule} from '@/store/modules/user'
import plusIcon from '@/assets/image/ic_plus.png'


@Component({
    components: {
        aTable: Table,
        aInput: Input,
        aButton: Button,
        aDivider: Divider,
        aDrawer: Drawer,
        aForm: Form,
        aRangePicker: DatePicker.RangePicker,
        Operate,
        Pagination,
        aRadio: Radio,
        aRadioGroup: Radio.Group,
        aSelect: Select,
        aSelectOption: Select.Option,
        aFormModel: FormModel,
        aFormModelItem: FormModel.Item,
        aSwitch: Switch
    }
})
export default class Department extends Vue {
    $refs!: {
        departmentFormModel: any
        departmentPage: any
        drawer: any
    }
    plusIcon = plusIcon
    // 增加/编辑
    departmentInfo: any = {
        id: 0,
        departmentName: '',
        companyId: null,
        companyName: undefined,
        enable: true
    }
    //编辑判断部门名称是否改变
    originalDepartmentName = ''
    // 删
    deleteParams = {
        idsList: []
    }
    //公司是否能选择
    companyCanSelect = true
    companyListData: any = []
    createDateRange = []
    // 查
    searchParams: any = {
        pageNum: 1,
        pageSize: 10,
        departmentName: '',
        companyName: '',
        startDate: null,
        endDate: null
    }
    //table
    tableData: any = []
    columns = [
        {title: '部门 ID', dataIndex: 'id', align: 'left', width: '78px'},
        {title: '部门名称', dataIndex: 'departmentName', align: 'left', width: '198px'},
        {title: '所属公司', dataIndex: 'companyName', width: '278px', align: 'left'},
        {title: '部门人数', dataIndex: 'peopleNum', width: '78px', align: 'left'},
        {
            title: '创建时间',
            dataIndex: 'updateTime',
            width: '198px', align: 'left',
            //时间排序
            sorter: (a: any, b: any) =>
                new Date(a.updateTime.substring(0, 19).replace(/-/g, '/')).getTime() -
                new Date(b.updateTime.substring(0, 19).replace(/-/g, '/')).getTime()
        },
        {
            title: '状态',
            dataIndex: 'enable',
            width: '78px', align: 'left',
            scopedSlots: {customRender: 'enableSwitch'}
        },
        {
            title: '操作', width: '98px', align: 'left',
            scopedSlots: {customRender: 'operate'}
        }
    ]

    //查询是否重复部门名称
    repeatDepartmentName = {
        companyId: null,
        departmentName: ''
    }
    //弹出框
    rules = {
        departmentName: [
            {validator: this.validDepartName, required: true, trigger: 'blur'}
        ],
        companyName: [
            {required: true, message: '请选择所属公司', trigger: 'blur'}
        ],
        enable: [
            {required: true}
        ]
    }
    drawTitle = ''
    drawDisplay = false
    total = 0

    get btns() {
        return [
            {
                label: '编辑',
                type: 'link',
                click: this.showEditDrawer.bind(this)
            },
            {
                label: '删除',
                type: 'link',
                click: this.remove.bind(this)
            }
        ]
    }

    // 分页配置
    get pagination(): any {
        return {
            total: this.total,
            pageSize: this.searchParams.pageSize,
            current: this.searchParams.pageNum,
            onChange: (page: any, pageSize: any) => {
                //设置当前页面
                // console.log('页码', page)
                // console.log('每页条数', pageSize);
                this.searchParams.pageSize = pageSize
                this.searchParams.pageNum = page
                this.search()
            },
            showSizeChange: () => {
                this.search()
            },
            // showTotal: (total: number) => {
            //     // console.log("total:" + total)
            //     return `共${total}条数据`
            // },
            showQuickJumper: true
        }
    }

    // pageSize = 10
    // pageNum = 1

    //多选
    get rowSelection() {
        return {
            onChange: (selectedRowKeys: any) => {
                this.deleteParams.idsList = selectedRowKeys
            }
        }
    }

    // changePageSize(pagination: any) {
    //     console.log('changePageSize')
    //     // this.pageSize = pagination.pageSize
    //     this.search()
    // }

    validDepartName(rule: any, value: any, callback: any) {
        const validDepartNameTest = /^[\u4e00-\u9fa5]{2,10}$/
        if (value === '') {
            callback(new Error('请输入部门名称'))
        } else if (!validDepartNameTest.test(value)) {
            callback(new Error('请输入2-10个中文字符长度部门名称'))
        } else {
            //如果id 等于 0,说明是添加
            this.repeatDepartmentName.companyId = this.departmentInfo.companyId

            if (this.departmentInfo.id === 0 || this.departmentInfo.id == undefined) {
                this.repeatDepartmentName.departmentName = value
                this.$httpRequester
                    .get('department/repeat/check', this.repeatDepartmentName)
                    .then((r: any) => {
                        if (r) {
                            callback(new Error('您输入的部门名称已存在'))
                        } else {
                            callback()
                        }
                    })
            } else {
                //否则的话判断是否改变了
                // console.log('原始的',this.originalDepartmentName)
                // console.log('当前的',value)

                if (this.originalDepartmentName !== value) {
                    // console.log('我改变辣')
                    this.repeatDepartmentName.companyId = this.departmentInfo.companyId
                    this.repeatDepartmentName.departmentName = value
                    // console.log('编辑的查询是否重复部门名称',this.repeatDepartmentName)
                    this.$httpRequester
                        .get('department/repeat/check', this.repeatDepartmentName)
                        .then((r: any) => {
                            if (r) {
                                callback(new Error('您输入的部门名称已存在'))
                            } else {
                                callback()
                            }
                        })
                } else {
                    // console.log('我没变')
                    callback()
                }
            }
        }
    }

    mounted() {
        this.search()
        // console.log('用户角色等级', userModule.companyName)
        if (userModule.roleLevel == 1) {
            this.listAllCompany()
        } else if (userModule.roleLevel == 2) {
            const companyId = userModule.companyId
            // console.log('公司 ID',companyId)
            this.departmentInfo.companyId = companyId
            this.getCompany(companyId)
        }
    }

    handleCompanyChange(value: any) {
        this.departmentInfo.companyId = parseInt(value)
        // console.log('公司下拉框的变化',value)
    }

    getCompany(companyId: any) {
        this.$httpRequester.get('company/get?id=' + companyId).then((r: any) => {
            this.companyListData = [
                {id: r.id, companyId, companyName: r.companyName}
            ]
            this.departmentInfo.companyId = r.companyId
            this.departmentInfo.companyName = r.companyName
        })
    }

    listAllCompany() {
        this.$httpRequester.get('company/listAll').then((r: any) => {
            this.companyListData = r
        })
    }

    //重置
    reset() {
        // console.log("点击了重置", this.searchParams.departmentName)
        this.searchParams.departmentName = ''
        this.searchParams.companyName = ''
        this.searchParams.startDate = null
        this.searchParams.endDate = null
        this.createDateRange = []
    }

    //获取时间数组
    onChange(date: any, dateString: any) {
        this.searchParams.startDate = dateString[0] + ' 00:00:00'
        this.searchParams.endDate = dateString[1] + ' 23:59:59'
    }

    //不可选择未来时间
    disabledDate(current: any) {
        return current && current > moment().endOf('day')
    }

    //刷新
    refreshList() {
        this.search()
    }

    //添加用户
    showAddDrawer() {
        this.drawDisplay = true
        this.drawTitle = '添加部门'
        this.departmentInfo.departmentName = ''
        this.departmentInfo.id = 0
        this.departmentInfo.companyId = ''
        this.departmentInfo.companyName = undefined
        this.departmentInfo.enable = true
        this.companyCanSelect = true
        if (userModule.roleLevel === 2) {
            this.departmentInfo.companyId = userModule.companyId
            this.companyCanSelect = false
            this.departmentInfo.companyName = userModule.companyName
        }
    }

    //编辑
    showEditDrawer(item: any) {
        // console.log('编辑的部门的ID',item)
        this.drawTitle = '编辑部门'
        this.departmentInfo.companyId = item.companyId
        this.departmentInfo.companyName = item.companyName
        this.drawDisplay = true
        this.departmentInfo.departmentName = item.departmentName
        this.originalDepartmentName = item.departmentName
        this.departmentInfo.enable = item.enable
        this.departmentInfo.id = item.id
        this.companyCanSelect = false
    }

    //提交
    submitDepartmentInfo() {
        this.$refs.departmentFormModel.validate((valid: boolean) => {
            if (valid) {
                const departmentVal = {
                    companyId: this.departmentInfo.companyId,
                    departmentName: this.departmentInfo.departmentName,
                    enable: this.departmentInfo.enable,
                    id: this.departmentInfo.id
                }
                // console.log(' this.departmentInfo.', departmentVal)
                //console.log('当前信息的 id',this.departmentInfo.id)
                if (
                    this.departmentInfo.id == 0 ||
                    this.departmentInfo.id == undefined
                ) {
                    // this.departmentInfo.companyId=userModule.companyId?userModule.companyId:0
                    // 是否启用
                    // this.departmentInfo.enable = this.departmentInfo.enable === 0
                    this.$httpRequester
                        .post('department/add', departmentVal)
                        .then((r: any) => {
                            this.$message.success('添加成功')
                            this.drawDisplay = false
                            this.departmentInfo.id = 0
                            this.departmentInfo.departmentName = ''
                            this.departmentInfo.enable = true
                            this.search()
                        })
                } else {
                    this.$httpRequester
                        .put('department/update', this.departmentInfo)
                        .then(() => {
                            this.onClose()
                            this.departmentInfo.id = 0
                            this.departmentInfo.departmentName = ''
                            this.departmentInfo.enable = true
                            this.$message.success('更新成功')
                            this.search()
                        })
                }
            }
        })
    }

    onClose() {
        this.drawDisplay = false
    }

    //查询
    searchInfo() {
        const params = {
            departmentName: this.searchParams.departmentName,
            companyName: this.searchParams.companyName,
            startDate: this.searchParams.startDate,
            endDate: this.searchParams.endDate
        }
        this.$httpRequester.post('department/list', params).then((r: any) => {
            this.total = r.totalSize ? r.totalSize : 0
            this.tableData = r.data
        })
    }

    search() {
        this.$httpRequester
            .post('department/list', this.searchParams)
            .then((r: any) => {
                this.total = r.totalSize ? r.totalSize : 0
                this.tableData = r.data
            })
    }

    //删除多个
    removeAllSelect() {
        const departmentIds = this.deleteParams.idsList.toString()
        if (departmentIds.length <= 0) {
            this.$message.info('请选择要删除的部门')
        } else {
            Modal.confirm({
                title: '删除部门',
                content: '确定要删除所选部门吗?',
                okText: '确认',
                okType: 'primary',
                cancelText: '取消',
                onOk: () => {
                    this.$httpRequester
                        .delete(
                            'department/del',
                            {departmentIds},
                            'application/x-www-form-urlencoded'
                        )
                        .then((r: any) => {
                            this.$message.success('操作成功')
                            this.search()
                        })
                }
            })
        }
    }

    //删除
    remove(item: any) {
        Modal.confirm({
            title: '用户删除',
            content: '确定要删除该记录吗?',
            okText: '是',
            okType: 'danger',
            cancelText: '否',
            onOk: () => {
                this.$httpRequester
                    .delete('department/del?departmentIds=' + item.id)
                    .then(() => {
                        this.$message.success('删除成功')
                        this.search()
                    })
            }
        })
    }

    //状态按钮
    handleItemSwitch(switchValue: any, record: any) {
        this.$httpRequester
            .get('department/status/change', {id: record.id, enable: switchValue})
            .then(() => {
                record.enable = switchValue
                this.$message.success('状态更新成功')
                this.search()
            })
    }
}
