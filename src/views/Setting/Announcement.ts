import { Table, Button, Drawer, Select, FormModel, Modal } from 'ant-design-vue'
import { Component, Vue } from 'vue-property-decorator'
import Operate from '../../components/Operate.vue'
import { userModule } from '@/store/modules/user'
import { homeModule } from '@/store/modules/home'
import AddPolicy from './Components/AddPolicy.vue'
import Intermediary from './Components/Intermediary.vue'
import QueAndQns from './Components/QueAndQns.vue'
import System from './Components/System.vue'
import Notice from './Components/Notice.vue'
import Publicity from './Components/Publicity.vue'

@Component({
  components: {
    Operate,
    AddPolicy,
    QueAndQns,
    Intermediary,
    Notice,
    Publicity,
    aTable: Table,
    aButton: Button,
    aDrawer: Drawer,
    aFormModel: FormModel,
    aFormModelItem: FormModel.Item,
    aSelect: Select,
    aSelectOption: Select.Option
  }
})
export default class Announcement extends Vue {
  $refs!: {
    announcementForm: any
  }
  selectedRowKeys = []
  addComponent: any = System
  isShoComponentMap = new Map<string, any>([
    ['0', AddPolicy],
    ['1', System],
    ['3', QueAndQns],
    ['4', Publicity],
    ['5', Notice],
    ['6', Intermediary]
  ])
  isNoticeType = '1'
  detailsData: object = {}
  showLoading = false
  fileList = []
  files = []
  noticeText = ''
  /* uploadList = []
    defaultUploadList = []
    selectFile(item: any) {
        console.log('item', item)
        const size = item.file.size
        const MB = 1024 * 1024
        if (size / MB > 10) {
            this.$message.warning('文件过大!')
            return
        }
        if (item.fileList.length > 10) {
            this.$message.warning('最多只能上传十个文件')
            return
        }
        this.fileList = item.fileList
        // const files  = this.files;
        // const formData = new FormData();
        // files.forEach(d => {
        //   formData.append('file', d);
        // });
        // this.$httpRequester.post("upload/file",formData).then((res: any)=>{
        //   this.uploadList.push(res.url)
        //   console.log(this.uploadList)
        // })
    }
    beforeUpload(file: any) {
        this.files = [...this.files, file] as []
        return false
    }
    removeFile(file: any) {
        const index = this.files.findIndex((f: any) => f.uid === file.uid)
        // console.log(index)
        const newFileList = this.files.slice()
        newFileList.splice(index, 1)
        this.files = newFileList
    }*/
  tableData: any = []
  pageSize = 10
  pageNum = 1
  total = 0
  isEdit = false
  editDisabled = false
  userList: any = []
  get pagination(): any {
    return {
      total: this.total,
      pageSize: this.pageSize,
      currnet: this.pageNum,
      change: this.changePageNum.bind(this),
      showSizeChange: () => {
        this.changePageSize
      },
      showTotal: (total: number) => {
        return `共${total}条数据`
      },
      showSizeChanger: false
    }
  }

  isSuperAdmin = userModule.roleLevel === 1
  isSales = userModule.roleLevel === 4
  isCompanyAdimin = userModule.roleLevel === 2
  formModal: any = {
    title: '',
    enclosureUrl: '',
    content: '',
    sourceUrl: '',
    noticeType: null,
    userList: []
  }
  noticeType = '1'
  drawDisplay = false
  noticeId = -1
  drawTitle = '添加公告'
  noticeOption = [
    { label: '系统公告', value: '1' },
    { label: '政策文件', value: '0' },
    { label: '资质问答', value: '3' },
    { label: '公示', value: '4' },
    { label: '公告', value: '5' },
    { label: '中介池', value: '6' }
  ]
  statusText = ['', '未发布', '已发布', '撤销']
  columns = [
    { title: 'ID', dataIndex: 'noticeId', width: 80 },
    { title: '公告类型', dataIndex: 'noticeType', width: 120 },
    { title: '地区', dataIndex: 'areaNames' },
    { title: '公告标题', dataIndex: 'title', ellipsis: true },
    {
      title: '发布时间',
      dataIndex: 'sendTime',
      width: 200
    },
    {
      title: '状态',
      dataIndex: 'status',
      width: 120,
      scopedSlots: { customRender: 'statusText' }
    },
    {
      title: '操作',
      width: 250,
      scopedSlots: { customRender: 'operate' }
    }

    // {
    //   title: '附件',
    //   dataIndex: 'file',
    //   width: '10%',
    //   scopedSlots: { customRender: 'download' },
    // },
    // {
    //   title: '公告内容',
    //   dataIndex: 'noHtmlContent',
    //   ellipsis: true,
    //   scopedSlots: { customRender: 'detail' },
    // },
    /*  {
            title: '接收人',
            dataIndex: 'users',
            ellipsis: true,
          },
        */
  ]
  btns = [
    {
      label: '发布',
      type: 'link',
      icon: '',
      click: this.sendAnnouncement.bind(this)
    },
    {
      label: '编辑',
      type: 'link',
      icon: 'form',
      click: this.editAnnouncement.bind(this)
    },
    {
      label: '撤销',
      type: 'link',
      icon: 'redo',
      color: '#FA8F00',
      isShow: false,
      click: this.undoAnnouncement.bind(this)
      // isShow: true
    },
    {
      label: '删除',
      type: 'link',
      icon: 'delete',
      color: '#ff4d4f',
      click: this.removeAnnouncement.bind(this)
    }
  ]

  // noticeType = ['', '系统公告', '提示公告', '公示公告']
  //多选
  onSelectChange(selectedRowKeys: any) {
    this.selectedRowKeys = selectedRowKeys
  }

  changePageNum(pagination: any) {
    this.pageNum = pagination.current
    this.pageSize = pagination.pageSize
    this.getAnnouncementList()
  }
  changePageSize(pagination: any) {
    this.pageSize = pagination.pageSize
    this.getAnnouncementList()
  }
  addEditDrawer() {
    this.addComponent = System
    this.editDisabled = false
    this.drawTitle = '添加公告'
    this.isEdit = false
    this.isNoticeType = '1'
    this.drawDisplay = true
  }

  removeAllSeleclt() {
    const arr: any = []
    this.selectedRowKeys.forEach(item => {
      arr.push(parseInt(item))
    })
    const noticeIdArr = JSON.stringify(arr)
    const noticeIds = noticeIdArr.replace(/\[|]/g, '')
    Modal.confirm({
      title: '公告删除',
      content: '确定要删除所选公告吗?',
      okText: '是',
      okType: 'danger',
      cancelText: '否',
      onOk: () => {
        this.$httpRequester
          .delete(
            'notice',
            { noticeIds: noticeIds },
            'application/x-www-form-urlencoded'
          )
          .then(() => {
            this.$message.success('删除成功')
            this.getAnnouncementList()
          })
      }
    })
  }

  removeAnnouncement(item: any) {
    Modal.confirm({
      title: '公告删除',
      content: '确定要删除所选公告吗?',
      okText: '是',
      okType: 'danger',
      cancelText: '否',
      onOk: () => {
        this.$httpRequester
          .delete(
            'notice',
            { noticeIds: item.noticeId },
            'application/x-www-form-urlencoded'
          )
          .then(() => {
            this.$message.success('删除成功')
            this.getAnnouncementList()
          })
      }
    })
  }

  sendAnnouncement(item: any) {
    this.$httpRequester
      .get('notice/release', { id: item.noticeId })
      .then(() => {
        this.$message.success('发送成功')
        // homeModule.getReadMessageList()
        homeModule.getNotReadStatus()
        item.btns.forEach((e: any) => {
          if (e.label === '发布' || e.label === '编辑') {
            e.isShow = false
          } else {
            e.isShow = true
          }
        })
        this.getAnnouncementList()
      })
  }

  closeDrawer() {
    this.drawDisplay = false
  }

  beforeSubmit(data: any, judgeSubmitEdit: boolean) {
    if (judgeSubmitEdit) {
      const params = {
        noticeType: this.isNoticeType,
        ...data
      }
      this.$httpRequester.put('notice/update', params).then(() => {
        this.drawDisplay = false
        this.$message.success('操作成功')
        this.getAnnouncementList()
      })
    } else {
      const params = {
        noticeType: this.isNoticeType,
        ...data
      }
      this.$httpRequester
        .post('notice/add', params)
        .then(() => {
          this.drawDisplay = false
          this.$message.success('操作成功')
          this.getAnnouncementList()
        })
        .catch((res: any) => {
          this.noticeText = res.msg
        })
    }
  }

  editAnnouncement(item: any) {
    this.drawTitle = '编辑公告'
    this.isEdit = true
    this.editDisabled = true
    this.isNoticeType = item.noticeTypeCode
    this.noticeTypeChange(item.noticeTypeCode)
    this.drawDisplay = true
    this.detailsData = item
    // this.noticeId = item.noticeId
    // this.formModal.title = item.title
    // this.formModal.content = item.content
    // this.fileList = item.enclosureUrl
    //     ? item.enclosureUrl.split(',').map((e: any, i: any) => {
    //         return {
    //             uid: i,
    //             isHaveUpload: true,
    //             name: e,
    //         }
    //     })
    //     : []
    // this.formModal.sourceUrl = item.sourceUrl
    // // console.log(this.fileList)
    // // console.log(item.enclosureUrl.split(','))
    // // this.formModal.noticeType = item.noticeTypeCode
    // this.formModal.userList = item.userList.map((e: any) => {
    //     return {
    //         key: e.userId,
    //         label: e.userName,
    //     }
    // })
    // this.drawDisplay = true
  }

  undoAnnouncement(item: any) {
    Modal.confirm({
      title: '注意',
      icon: 'exclamation-circle',
      content: `确定撤销${item.title}这条公告吗?`,
      onOk: () => {
        const noticeId = item.noticeId
        this.$httpRequester.get('notice/revoke', { id: noticeId }).then(() => {
          this.$message.success('操作成功')
          this.getAnnouncementList()
        })
      }
    })
  }
  // refreshList() {
  //     this.getAnnouncementList()
  // }
  previewOrDownloadFile(file: any) {
    this.showLoading = true
    if (file.type === 'doc' || file.type === 'exc') {
      this.$httpRequester
        .download('upload/file/get', { path: file.url })
        .then((r: any) => {
          const blob = new Blob([r])
          const link = document.createElement('a')
          const firstIndex = file.url.indexOf('/')
          const lastIndex = file.url.lastIndexOf('.')
          const fileName = file.url.substring(firstIndex + 1, lastIndex)
          //  const time = this.$moment().format('YYYY-MM-DD HH:mm:ss')
          link.href = window.URL.createObjectURL(blob)
          link.download = `${fileName}${file.suffix}`
          link.setAttribute('target', 'view_window')
          // document.appendChild(link)
          link.click()
          window.URL.revokeObjectURL(link.href)
          // document.removeChild(link)
          this.showLoading = false
        })
        .catch(() => {
          this.showLoading = false
        })
    } else {
      this.showLoading = false
      window.open(`${apiUrl}upload/file/get?path=${file.url}`)
    }
  }
  /*    submitEdit() {
        // console.log(this.formModal)
        let userList: any = []
        const findIndex = this.formModal.userList.findIndex(
            (e: any) => e.value === '-1'
        )
        if (findIndex !== -1) {
            // 选择全体的时候
            this.userList.forEach((e: any) => {
                if (e.value !== '-1') {
                    userList.push(e.value)
                }
            })
        } else {
            userList = this.formModal.userList.map((e: any) => {
                return e.key
            })
        }
        if (this.noticeId === -1) {
            const params = {
                title: this.formModal.title,
                content: this.formModal.content,
                enclosureUrl: this.formModal.enclosureUrl,
                sourceUrl: this.formModal.sourceUrl,
                noticeType: this.formModal.noticeType,
                userList,
            }
            this.$httpRequester.post('notice/add', params).then(() => {
                this.$message.success('操作成功')
                // this.closeDrawer()
            })
        } else {
            const params = {
                title: this.formModal.title,
                content: this.formModal.content,
                enclosureUrl: this.formModal.enclosureUrl,
                sourceUrl: this.formModal.sourceUrl,
                noticeType: this.formModal.noticeType,
                userList,
                id: this.noticeId,
            }
            this.$httpRequester.put('notice/update', params).then(() => {
                this.$message.success('操作成功')
                // this.closeDrawer()
            })
        }
        // console.log(this.formModal)
    }*/
  getAnnouncementList() {
    const params = {
      pageNum: this.pageNum,
      pageSize: this.pageSize
    }
    this.$httpRequester.post('notice/selManagerList', params).then((r: any) => {
      // console.log(r)
      r.data.forEach((e: any) => {
        if (e.noticeTypeCode === '2') {
          this.btns = [
            {
              label: '发布',
              type: 'link',
              icon: '',
              click: this.sendAnnouncement.bind(this)
            },
            {
              label: '撤销',
              type: 'link',
              icon: 'redo',
              color: '#FA8F00',
              isShow: false,
              click: this.undoAnnouncement.bind(this)
              // isShow: true
            },
            {
              label: '删除',
              type: 'link',
              icon: 'delete',
              color: '#ff4d4f',
              click: this.removeAnnouncement.bind(this)
            }
          ]
        } else {
          this.btns = [
            {
              label: '发布',
              type: 'link',
              icon: '',
              click: this.sendAnnouncement.bind(this)
            },
            {
              label: '编辑',
              type: 'link',
              icon: 'form',
              click: this.editAnnouncement.bind(this)
            },
            {
              label: '撤销',
              type: 'link',
              icon: 'redo',
              color: '#FA8F00',
              isShow: false,
              click: this.undoAnnouncement.bind(this)
              // isShow: true
            },
            {
              label: '删除',
              type: 'link',
              icon: 'delete',
              color: '#ff4d4f',
              click: this.removeAnnouncement.bind(this)
            }
          ]
        }
        e.btns = []
        this.btns.forEach((bt: any) => {
          if (
            bt.label === '发布' ||
            bt.label === '编辑' ||
            bt.label === '删除'
          ) {
            e.btns.push({
              ...bt,
              isShow: e.statusCode === '11' ? false : true
            })
          } else {
            e.btns.push({
              ...bt,
              isShow: e.statusCode === '11' ? true : false
            })
          }
        })
        e.areaNames = e.areaNames ? e.areaNames.toString() : ''
        e.sendTime = e.sendTime
          ? this.$moment(e.sendTime).format('YYYY-MM-DD')
          : ''
        e.statusText = this.statusText[e.status]
        e.noticeIdText =
          e.noticeId.length > 15
            ? e.noticeId.substring(15, e.noticeId.length)
            : e.noticeId
        e.noHtmlContent = e.content
        // e.users = e.userList
        //   .map((u: any) => {
        //     return u.userName
        //   })
        //   .join(',')

        // e.downloadFiles = []
        // e.files = e.enclosureUrl ? e.enclosureUrl.split(',') : []
        /*   e.files.forEach((f: any) => {
                     const fileNameIndex = f.lastIndexOf('_')
                     let fileName = ''
                     if (fileNameIndex > 0) {
                       fileName = f.substr(0, fileNameIndex)
                     } else {
                       fileName = f
                     }
                     const sufffixIndex = fileName.lastIndexOf('.')
                     const suffix = fileName.substr(sufffixIndex, fileName.length)
                     const image = imageTypes.find((image: any) => image === suffix)
                     const doc = docTypes.find((doc: any) => doc === suffix)
                     const exc = excel.find((ex: any) => ex === suffix)
                     if (image) {
                       e.downloadFiles.push({
                         type: 'image',
                         icon: 'file-image',
                         suffix,
                         title: '图片',
                         url: f,
                       })
                     } else if (doc) {
                       e.downloadFiles.push({
                         type: 'doc',
                         icon: 'file-word',
                         suffix,
                         title: '文档',
                         url: f,
                       })
                     } else if (exc) {
                       e.downloadFiles.push({
                         type: 'exc',
                         icon: 'file-excel',
                         suffix,
                         title: '表格',
                         url: f,
                       })
                     }
                   })*/
      })
      this.tableData = r.data
      this.total = r.totalSize
    })
  }

  getStaffList() {
    const params = {
      companyId: userModule.companyId
    }
    this.$httpRequester.get('member/listAll', params).then((r: any) => {
      // console.log(r)
      this.userList = r.map((e: any) => {
        return { label: e.memberName, value: e.id, disabled: false }
      })
      this.userList.unshift({
        label: '全体公司成员',
        value: '-1',
        disabled: false
      })
      // this.memberData = r;
    })
  }
  selectStaff(value: any) {
    if (value.findIndex((e: any) => e.key === '-1') !== -1) {
      this.formModal.userList = [
        {
          label: '全体公司成员',
          value: '-1'
        }
      ]
      this.userList.forEach((e: any) => {
        if (e.value !== '-1') {
          e.disabled = true
        }
      })
    } else {
      this.formModal.userList = value
      this.userList = this.userList.map((e: any) => {
        return {
          label: e.label,
          value: e.value,
          disabled: false
        }
      })
    }
  }
  noticeTypeChange(value: any) {
    this.isNoticeType = value
    this.addComponent = this.isShoComponentMap.get(value)
  }
  mounted() {
    this.getAnnouncementList()
    this.$nextTick(() => {
      if (this.isCompanyAdimin) {
        // 公司管理员才能发布
        this.getStaffList()
      }
    })
  }
}
