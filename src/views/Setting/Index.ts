import { Component, Vue } from 'vue-property-decorator'
import { userModule } from '@/store/modules/user'

@Component({})
export default class Index extends Vue {
  menuItems=userModule.settingMenu  || []
  // menuItems = [
  //     { title: '用户管理', path: '/user', name: 'userSetting', isSelect: false },
  //     { title: '组织管理', path: '/organization', name: 'organizationSetting', isSelect: false },
  //     { title: '角色管理', path: '/role', name: 'roleSetting', isSelect: false },
  //     // { title: '菜单管理', path: '/menu', name: 'menuSetting', isSelect: false },
  //     { title: '操作日志', path: '/operalog', name: 'operalogSetting', isSelect: false },
  //     { title: '公告管理', path: '/announcement', name: 'announcementSetting', isSelect: false },
  //     { title: '企业股权报价', path: '/quote', name: 'quoteSetting', isSelect: false },
  //     { title: '意见反馈', path: '/advice', name: 'adviceSetting', isSelect: false },
  // ]
  // menuItems = userModule.subMenus || []
  goToMenu(item: any) {
    if (item.isSelect) {
      return
    }
    this.$router.push({ name: item.name })
    item.isSelect = true
    this.menuItems.forEach((e: any) => {
      if (e.name !== item.name) {
        e.isSelect = false
      }
    })
    // userModule.setSubMenus(this.menuItems)
  }
  mounted() {
    // debugger
    // const fatherIndex = this.$route.matched.length - 2
    // const fatherRoute = this.$route.matched[fatherIndex]
    // const currentMenu = userModule.fatherMenuList.find((e: any) => {
    //   return fatherRoute.name === e.name
    // })
    // if (currentMenu) {
    //   this.menuItems = currentMenu.children
      this.menuItems.forEach((v: any) => {
        if (v.name === this.$route.name) {
          v.isSelect = true
        }
      })
    // }
    // userModule.setSubMenus(vm.menuItems)
  }
/*  beforeRouteEnter(to: any, from: any, next: any) {
    console.log(to.matched)
    next((vm: any) => {
      const fatherIndex = to.matched.length - 2
      const fatherRoute = to.matched[fatherIndex]
      const currentMenu = userModule.fatherMenuList.find(
        (e: any) => fatherRoute.name === e.name
      )
      if (currentMenu) {
        vm.menuItems = currentMenu.children
        vm.menuItems.forEach((v: any) => {
          if (v.name === to.name) {
            v.isSelect = true
          }
        })
      }
      // userModule.setSubMenus(vm.menuItems)
      // console.log(currentMenu)
      // next({ name: currentMenu.[0].name })
    })
  }*/
}
