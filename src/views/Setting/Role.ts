import { Component, Vue } from 'vue-property-decorator'
import {
  Button,
  Input,
  Table,
  Switch,
  Drawer,
  Select,
  FormModel,
  Radio,
  Modal
} from 'ant-design-vue'
import Operate from '../../components/Operate.vue'
import { userModule } from '@/store/modules/user'

@Component({
  components: {
    aButton: Button,
    aInput: Input,
    aTable: Table,
    aTextarea: Input.TextArea,
    aSwitch: Switch,
    aDrawer: Drawer,
    aSelect: Select,
    aSelectOption: Select.Option,
    aFormModel: FormModel,
    aFormModelItem: FormModel.Item,
    aRadio: Radio,
    aRadioGroup: Radio.Group,
    Operate,
    userModule: userModule
  }
})
export default class Role extends Vue {
  btnPermDisplay = false
  isEdit = false
  tableData = []
  drawerTitle = '添加角色'
  drawerDisplay = false
  roleList = [] as any
  menuList = []
  columns = [
    { title: 'ID', dataIndex: 'id', width: 80 },
    { title: '角色名称', dataIndex: 'roleName', width: 150 },
    { title: '角色描述', dataIndex: 'description', ellipsis: true },
    { title: '创建时间', dataIndex: 'createTime' },
    {
      title: '启用',
      dataIndex: 'isEnable',
      width: 100,
      scopedSlots: { customRender: 'enableSwitch' }
    },
    {
      title: '操作',
      width: 170,
      scopedSlots: { customRender: 'operate' }
    }
  ]
  editModel: any = {
    id: 0,
    roleName: '',
    roleLevelName: '',
    enable: false,
    roleLevel: 0,
    roleId: 0,
    menuVOList: [],
    description: ''
  }
  btns = [
    {
      label: '编辑',
      type: 'link',
      icon: 'form',
      click: this.editRole.bind(this)
    },
    {
      label: '删除',
      type: 'link',
      icon: 'delete',
      color: '#909399',
      click: this.removeRole.bind(this)
    }
  ]
  deleteParams = {
    idsList: []
  }

  refreshList() {
    this.getRolesList()
  }

  removeAllSeleclt() {
    Modal.confirm({
      title: '角色删除',
      content: '确定要删除所选记录吗?',
      okText: '是',
      okType: 'danger',
      cancelText: '否',
      onOk: () => {
        this.$httpRequester
          .post('role/delete', this.deleteParams.idsList)
          .then((r: any) => {
            this.$message.success('删除成功')
            this.getRolesList()
          })
      }
    })
  }

  roleDrawerShow() {
    this.drawerDisplay = true
  }

  addRoleDrawer() {
    this.drawerTitle = '新增角色'
    this.drawerDisplay = true
    this.editModel.id = 0
    this.roleList.forEach((e: any) => {
      if (userModule.roleId == e.id) {
        this.editModel.roleLevel = e.level
        return
      }
    })
    this.editModel.roleName = ''
    this.editModel.enable = false
    this.editModel.menuVOList = []
    this.editModel.description = ''
    this.getMenuList(userModule.roleId)
    this.roleDrawerShow()
  }

  closeDrawer() {
    this.drawerDisplay = false
    this.getRolesList()
  }

  getRolesList() {
    this.$httpRequester.get('role/list').then((r: any) => {
      r.data.forEach((e: any) => {
        e.isEnable = e.enable
      })
      this.tableData = r.data
    })
  }

  submitRoleEdit() {
    if (this.editModel.id === 0) {
      const params = {
        ...this.editModel
      }
      params.menuVOList = params.menuVOList.map((e: any) => {
        return { menuName: e.label, id: e.key }
      })
      // params.enable = params.enable
      this.$httpRequester.post('role/add', params).then(() => {
        this.$message.success('操作成功')
        this.closeDrawer()
      })
    } else {
      const params = {
        ...this.editModel
      }
      this.roleList.forEach((e: any) => {
        if (e.id == this.editModel.roleId) {
          params.roleLevel = e.level
        }
      })
      params.menuVOList = params.menuVOList.map((e: any) => {
        return { menuName: e.label, id: e.key }
      })
      // params.enable = params.enable
      this.$httpRequester.post('role/update', params).then(() => {
        this.$message.success('操作成功')
        this.closeDrawer()
      })
    }
  }

  getRoleEnumList() {
    this.roleList = [
      { roleLevelName: '超级管理员', level: 1, id: 1 },
      { roleLevelName: '公司管理员', level: 2, id: 2 },
      { roleLevelName: '销售主管', level: 3, id: 3 },
      { roleLevelName: '销售人员', level: 4, id: 4 }
    ]
    // this.$httpRequester
    //   .get('enumClass/selList?className=RoleEnum')
    //   .then((r: any) => {
    //     this.roleList = r
    //   })
  }

  getMenuList(roleId: any) {
    this.$httpRequester.get('menu/get?roleId=' + roleId).then((r: any) => {
      this.menuList = r.map((e: any) => {
        return {
          value: e.id,
          label: e.menuName
        }
      })
    })
  }

  handleItemSwitch(item: any, record: any) {
    if (record.id > 0) {
      this.$httpRequester
        .get('role/status/change?id=' + record.id + '&enable=' + item)
        .then((r: any) => {
          this.$message.success('状态更新成功')
          this.getRolesList()
        })
    }
  }

  editRole(item: any) {
    this.drawerTitle = '编辑角色'
    this.drawerDisplay = true
    this.editModel.id = item.id
    this.editModel.roleLevel = item.roleLevel
    this.editModel.roleName = item.roleName
    this.editModel.enable = item.enable
    this.editModel.menuVOList = item.menuVOList.map((e: any) => {
      return { label: e.menuName, key: e.id }
    })
    this.editModel.description = item.description
    this.roleList.forEach((e: any) => {
      if (item.roleLevel == e.level) {
        this.editModel.roleLevelName = e.roleLevelName
        return
      }
    })
    this.getMenuList(item.id)
  }

  removeRole(item: any) {
    Modal.confirm({
      title: '用户删除',
      content: '确定要删除该记录吗?',
      okText: '是',
      okType: 'danger',
      cancelText: '否',
      onOk: () => {
        this.$httpRequester.post('role/delete', [item.id]).then((r: any) => {
          this.$message.success('删除成功')
          this.getRolesList()
        })
      }
    })
  }

  get rowSelection() {
    return {
      onChange: (selectedRowKeys: any, selectedRows: any) => {
        this.deleteParams.idsList = selectedRowKeys
      }
    }
  }

  handleRoleChange(value: any) {
    this.roleList.forEach((e: any) => {
      if (value == e.roleLevelName) {
        this.editModel.roleLevel = e.level
        return
      }
    })
  }
  mounted() {
    this.getRoleEnumList()
    this.getRolesList()
    if (userModule.roleId == 1) {
      this.editModel.roleId = userModule.roleId
      this.btnPermDisplay = true
    } else if (userModule.roleId == 2) {
      this.btnPermDisplay = false
    }
  }
}
