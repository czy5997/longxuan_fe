import { Component, Vue } from 'vue-property-decorator'
// import 'ant-design-vue/dist/antd.css'
import Operate from '@/components/Operate.vue'
import { Button, Input, Table, Drawer } from 'ant-design-vue'
@Component({
  components: {
    aButton: Button,
    aInput: Input,
    aTable: Table,
    aDrawer: Drawer,
    Operate
  }
})
export default class EnterpriseFocusStatistics extends Vue {
  //table
  tableData: any = []
  total = 0
  //搜索
  searchParams: any = {
    page: 1,
    rows: 10,
    companyName: '',
    sortField: '',
    sortOrder: ''
  }
  //列
  columns = [
    { title: 'ID', dataIndex: 'enterpriseId' },
    { title: '企业名称', dataIndex: 'enterpriseName' },
    { title: '法定代表人', dataIndex: 'legalPerson', width: 120 },
    { title: '注册资本(元)', dataIndex: 'registeredCapital', width: 120 },
    { title: '成立时间', dataIndex: 'establishmentDate' },
    { title: '关注次数', dataIndex: 'attentionNumber', sorter: true },
    {
      title: '最后关注时间',
      dataIndex: 'updateTime',
      sorter: true
    },
    { title: '最后关注企业', dataIndex: 'attentionCompanyName' },
    { title: '操作', scopedSlots: { customRender: 'operate' } }
  ]
  // 列表分页配置
  get pagination(): any {
    return {
      showSizeChanger: false,
      pageSizeOptions: ['10', '20', '30', '40', '50'], //每页中显示的数据
      total: this.total,
      pageSize: this.searchParams.rows,
      current: this.searchParams.page
    }
  }
  listChange() {
    this.$httpRequester
      .get('attention/queryStatistics', this.searchParams)
      .then((r: any) => {
        this.total = r.total
        this.tableData = r.records
        let n = 1
        r.records.map((item: any) => {
          item.id = n++
        })
      })
  }
  //搜索
  listDetails() {
    this.$httpRequester
      .get('attention/queryUserDetails', this.pageDetailsInfo)
      .then((r: any) => {
        this.totalDataDetails = r.total
        this.tableDataDetails = r.records
        let n = 1
        r.records.map((item: any) => {
          item.id = n++
        })
      })
  }

  //列表分页
  handleTableChange(pagination: any, filters: any, sorter: any) {
    //排序信息
    this.searchParams.sortField = sorter.field
    this.searchParams.sortOrder = sorter.order
    this.searchParams.rows = pagination.pageSize
    this.searchParams.page = pagination.current
    this.pagination.current = pagination.current
    this.pagination.pageSize = pagination.pageSize
    this.listChange()
  }
  //详情table
  drawDisplay = false
  columnsDetails = [
    { title: '序号', dataIndex: 'id' },
    { title: '关注用户', dataIndex: 'memberName' },
    { title: '联系电话', dataIndex: 'userAccount' },
    { title: '企业名称', dataIndex: 'attentionCompanyName' },
    { title: '关注时间', dataIndex: 'createTime' }
  ]
  //详情总条数
  totalDataDetails = 0
  tableDataDetails: any = []
  //详情公司名称
  enterpriseNameDetails = ''
  //企业关注详情
  pageDetailsInfo: any = {
    page: 1,
    rows: 10,
    enterpriseId: ''
  }
  //详情分页配置
  get paginationDetails(): any {
    return {
      showSizeChanger: false,
      pageSizeOptions: ['10', '20', '30', '40', '50'], //每页中显示的数据
      total: this.totalDataDetails,
      pageSize: this.pageDetailsInfo.rows,
      current: this.pageDetailsInfo.page
    }
  }
  //详情分页
  handleTablePaginationDetails(pageDetails: any) {
    // console.log('pagination',pageDetails)
    this.pageDetailsInfo.rows = pageDetails.pageSize
    this.pageDetailsInfo.page = pageDetails.current
    this.paginationDetails.current = pageDetails.current
    this.paginationDetails.pageSize = pageDetails.pageSize
    this.listDetails()
  }

  get btns() {
    return [
      {
        label: '详情',
        type: 'link',
        icon: 'form',
        click: this.details.bind(this)
      }
    ]
  }
  //搜索
  searchInfo() {
    // console.log(this.searchParams.companyName)
    this.listChange()
  }
  //重置
  reset() {
    this.searchParams.companyName = ''
  }
  //详情
  details(item: any) {
    // console.log('详情',item)
    this.drawDisplay = true
    this.enterpriseNameDetails = item.enterpriseName
    this.pageDetailsInfo.enterpriseId = item.enterpriseId
    this.listDetails()
  }
  //关闭
  onClose() {
    this.drawDisplay = false
  }
  mounted() {
    this.listChange()
  }
}
