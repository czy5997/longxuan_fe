import {
  Table,
  Select,
  FormModel,
  Input,
  DatePicker,
  Drawer,
  Modal,
  Button,
  Switch
} from 'ant-design-vue'
import { Component, Vue } from 'vue-property-decorator'
import TreeCheckboxSingleSelect from '@/components/TreeCheckboxSingleSelect.vue'
import { userModule } from '@/store/modules/user'

@Component({
  components: {
    aTable: Table,
    aDraw: Drawer,
    aModal: Modal,
    aInput: Input,
    aButton: Button,
    aDrawer: Drawer,
    aDatePicker: DatePicker,
    aRangePicker: DatePicker.RangePicker,
    aSelect: Select,
    aSelectOption: Select.Option,
    aFormModel: FormModel,
    aFormModelItem: FormModel.Item,
    aSwitch: Switch,
    TreeCheckboxSingleSelect
  }
})
export default class RecommendQualifications extends Vue {
  form = {
    id: null,
    qualificationId: null,
    enable: null as any,
    qualificationType: null,
    qualification: '',
    sortIndex: null
  }
  qualificationList = [
    { value: 1, label: '勘察企业' },
    { value: 2, label: '设计企业' },
    { value: 3, label: '建筑业企业' },
    { value: 4, label: '监理企业' },
    { value: 5, label: '设计与施工一体化企业' },
    { value: 6, label: '造价咨询企业' }
  ]

  qualificationListMap = new Map<string, any>([
    ['勘察企业', 1],
    ['设计企业', 2],
    ['建筑业企业', 3],
    ['监理企业', 4],
    ['设计与施工一体化企业', 5],
    ['造价咨询企业', 6]
  ])

  columns = [
    { title: '编号', dataIndex: 'id', width: 70 },
    {
      title: '资质类别',
      dataIndex: 'qualificationCategory'
    },
    {
      title: '资质名称及等级',
      dataIndex: 'qualificationNameLevel',
      width: 300
    },
    {
      title: '排序',
      dataIndex: 'sort'
      // scopedSlots: { customRender: 'phoneList' },
    },
    {
      title: '创建时间',
      dataIndex: 'createTime'
      // scopedSlots: {customRender: 'followStatus'},
    },
    {
      title: '状态',
      dataIndex: 'enableSwitch',
      scopedSlots: { customRender: 'enableSwitch' },
      width: 90
    },
    {
      title: '操作',
      width: 150,
      scopedSlots: { customRender: 'operate' }
    }
  ]
  tableData = [] as any
  formModal = {}
  visible = false

  validSortIndex(rule: any, value: any, callback: any) {
    // console.log('2', this.form.sortIndex)
    if (this.form.sortIndex) {
      callback()
    } else {
      callback(new Error('请输入排序'))
    }
  }

  validQualificationType(rule: any, value: any, callback: any) {
    // console.log('22', this.form.qualificationType)
    if (this.form.qualificationType) {
      callback()
    } else {
      callback(new Error('请选择资质类别'))
    }
  }

  validQualification(rule: any, value: any, callback: any) {
    // console.log('222', this.form.qualification)
    if (this.form.qualification) {
      callback()
    } else {
      callback(new Error('资质名称及等级不能为空!'))
    }
  }

  rules = {
    sortIndex: [
      { required: true, validator: this.validSortIndex, trigger: 'blur' }
    ],
    qualification: [
      { required: true, validator: this.validQualification, trigger: 'change' }
    ],
    qualificationType: [
      {
        required: true,
        validator: this.validQualificationType,
        trigger: 'blur'
      }
    ]
    // qualificationId: [
    //     {required: true, validator: this.validQualificationTypeId, trigger: 'change',}
    // ],
  }

  listData() {
    this.$httpRequester
      .get('recommend/qualification', { pageNum: 1, pageSize: 20 })
      .then((res: any) => {
        res.records.forEach((item: any) => {
          item.createTime = this.$moment(item.createTime).format('yyyy-MM-DD')
          item.type = this.qualificationListMap.get(item.qualificationCategory)
          item.enable = item.enable !== 0
        })
        this.tableData = res.records
        // console.log(this.tableData)
      })
  }

  //状态按钮
  handleItemSwitch(switchValue: any, record: any) {
    const id = record.id
    this.$httpRequester.put('recommend/qualification/enable', id).then(() => {
      record.enable = switchValue
      this.$message.success('状态更新成功')
      this.listData()
    })
  }
  getQualificationType() {
    return document.getElementById('qualificationType')
  }

  edit(item: any, record: any) {
    if (record.enable) {
      this.$message.error('启用状态下不能编辑')
    } else {
      this.visible = true
      this.form.qualificationType = item.type
      this.form.qualificationId = item.qualificationId
      this.form.qualification = item.qualificationNameLevel
      this.form.sortIndex = item.sort
      this.form.id = item.id
      this.form.enable = item.enable ? 1 : 0
      if (this.form.qualificationType === 3) {
        this.qualificationShow = false
      } else {
        this.qualificationShow = true
      }
    }
  }

  onClose() {
    this.visible = false
  }

  submit() {
    const feedbackForm = this.$refs.recommendQualificationsForm as any
    feedbackForm.validate((valid: boolean) => {
      if (valid) {
        const params = {
          enable: this.form.enable,
          id: this.form.id,
          qualificationCategory: this.form.qualificationType,
          qualificationId: this.form.qualificationId,
          qualificationNameLevel: this.form.qualification,
          sort: this.form.sortIndex
        }
        this.$httpRequester.put('recommend/qualification', params).then(() => {
          this.$message.success('更新成功')
          this.listData()
          this.visible = false
        })
      }
    })
  }
  qualificationShow = true
  qualificationTypeChange(val: any) {
    if (val === 3) {
      this.qualificationShow = false
      this.form.qualification = ''
      this.form.qualificationId = null
    } else {
      this.qualificationShow = true
      this.form.qualificationId = null
      this.form.qualification = ''
    }
  }
  qualificationIdChange(val: any) {
    const currentMenu = this.getQualificationTypeData.find((e: any) => {
      if (val === e.text) {
        return e.id
      }
    })
    this.form.qualificationId = currentMenu.id
  }
  getQualificationTypeData = [] as any
  getQualificationTypeList() {
    this.$httpRequester
      .get('qualification/selQualificationAll', {})
      .then((res: any) => {
        const arr = [] as any
        res.forEach((item: any) => {
          item.qualificationLevels.forEach((d: any) => {
            const arrItem = { id: d.id, text: item.qualificationName + d.level }
            arr.push(arrItem)
          })
        })
        this.getQualificationTypeData = arr
      })
  }

  mounted() {
    this.getQualificationTypeList()

    this.listData()
  }
}
