import {
  Table,
  Select,
  FormModel,
  Upload,
  Input,
  DatePicker,
  Drawer,
  Modal,
  Button
} from 'ant-design-vue'
import Operate from '@/components/Operate.vue'
import { Component, Vue, Watch } from 'vue-property-decorator'
import TinyEdit from '@/components/TinyEdit.vue'
import { monitorEventLoopDelay } from 'perf_hooks'
import lookupFiles = Mocha.utils.lookupFiles

@Component({
  components: {
    aTable: Table,
    aDraw: Drawer,
    aModal: Modal,
    aInput: Input,
    aButton: Button,
    aDrawer: Drawer,
    aDatePicker: DatePicker,
    aRangePicker: DatePicker.RangePicker,
    aSelect: Select,
    aSelectOption: Select.Option,
    aFormModel: FormModel,
    aFormModelItem: FormModel.Item,
    aUpload: Upload,
    Operate,
    TinyEdit
  }
})
export default class Dynamic extends Vue {
  createDateRange = [] as any
  form = {
    title: '',
    classify: '',
    time: []
  }
  classifyList = [
    { val: '', label: '全部' },
    { val: 'PLAY', label: '玩转小牛' },
    { val: 'INFO', label: '行业资讯' },
    { val: 'FAQ', label: '资质问答' },
    { val: 'RECOMMEND', label: '推荐专题' }
  ]
  dynamicTypeEnumList = [
    { val: 'PLAY', label: '玩转小牛' },
    { val: 'INFO', label: '行业资讯' },
    { val: 'FAQ', label: '资质问答' },
    { val: 'RECOMMEND', label: '推荐专题' }
  ]
  releaseEnumMap = new Map([
    [null, '___'],
    ['RELEASE', '已发布'],
    ['NO_RELEASE', '未发布']
  ])
  dynamicTypeEnumMap = new Map([
    ['PLAY', '玩转小牛'],
    ['INFO', '行业资讯'],
    ['FAQ', '资质问答'],
    ['RECOMMEND', '推荐专题']
  ])
  pageNum = 1
  pageSize = 10
  total = 0
  tableData: any = []
  columns = [
    { title: '编号', dataIndex: 'index', width: 80 },
    { title: '标题名称', dataIndex: 'title', width: 120 },
    { title: '动态分类', dataIndex: 'dynamicType' },
    { title: '排序', dataIndex: 'sort', ellipsis: true },
    {
      title: '阅读量',
      dataIndex: 'readCount',
      width: 200
    },
    {
      title: '创建时间',
      dataIndex: 'dynamicTime',
      width: 120
      // scopedSlots: {customRender: 'statusText'},
    },
    {
      title: '状态',
      dataIndex: 'isRelease',
      width: 120
      // scopedSlots: {customRender: 'statusText'},
    },
    {
      title: '操作',
      width: 350,
      scopedSlots: { customRender: 'operate' }
    }
  ]

  btns = [
    {
      label: '预览',
      type: 'link',
      icon: '',
      isShow: false,
      click: this.preview.bind(this)
    },
    {
      label: '编辑',
      type: 'link',
      icon: 'form',
      isShow: false,
      click: this.edit.bind(this)
    },
    {
      label: '删除',
      type: 'link',
      icon: 'delete',
      isShow: false,
      color: '#ff4d4f',
      click: this.remove.bind(this)
    },
    {
      label: '取消发布',
      type: 'link',
      icon: 'redo',
      color: '#FA8F00',
      isShow: false,
      click: this.cancel.bind(this)
    }
  ]
  title = ''
  visible = false
  formModal = {
    context: '',
    description: '',
    dynamicId: null,
    dynamicTime: '',
    dynamicTypeEnum: 'PLAY',
    homepagePic: '',
    keyWordList: '',
    sort: 1,
    title: ''
  } as any
  fileList = [] as any
  files = [] as any
  releaseTime = null as any
  previewVisible = false
  isShowLength = null

  @Watch('isShowLength', { deep: true })
  hideHandler(newVal: any) {
    this.isShowLength = newVal
    localStorage.setItem('num', JSON.stringify(this.isShowLength))
  }
  validateHomepagePic = (rule: any, value: any, callback: any) => {
    const num = localStorage.getItem('num')
    if (num === 'null' || num === null) {
      callback(new Error('请上传封面图片'))
    } else {
      callback()
    }
  }

  ruleCustom = {
    dynamicTypeEnum: [{ required: true }],
    title: [
      {
        required: true,
        message: '请输入标题名称',
        trigger: 'blur'
      }
    ],
    description: [
      {
        required: true,
        message: '请输入描述说明',
        trigger: 'blur'
      }
    ],
    context: [
      {
        required: true,
        message: '请输入内容',
        trigger: 'blur'
      }
    ],
    keyWordList: [
      {
        required: true,
        message: '请输入关键词',
        trigger: 'blur'
      }
    ],
    dynamicTime: [
      {
        required: true
      }
    ],
    homepagePic: [
      { validator: this.validateHomepagePic, required: true, trigger: 'blur' }
    ]
  }

  onClose() {
    this.visible = false
    this.formModal.context = ''
    this.formModal.description = ''
    this.formModal.dynamicId = ''
    this.formModal.dynamicTime = ''
    this.formModal.dynamicTypeEnum = 'PLAY'
    this.formModal.homepagePic = ''
    this.formModal.keyWordList = ''
    this.formModal.sort = 1
    this.formModal.title = ''
    this.fileList = []
    this.files = []
    this.isShowLength = null
  }
  get pagination(): any {
    return {
      total: this.total,
      pageSize: this.pageSize,
      current: this.pageNum,
      onChange: (page: any, pageSize: any) => {
        //设置当前页面
        // console.log('页码', page)
        // console.log('每页条数', pageSize);
        this.pageSize = pageSize
        this.pageNum = page
        this.getList()
      },
      showSizeChange: () => {
        this.search()
      },
      showTotal: (total: number) => {
        // console.log("total:" + total)
        return `共${total}条数据`
      },
      showSizeChanger: false
    }
  }

  //查询
  search() {
    this.pageNum = 1
    this.pageSize = 10
    this.getList()
  }

  reset() {
    this.pageNum = 1
    this.pageSize = 10
    this.createDateRange = []
    this.form.time = []
    this.form.title = ''
    this.form.classify = ''
  }

  addDynamic() {
    this.title = '新增'
    this.visible = true
    this.releaseTime = this.$moment()
    this.isShowLength = null
    this.formModal.dynamicTime = this.releaseTime
  }

  //获取列表数据
  getList() {
    if (this.createDateRange.length > 1) {
      this.createDateRange[0] =
        this.$moment(this.createDateRange[0]).format('yyyy-MM-DD') + ' 00:00:00'
      this.createDateRange[1] =
        this.$moment(this.createDateRange[1]).format('yyyy-MM-DD') + ' 23:59:59'
    }
    const params = {
      current: this.pageNum,
      size: this.pageSize,
      dynamicTime: this.createDateRange,
      dynamicTypeEnum: this.form.classify === '' ? null : this.form.classify,
      title: this.form.title
    }
    this.$httpRequester.post('dynamic/selAllList', params).then((res: any) => {
      // console.log(res)
      res.list.forEach((item: any, index: any) => {
        item.index = index + 1
        // console.log(item.releaseEnum)
        item.isRelease = this.releaseEnumMap.get(item.releaseEnum)
        item.dynamicType = this.dynamicTypeEnumMap.get(item.dynamicTypeEnum)
        item.btns = []

        this.btns.forEach((bt: any) => {
          if (item.releaseEnum === 'RELEASE') {
            if (bt.label === '取消发布') {
              item.btns.push({
                ...bt,
                isShow: true
              })
            } else {
              item.btns.push({
                ...bt,
                isShow: false
              })
            }
          } else if (item.releaseEnum === 'NO_RELEASE') {
            if (bt.label === '取消发布') {
              item.btns.push({
                ...bt,
                isShow: false
              })
            } else {
              item.btns.push({
                ...bt,
                isShow: true
              })
            }
          }
        })
      })
      this.tableData = res.list
      this.total = res.total
      // console.log(this.tableData)
    })
  }

  changePageNum(pagination: any) {
    this.pageNum = pagination.current
    this.pageSize = pagination.pageSize
    this.getList()
  }

  changePageSize(pagination: any) {
    this.pageSize = pagination.pageSize
    this.getList()
  }

  previewText = {} as any
  previewDynamicId = null

  preview(item: any) {
    this.previewDynamicId = item.dynamicId
    // console.log(this.previewDynamicId)
    this.previewVisible = true
    this.$httpRequester
      .get('dynamic/Preview?dynamicId=' + item.dynamicId)
      .then((res: any) => {
        this.previewText = res
      })
  }

  //编辑详情
  editDetail(id: any) {
    this.$httpRequester
      .post(
        'dynamic/completeDetail',
        { dynamicId: id },
        'application/x-www-form-urlencoded'
      )
      .then((res: any) => {
        this.formModal = { ...res }
        this.formModal.homepagePic = res.homepagePic
        this.formModal.keyWordList = res.keyWordList.join(',')
        this.releaseTime = this.$moment(res.dynamicTime)
        this.fileList = res.homepagePic
          ? res.homepagePic.split(',').map((e: any, i: any) => {
              return {
                uid: i,
                name: 'image.png',
                status: 'done',
                isHaveUpload: true,
                url: e
              }
            })
          : []
        this.isShowLength = this.fileList.length
      })
  }

  removeFile(file: any) {
    const index = this.files.findIndex((f: any) => f.uid === file.uid)
    // console.log(index)
    const newFileList = this.files.slice()
    newFileList.splice(index, 1)
    this.files = newFileList
    this.isShowLength = newFileList.length
    // console.log('删除之后', this.files)
  }

  beforeUpload(file: any) {
    this.files = [...this.files, file]
    return false
  }

  selectFile(item: any) {
    const size = item.file.size
    const MB = 1024 * 1024
    if (size / MB > 10) {
      this.$message.warning('文件过大!')
      return
    }
    this.fileList = item.fileList
    this.isShowLength = item.fileList.length

    if (this.fileList < 1) {
      return
    }
    const file = item.file
    const formdata = new FormData()
    formdata.append('file', file)
    this.$httpRequester.post('cloud/upload', formdata).then(r => {
      this.formModal.homepagePic = r
    })
  }

  edit(item: any) {
    this.visible = true
    this.title = '编辑'
    this.editDetail(item.dynamicId)
  }

  editPreview(id: any) {
    this.previewVisible = false
    this.visible = true
    this.title = '编辑'
    this.editDetail(id)
  }

  remove(item: any) {
    // content: 'Some descriptions',
    const _that = this
    Modal.confirm({
      title: '温馨提示',
      content: `您确定要将标题名称为${item.title}的信息删除？`,
      okText: '确定',
      okType: 'danger',
      cancelText: '取消',
      onOk() {
        _that.$httpRequester
          .get('dynamic/del?dynamicId=' + item.dynamicId)
          .then(() => {
            _that.$message.success('操作成功')
            _that.getList()
          })
      },
      onCancel() {
        _that.$message.success('操作取消')
      }
    })
  }

  cancel(item: any) {
    const _that = this
    Modal.confirm({
      title: '温馨提示',
      content: `您确定要将标题名称为${item.title}的信息取消发布？`,
      okText: '确定',
      cancelText: '取消',
      onOk() {
        _that.$httpRequester
          .get('dynamic/cancelRelease?dynamicId=' + item.dynamicId)
          .then(() => {
            _that.$message.success('操作成功')
            _that.getList()
          })
      },
      onCancel() {
        _that.$message.success('操作取消')
      }
    })
    // console.log(item)
  }

  release(id: any) {
    this.$httpRequester
      .get('dynamic/release?dynamicId=' + id)
      .then((res: any) => {
        this.$message.success('操作成功')
        this.previewVisible = false
        this.getList()
      })
  }

  closePreview() {
    this.previewVisible = false
  }

  dateFormat = 'YYYY-MM-DD'

  disabledDate(current: any) {
    return current && current > this.$moment()
  }

  onChange(date: any, dateString: any) {
    this.form.time = dateString
  }

  releaseTimeChange(date: any, dateString: any) {
    this.formModal.dynamicTime = dateString
    this.releaseTime = dateString
  }

  submit() {
    const feedbackForm = this.$refs.dynamicForm as any
    feedbackForm.validate((val: boolean) => {
      console.log(val)
      if (val) {
        const params = {
          context: this.formModal.context,
          description: this.formModal.description,
          dynamicId: this.formModal.dynamicId,
          dynamicTime: this.$moment(this.formModal.dynamicTime).format(
            'yyyy-MM-DD'
          ),
          dynamicTypeEnum: this.formModal.dynamicTypeEnum,
          homepagePic: this.formModal.homepagePic,
          keyWordList: this.formModal.keyWordList.split(','),
          sort: this.formModal.sort,
          title: this.formModal.title
        }
        if (this.formModal.dynamicId) {
          // if ( params.homepagePic.substring(0, 10) === "http://dhc") {
          //     params.homepagePic= this.formModal.homepagePic.slice(this.formModal.homepagePic.indexOf('.com/') + "com/".length+1,this.formModal.homepagePic.indexOf('?'))
          // }
          this.$httpRequester
            .post('dynamic/update', params)
            .then((res: any) => {
              this.$message.success('操作成功')
              this.onClose()
              this.getList()
            })
        } else {
          this.$httpRequester.post('dynamic/add', params).then((res: any) => {
            this.$message.success('操作成功')
            this.onClose()
            this.getList()
          })
        }
      } else {
        return false
      }
    })
  }

  mounted() {
    this.getList()
  }
  destroyed() {
    localStorage.removeItem('num')
  }
}
