import { Component, Vue } from 'vue-property-decorator'
import {
  Button,
  Input,
  Switch,
  Icon,
  Table,
  Tag,
  Divider,
  DatePicker
} from 'ant-design-vue'

@Component({
  components: {
    aButton: Button,
    aInput: Input,
    aSwitch: Switch,
    aIcon: Icon,
    aTable: Table,
    aTag: Tag,
    aDivider: Divider,
    aDateRange: DatePicker.RangePicker
  }
})
export default class OperaLog extends Vue {
  createDateRange = []
  data = []
  pageSize = 10
  pageNum = 1
  total = 0
  loading = false
  pageSizeOptions = ['10', '20', '30', '40']

  get pagination(): any {
    return {
      total: this.total,
      pageSize: this.pageSize,
      current: this.pageNum,
      onChange: (page: any, pageSize: any) => {
        //设置当前页面
        this.pageSize = pageSize
        this.pageNum = page
        this.search()
      },
      showSizeChange: (page: any, pageSize: any) => {
        this.pageSize = pageSize
        this.pageNum = page
        this.search()
      },
      // showTotal: (total: number) => {
      //   return `共${total}条数据`
      // },
      showQuickJumper: true

    }
  }

  columns = [
    {
      title: '编号ID',
      dataIndex: 'id',
      width: '100px'
    },
    {
      title: '操作用户',
      dataIndex: 'userName',
      width: '200px'
    },
    {
      title: '所属公司',
      dataIndex: 'companyName',
      width: '300px'
    },
    {
      title: '用户角色',
      dataIndex: 'roleName',
      width: '80px'
    },
    {
      title: '操作时间',
      dataIndex: 'createTime',
      sorter: true,
      width: '168px'
    },
    {
      title: 'IP',
      dataIndex: 'userIp',
      width: '110px'
    },
    {
      title: '操作记录',
      dataIndex: 'content',
      width: '110px',
      align:'left'
    }
  ]

  sorter = {
    sortField: '',
    sortOrder: ''
  }

  searchParams = {
    searchName: ''
    // companyName: '',
  }
  dateRanger = [this.$moment(), this.$moment()]
  getPopupContainer() {
    return document.getElementById('dateRanger')
  }
  //搜索
  searchClick() {
    this.pageNum = 1
    const searhch: any = this.searchParams
    const sorter: any = this.sorter
    this.fetch({
      rows: this.pageSize,
      page: this.pageNum,
      sortField: sorter.field,
      sortOrder: sorter.order,
      userName: searhch.searchName,
      startTime: this.dateRanger[0]
        ? this.dateRanger[0].format('YYYY-MM-DD') + ' 00:00:00'
        : '',
      endTime: this.dateRanger[1]
        ? this.dateRanger[1].format('YYYY-MM-DD') + ' 23:59:59'
        : ''
    })
  }
  search() {
    const searhch: any = this.searchParams
    const sorter: any = this.sorter
    this.fetch({
      rows: this.pageSize,
      page: this.pageNum,
      sortField: sorter.field,
      sortOrder: sorter.order,
      userName: searhch.searchName,
      startTime: this.dateRanger[0]
        ? this.dateRanger[0].format('YYYY-MM-DD') + ' 00:00:00'
        : '',
      endTime: this.dateRanger[1]
        ? this.dateRanger[1].format('YYYY-MM-DD') + ' 23:59:59'
        : ''
    })
  }

  //重置
  reset() {
    this.dateRanger = [this.$moment(), this.$moment()]
    this.searchParams.searchName = ''
    this.pageNum = 1
    this.pageSize = 10
    // companyName: '',
    this.search()
  }

  refreshList() {
    this.search()
  }

  //设置分页大小与过滤字段
  handleTableChange(pagination: any, filters: any, sorter: any) {
    // console.log(
    //     'handleTableChange' +
    //     ' pagesize ' +
    //     pagination.pageSize +
    //     ' pageNum ' +
    //     pagination.current
    // )
    this.pageSize = pagination.pageSize
    this.pageNum = pagination.current
    this.sorter = sorter
    this.search()
  }

  mounted() {
    this.search()
  }

  fetch(params = {}) {
    this.loading = true
    this.$httpRequester
      .get('operateLog/queryLogByPage', params)
      .then((r: any) => {
        this.data = r.records
        this.total = r.total
      })
    this.loading = false
  }

  //不可选择未来时间
  disabledDate(current: any) {
    // && current > this.$moment().subtract(3, 'month')
    return !(current < this.$moment().endOf('day'))
  }
}
