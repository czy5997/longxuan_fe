import {Table, Button, Drawer, Divider, Modal} from 'ant-design-vue'
import {Component, Vue} from 'vue-property-decorator'
import Operate from '../../components/Operate.vue'
import EditableCell from '@/components/EditableCell.vue'

@Component({
    components: {
        aTable: Table,
        aButton: Button,
        aDrawer: Drawer,
        aDivider: Divider,
        Operate,
        EditableCell
    }
})
export default class Advice extends Vue {
    loading = false
    tableData = []
    pageSize = 10
    pageNum = 1
    total = 0
    typeMap = new Map<any, any>([
        ['PRODUCT_ISSUES', '产品BUG'],
        ['OPTIMIZATION_SUGGESTIONS', '优化建议'],
        ['OTHER', '其他']
    ])
    selectedRowKeys = [] // Check here to configure the default column
    columns = [
        {title: 'ID', dataIndex: 'id', width: '10%'},
        {title: '反馈类型', dataIndex: 'type', width: '10%'},
        {title: '反馈内容', dataIndex: 'content', width: '20%'},
        {
            title: '附件',
            dataIndex: 'file',
            width: '10%',
            scopedSlots: {customRender: 'download'}
        },
        {title: '联系方式', dataIndex: 'contactInformation', width: '10%'},
        {title: '反馈时间', dataIndex: 'createTime', width: '15%'},
        {
            title: '状态',
            dataIndex: 'stateValue',
            width: '10%',
            scopedSlots: {customRender: 'state'}
        },
        {title: '处理时间', dataIndex: 'handleTime', width: '15%'}
    ]

    get pagination(): any {
        return {
            total: this.total,
            pageSize: this.pageSize,
            currnet: this.pageNum,
            change: this.changePageNum.bind(this),
            showSizeChange: () => {
                this.changePageSize
            },
            showTotal: (total: number) => {
                return `共${total}条数据`
            },
            showSizeChanger: false
        }
    }

    changePageNum(pagination: any) {
        this.pageNum = pagination.current
        this.pageSize = pagination.pageSize
        this.getAdviceList()
    }

    changePageSize(pagination: any) {
        this.pageNum = pagination.current
        this.pageSize = pagination.pageSize
        this.getAdviceList()
    }

    editAdvice() {
        console.log('edit')
    }

    removeAdvice() {
        console.log('removeAdvice')
    }

    //多项删除
    removeAllSleclt() {
        const ids = this.selectedRowKeys
        if (ids.length <= 0) {
            this.$message.info('请选择要删除的选项')
        } else {
            Modal.confirm({
                title: '意见删除',
                content: '确定要删除所选意见吗?',
                okText: '是',
                okType: 'danger',
                cancelText: '否',
                onOk: () => {
                    this.$httpRequester.post('feedback/del', ids).then((r: any) => {
                        this.refreshList()
                    })
                }
            })
        }
    }

    //多选项事件触发
    onSelectChange(selectedRowKeys: any) {
        // console.log(selectedRowKeys)
        this.selectedRowKeys = selectedRowKeys
    }

    //刷新
    refreshList() {
        this.getAdviceList()
    }

    mounted() {
        this.getAdviceList()
    }

    // 下载文档
    previewOrDownloadFile(file: any) {
        console.log(file)
        if (file.type === 'doc' || file.type === 'exc') {
            this.$httpRequester
                .download('upload/file/get', {path: file.url})
                .then((r: any) => {
                    const blob = new Blob([r])
                    const link = document.createElement('a')
                    const time = this.$moment().format('YYYY-MM-DD HH:mm:ss')
                    link.href = window.URL.createObjectURL(blob)
                    link.download = `${file.fileName}`
                    link.click()
                })
        } else {
            window.open(`${apiUrl}upload/file/get?path=${file.url}`)
        }
    }

    //获取意见反馈详情
    getAdviceList() {
        const params = {
            currentPage: this.pageNum,
            pageSize: this.pageSize
        }
        this.loading = true
        this.$httpRequester.get('feedback/selList', params).then((r: any) => {
            const imageTypes = ['.jpg', '.png']
            const docTypes = ['.doc', '.docx']
            const excel = ['.xlsx', '.xls']
            r.list.forEach((e: any) => {
                e.type = this.typeMap.get(e.type)
                e.handleTime = e.handleTime ? this.$moment(e.handleTime).format('YYYY-MM-DD HH:mm:ss') : '-'
                e.createTime = e.createTime ? this.$moment(e.createTime).format('YYYY-MM-DD HH:mm:ss') : '-'
                if (e.state === '已处理') {
                    e.stateValue = '1'
                } else {
                    e.stateValue = '0'
                }
                e.downloadFiles = []
                e.file.forEach((f: any) => {
                    const findSuffix = f.filePath.indexOf('.')
                    const suffix = f.filePath.substr(findSuffix, f.length)
                    const image = imageTypes.find((image: any) => image === suffix)
                    const doc = docTypes.find((doc: any) => doc === suffix)
                    const exc = excel.find((ex: any) => ex === suffix)
                    if (image) {
                        e.downloadFiles.push({
                            fileName: f.fileName,
                            type: 'image',
                            icon: 'file-image',
                            suffix,
                            title: '图片',
                            url: f.filePath
                        })
                    } else if (doc) {
                        e.downloadFiles.push({
                            fileName: f.fileName,
                            type: 'doc',
                            icon: 'file-word',
                            suffix,
                            title: '文档',
                            url: f.filePath
                        })
                    } else if (exc) {
                        e.downloadFiles.push({
                            fileName: f.fileName,
                            type: 'exc',
                            icon: 'file-excel',
                            suffix,
                            title: '表格',
                            url: f.filePath
                        })
                    }
                })
            })
            // console.log(r.data)
            this.tableData = r.list
            this.total = r.total
        })
        this.loading = false
    }

    onCellChange(value: string, item: any) {
        const params = {
            feedbackId: item.id,
            state: value
        }
        this.loading = true
        this.$httpRequester
            .post('feedback/changeState', params, 'application/x-www-form-urlencoded')
            .then((r: any) => {
                this.$message.success('操作成功')
                this.loading = false
                this.getAdviceList()
            })
    }
}
