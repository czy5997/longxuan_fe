import { Component, Vue } from 'vue-property-decorator'
import {
  Input,
  Table,
  Button,
  Divider,
  Pagination,
  Drawer,
  Radio,
  Select,
  FormModel,
  Modal,
  Switch
} from 'ant-design-vue'
// import 'ant-design-vue/dist/antd.css'
import Operate from '@/components/Operate.vue'
import { userModule } from '@/store/modules/user'
import plusIcon from '@/assets/image/ic_plus.png'

@Component({
  components: {
    aTable: Table,
    aInput: Input,
    aButton: Button,
    aDivider: Divider,
    aDrawer: Drawer,
    Operate,
    Pagination,
    aRadio: Radio,
    aRadioGroup: Radio.Group,
    aSelect: Select,
    aSelectOption: Select.Option,
    aFormModel: FormModel,
    aFormModelItem: FormModel.Item,
    aSwitch: Switch
  }
})
export default class User extends Vue {
  $refs!: {
    userFormModel: any
    userPage: any
    drawer: any
  }
  get pagination(): any {
    return {
      total: this.total,
      pageSize: this.pageSize,
      current: this.current,
      onChange: (page: any, pageSize: any) => {
        //设置当前页面
        // console.log('页码', page)
        // console.log('每页条数', pageSize);
        this.current = pageSize
        this.current = page
        // this.search()
      },
      showQuickJumper: true,
    }
  }

  plusIcon=plusIcon
  tableData = []
  current = 1
  pageSize = 10
  total = 0
  columns = [
    { title: 'ID', dataIndex: 'id' ,width: '50px',align:'left'},
    { title: '姓名', dataIndex: 'memberName', width: '85px',align:'left' },
    { title: '账号', dataIndex: 'userAccount',width: '90px',align:'left' },
    { title: '微信昵称', dataIndex: 'wechatName', width: '60px',align:'left'},
    { title: '用户角色', dataIndex: 'roleName',width: '85px',align:'left' },
    { title: '所属公司', dataIndex: 'companyName',width: '65px',align:'left' },
    { title: '所属部门', dataIndex: 'departmentName', width: '50px',align:'left'},
    {
      title: '部门领导',
      dataIndex: 'isLead',
      width: '85px',align:'left',
      scopedSlots: { customRender: 'change2String' }
    },
    { title: '创建时间', dataIndex: 'createTime',width: '85px',align:'left' },
    { title: '最后登录时间', dataIndex: 'loginTime',width: '85px',align:'left' },
    { title: '用户ip', dataIndex: 'userIp',width: '90px',align:'left' },
    {
      title: '启用状态',
      dataIndex: 'isEnable',
      width: '60px',align:'left',
      scopedSlots: { customRender: 'enableSwitch' }
    },
    { title: '操作',width: '110px',align:'left', scopedSlots: { customRender: 'operate' } }
  ]
  btns = [
    {
      label: '编辑',
      type: 'link',
      click: this.showEditDrawer.bind(this)
    },
    {
      label: '删除',
      type: 'link',
      click: this.remove.bind(this)
    }
  ]
  searchParams = {
    searchName: '',
    companyName: ''
  }
  userParams: any = {
    id: 0,
    memberName: '',
    departmentId: 0,
    departmentName: '',
    companyId: null,
    companyName: '',
    userAccount: '',
    roleId: '',
    roleName: '',
    roleLevel: 0,
    isLead: false,
    enable: 0,
    verifyCode: '',
    isEnable: true
  }
  deleteParams = {
    idsList: []
  }
  rules: any = {
    userAccount: [
      { validator: this.validMobile, required: true, trigger: 'blur' }
    ],
    memberName: [
      { required: true, message: '请输入员工姓名', trigger: 'blur' }
    ],
    roleName: [{ required: true, message: '请选择员工角色', trigger: 'blur' }],
    companyName: [
      { required: true, message: '请选择所属公司', trigger: 'blur' }
    ],
    isLead: [{ required: true }],
    enable: [{ required: true }]
  }
  validMobile(rule: any, value: any, callback: any) {
    const mobileTest = /^1(3|4|5|6|7|8|9)\d{9}$/
    if (value === '') {
      callback(new Error('请输入手机号码'))
    } else if (!mobileTest.test(value)) {
      callback(new Error('请输入正确的手机号码'))
    } else {
      callback()
    }
  }
  drawDisplay = false
  accountDisplay = false
  verifyCode = true
  companyCanSelect = false
  departmentNameCanSelect = false
  isLeadCanSelect = false
  companyData = {}
  companyListData: any = []
  memberData = []
  roleData = []
  departmentData = []
  codeBtnText = '获取验证码'
  codeSIV: any = null
  drawTitle = '新增用户'
  codeDisabled = false
  upLeadSelectDisplay = false
  roleCanSelect = false

  mounted() {
    this.search()
    if (userModule.roleLevel == 1) {
      this.listAllCompany()
      this.listRole()
    } else if (userModule.roleLevel == 2) {
      const companyId = userModule.companyId
      this.userParams.companyId = companyId
      this.getCompany(companyId)
      this.listAllMember(companyId)
      this.listRole()
      this.DepartmentNameList()
    }
  }

  get rowSelection() {
    return {
      onChange: (selectedRowKeys: any) => {
        this.deleteParams.idsList = selectedRowKeys
      }
    }
  }

  getFormNode() {
    return document.getElementById('userFormModel')
  }
  refreshList() {
    this.search()
  }

  showAddDrawer() {
    this.drawTitle = '添加用户'
    this.departmentNameCanSelect = false
    this.verifyCode = true
    this.drawDisplay = true
    this.accountDisplay = false
    this.userParams.id = 0
    this.userParams.userAccount = ''
    this.userParams.enable = true
    this.userParams.isEnable = true
    this.userParams.isLead = false
    this.userParams.roleId = ''
    this.userParams.roleName = ''
    this.userParams.departmentName = ''
    this.userParams.departmentId = ''
    this.userParams.memberName = ''
    this.userParams.verifyCode = ''
    if (userModule.roleLevel == 1) {
      this.departmentNameCanSelect = true
      this.companyCanSelect = true
      this.userParams.companyName = ''
    } else if (userModule.roleLevel == 2) {
      this.companyCanSelect = false
      this.roleCanSelect = true
      this.userParams.companyId = userModule.companyId
    }
  }

  showEditDrawer(item: any) {
    //超级管理员的话,公司可以选
    // console.log(userModule.roleLevel)
    if (userModule.roleLevel === 1) {
      this.companyCanSelect = true
    } else {
      this.companyCanSelect = false
    }

    this.drawTitle = '编辑用户'

    this.verifyCode = false
    this.accountDisplay = true
    this.userParams.id = item.id
    this.userParams.isLead = item.isLead
    this.userParams.userAccount = item.userAccount
    this.userParams.memberName = item.memberName
    this.userParams.enable = item.enable
    this.userParams.isEnable = item.isEnable
    this.userParams.roleId = item.roleId
    this.userParams.roleName = item.roleName
    this.userParams.companyId = item.companyId
    this.userParams.companyName = item.companyName
    this.userParams.departmentId = item.departmentId
    this.userParams.roleLevel = item.roleLevel
    // console.log('编辑',item.roleLevel)
    // if(item.roleLevel>2){
    // this.isLeadCanSelect=false
    // this.userParams.isLead=true
    // }

    if (item.roleLevel < 3) {
      // console.log("不可以选择啦")
      //超级管理员不可选择部门,部门领导
      // 销售人员，经理 可以选择领导
      this.departmentNameCanSelect = true
      this.userParams.departmentName = ''
      this.userParams.departmentId = 0
      this.isLeadCanSelect = true
      this.roleCanSelect = false
      this.userParams.isLead = false
      // this.getMemberByRoleLevel(item.roleLevel)
      this.upLeadSelectDisplay = true
    } else {
      // console.log("可以选择啦")
      // this.departmentNameCanSelect = true
      this.departmentNameCanSelect = false
      this.isLeadCanSelect = false
      this.userParams.departmentName = item.departmentName
      this.userParams.upLeadId = ''
      this.roleCanSelect = true
      // this.userParams.upLeadName = ''
      this.upLeadSelectDisplay = false
    }

    this.handleCompanyChange(this.userParams.companyId)
  }

  onClose() {
    this.upLeadSelectDisplay = false
    this.roleCanSelect = false
    this.drawDisplay = false
  }

  submitUserInfo() {
    this.$refs.userFormModel.validate((val: boolean) => {
      if (val) {
        if (this.userParams.id == 0) {
          // console.log('添加用户详情',this.userParams)
          this.$httpRequester.post('member/add', this.userParams).then(() => {
            this.onClose()
            this.$message.success('新增用户')
            this.search()
          })
        } else if (this.userParams.id > 0) {
          // console.log('提交用户更改',this.userParams)
          this.$httpRequester
            .post('member/update', this.userParams)
            .then(() => {
              this.onClose()
              this.$message.success('更新成功')
              this.search()
            })
        }
      }
    })
  }

  search() {
    this.$httpRequester
      .post('member/list', this.searchParams)
      .then((r: any) => {
        // console.log('userList',r)
        this.current = r.pageNum
        this.pageSize = r.pageSize
        this.total = r.totalSize
        r.data.forEach((e: any) => {
          e.isEnable = e.enable
          e.wechatName = e.wechatName || '-'
        })
        this.tableData = r.data
      })
  }

  send() {
    if (this.userParams.userAccount == '') {
      this.$message.success('请输入手机号，即：登录账号')
      return
    }
    this.$httpRequester
      .post(
        'user/sms/verificte/send',
        { cellPhone: this.userParams.userAccount },
        'application/x-www-form-urlencoded'
      )
      .then(() => {
        this.$message.success('请查看您的手机短信')
        this.codeDisabled = true
        let time = 120
        this.codeSIV = setInterval(() => {
          time -= 1
          this.codeBtnText = `重新发送(${time}S)`
          if (time === 1) {
            this.codeBtnText = '获取验证码'
            this.codeDisabled = false
            clearTimeout(this.codeSIV)
          }
        }, 1000)
      })
  }

  getCompany(companyId: any) {
    this.$httpRequester.get('company/get?id=' + companyId).then((r: any) => {
      this.companyData = r
      this.companyListData = [
        { id: r.id, companyId, companyName: r.companyName }
      ]
      this.userParams.companyId = r.companyId
      this.userParams.companyName = r.companyName
    })
  }

  listAllCompany() {
    this.$httpRequester.get('company/listAll').then((r: any) => {
      this.companyListData = r
    })
  }

  listAllMember(companyId: any) {
    if (companyId == null) {
      this.memberData = []
    } else {
      this.memberData = []
      this.$httpRequester
        .get('member/listAll?companyId=' + companyId)
        .then((r: any) => {
          // console.log(r)
          this.memberData = r || []
        })
    }
  }

  listRole() {
    this.$httpRequester.get('role/listAll').then((r: any) => {
      this.roleData = r
    })
  }

  //部门列表
  DepartmentNameList() {
    this.$httpRequester
      .get('department/listAll?companyId=' + this.userParams.companyId)
      .then((r: any) => {
        // console.log('部门列表',r)
        this.departmentData = r
      })
  }

  reset() {
    this.searchParams.searchName = ''
    this.searchParams.companyName = ''
    this.search()
  }

  filterOption(input: any, option: any) {
    return (
      option.componentOptions.children[0].text
        .toLowerCase()
        .indexOf(input.toLowerCase()) >= 0
    )
  }

  removeAllSelect() {
    if (this.deleteParams.idsList.length <= 0){
      this.$message.info('请选择要删除的用户')
    }else {
      Modal.confirm({
        title: '用户删除',
        content: '确定要删除所选用户吗?',
        okText: '确认',
        okType: 'primary',
        cancelText: '取消',
        onOk: () => {
          this.$httpRequester
              .post('member/delete', this.deleteParams.idsList)
              .then(() => {
                this.$message.success('删除成功')
                this.search()
              })
        }
      })
    }
  }

  remove(item: any) {
    Modal.confirm({
      title: '用户删除',
      content: '确定要删除该记录吗?',
      okText: '是',
      okType: 'danger',
      cancelText: '否',
      onOk: () => {
        this.$httpRequester.post('member/delete', [item.id]).then(() => {
          this.$message.success('删除成功')
          this.search()
        })
      }
    })
  }

  handleCompanyChange(value: any) {
    this.$httpRequester
      .get('department/listAll?companyId=' + value)
      .then((r: any) => {
        this.departmentData = r
      })
    // this.listAllMember(value);
    if (this.userParams.roleLevel) {
      this.getMemberByRoleLevel(this.userParams.roleLevel)
    }
    this.userParams.companyId = value
    if (userModule.roleLevel < 2) {
      console.log('thise')
      this.roleCanSelect = true
    }
    this.drawDisplay = true
  }

  handDepartmentNameChange(value: any) {
    // this.userParams.departmentName=""
    // this.userParams.departmentId=null
    this.userParams.departmentId = value
    const findName = this.departmentData.find((e: any) => e.id === value) as any
    this.userParams.departmentName = findName ? findName.departmentName : ''
  }

  handleRoleChange(value: any) {
    // console.log(value)
    // this.userParams.upLeadId = ''
    this.userParams.roleId = value
    //角色等级,公司管理员，所属部门置灰， 是否为部门领导置灰
    if (value < 3) {
      this.departmentNameCanSelect = true
      this.isLeadCanSelect = true
      this.userParams.departmentName = ''
      this.userParams.departmentId = null
      this.$set(this.rules, 'departmentName', [])
    } else {
      this.departmentNameCanSelect = false
      // this.rules.departmentName = [
      //   { required: true, message: '请选择所属部门', trigger: 'blur' },
      // ]
      this.$set(this.rules, 'departmentName', [
        { required: true, message: '请选择所属部门', trigger: 'blur' }
      ])
      // this.rules = this.$deepCopy(this.rules)
      this.isLeadCanSelect = false
      this.userParams.isLead = false
    }
    // this.userParams.upLeadName = ''
    const findRole = this.roleData.find((r: any) => r.id === value) as any
    this.userParams.roleLevel = findRole.roleLevel
    // if (this.userParams.roleLevel < 3) {
    //
    // } else {
    //
    // }
    // console.log('角色等级',this.userParams.roleLevel)
    if (this.userParams.roleLevel === 1) {
      this.userParams.companyId = ''
      this.userParams.companyName = ''
      this.companyCanSelect = false
    } else {
      // this.userParams.companyName = userModule.companyName
      if (userModule.roleLevel === 1) {
        this.companyCanSelect = true
      } else {
        this.companyCanSelect = false
      }
    }
    if (this.userParams.roleLevel <= 2) {
      this.upLeadSelectDisplay = false
    } else {
      this.upLeadSelectDisplay = true
      this.getMemberByRoleLevel(this.userParams.roleLevel)
    }
  }

  getMemberByRoleLevel(level: number) {
    const params = {
      roleLevel: level,
      companyId: userModule.roleLevel === 1 ? this.userParams.companyId : ''
    }
    this.$httpRequester.get('member/listByRole', params).then((r: any) => {
      this.memberData = r || []
    })
  }

  handleItemSwitch(item: any, record: any) {
    if (record.id > 0) {
      this.$httpRequester
        .get('member/status/change?id=' + record.id + '&enable=' + item)
        .then(() => {
          this.$message.success('状态更新成功')
          this.search()
        })
    }
  }
}
