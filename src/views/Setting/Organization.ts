import { Component, Vue } from 'vue-property-decorator'
import {
  Button,
  DatePicker,
  Input,
  Table,
  Switch,
  Drawer,
  Select,
  FormModel,
  Cascader,
  Radio,
  Modal
} from 'ant-design-vue'
import Operate from '../../components/Operate.vue'
import { userModule } from '@/store/modules/user'
import plusIcon from '@/assets/image/ic_plus.png'
import { areaJSON } from '@/utils/area.js'

@Component({
  components: {
    aButton: Button,
    aDateRange: DatePicker.RangePicker,
    aInput: Input,
    aTable: Table,
    aSwitch: Switch,
    aDrawer: Drawer,
    aSelect: Select,
    aSelectOption: Select.Option,
    aFormModel: FormModel,
    aFormModelItem: FormModel.Item,
    aRadio: Radio,
    aRadioGroup: Radio.Group,
    aCascader: Cascader,
    Operate,
    userModule: userModule
  }
})
export default class Role extends Vue {
  $refs!: {
    organizationForm: any
  }
  plusIcon = plusIcon
  searchKeyword = ''
  isEdit = false
  createDateRange = []
  tableData = []
  total = 0
  pageSize = 10
  pageNum = 1
  drawerTitle = '添加组织'
  drawerDisplay = false
  areaList = []
  companyId = -1
  btnPermDisplay = false
  columns = [
    { title: 'ID', dataIndex: 'id', width: '50px', align: 'center' },
    // { title: '组织编码', dataIndex: 'companyCode', width: 100 },
    {
      title: '公司名称',
      dataIndex: 'companyName',
      width: '150px',
      align: 'left'
    },
    {
      title: '联系人',
      dataIndex: 'contactsPerson',
      width: '70px',
      align: 'left'
    },
    {
      title: '联系电话',
      dataIndex: 'contactsPhone',
      width: '70px',
      align: 'left'
    },
    {
      title: '使用版本',
      dataIndex: 'buyVersionNum',
      width: '50px',
      align: 'left'
    },
    {
      title: '使用人数',
      dataIndex: 'buyUserCount',
      width: '50px',
      align: 'left'
    },
    {
      title: '使用地区',
      dataIndex: 'useAreaName',
      width: '80px',
      align: 'left'
    },
    {
      title: '公司人数',
      dataIndex: 'count',
      width: '50px',
      align: 'left'
    },
    {
      title: '系统有效期',
      dataIndex: 'systemTime',
      width: '100px',
      align: 'left',
      scopedSlots: { customRender: 'statusText' }
    },
    {
      title: '创建时间',
      width: '100px',
      align: 'left',
      dataIndex: 'createTime',
      scopedSlots: { customRender: 'statusText' }
    },
    {
      title: '状态',
      dataIndex: 'isEnable',
      width: '50px',
      align: 'left',
      scopedSlots: { customRender: 'enableSwitch' }
    },
    {
      title: '操作',
      width: '110px',
      align: 'left',
      scopedSlots: { customRender: 'operate' }
    }
  ]
  versionOption = [
    { value: 'V_FREE_TEMP', label: '体验版' },
    { value: 'V_PERSONAL_VIP', label: 'VIP个人版' },
    { value: 'V_ENTERPRISE_VIP', label: 'VIP企业版' },
    { value: 'V_TEAM_VIP', label: 'VIP集团版' }
  ]

  editModel: any = {
    companyName: '',
    // companyCode: '',
    contactsPerson: '',
    contactsPhone: '',
    useAreaName: [],
    dateRange: [],
    buyVersion: undefined,
    buyUserCount: '',
    isEnable: false
  }
  params: any = {
    pageNum: this.pageNum,
    pageSize: this.pageSize,
    companyName: '',
    startDate: null,
    endDate: null,
    buyVersion: undefined
  }
  btns = [
    {
      label: '编辑',
      type: 'link',
      color: '#1890FF',
      click: this.editOrganization.bind(this)
    },
    {
      label: '删除',
      type: 'link',
      color: '#1890FF',
      click: this.removeOrganization.bind(this)
    }
  ]
  deleteParams = {
    idsList: []
  }
  userCountDisabled = false
  versonOption = [
    { label: '体验版', value: 'V_FREE_TEMP' },
    { label: 'VIP个人版', value: 'V_PERSONAL_VIP' },
    { label: 'VIP企业版', value: 'V_ENTERPRISE_VIP' },
    { label: 'VIP集团版', value: 'V_TEAM_VIP' }
  ]
  rules = {
    contactsPhone: [
      { validator: this.validMobile, required: true, trigger: 'blur' }
    ],
    contactsPerson: [
      { required: true, message: '请输入公司联系人姓名', trigger: 'blur' }
    ],
    companyName: [
      { required: true, message: '请输入公司名称', trigger: 'blur' }
    ],
    // companyCode: [
    //   { validator: this.validAlphaBet, required: true, trigger: 'blur' },
    // ],
    dateRange: [
      { required: true, message: '请选择系统使用期限', trigger: 'blur' }
    ],
    useAreaName: [
      { required: true, message: '请选择使用地区', trigger: 'blur' }
    ],
    buyVersion: [
      { required: true, message: '请选择使用版本', trigger: 'blur' }
    ],
    buyUserCount: [
      { required: true, validator: this.validNumber, trigger: 'blur' }
    ],
    isEnable: [
      {
        required: true
      }
    ]
  }

  validAlphaBet(rule: any, value: any, callback: any) {
    const regx = /^[a-z0-9]+$/i
    if (value) {
      if (!regx.test(value)) {
        callback(new Error('只能输入英文字母及数字'))
      } else {
        callback()
      }
    } else {
      callback()
    }
  }
  get pagination(): any {
    return {
      total: this.total,
      pageSize: this.pageSize,
      current: this.pageNum,
      onChange: (page: any, pageSize: any) => {
        //设置当前页面
        this.pageSize = pageSize
        this.pageNum = page
      },
      showSizeChange: (page: any, pageSize: any) => {
        this.pageSize = pageSize
        this.pageNum = page
      },
      // showTotal: (total: number) => {
      //   return `共${total}条数据`
      // },
      showQuickJumper: true

    }
  }

  validNumber(rule: any, value: any, callback: any) {
    if (!/^\d+$/.test(value)) {
      callback(new Error('请输入正确的数字'))
    } else {
      callback()
    }
  }
  validMobile(rule: any, value: any, callback: any) {
    const mobileTest = /^1(3|4|5|6|7|8|9)\d{9}$/
    if (value === '') {
      callback(new Error('请输入手机号码'))
    } else if (!mobileTest.test(value)) {
      callback(new Error('请输入正确的手机号码'))
    } else {
      callback()
    }
  }

  editOrganization(item: any) {
    this.drawerTitle = '编辑' + item.companyName
    this.companyId = item.id
    this.editModel.companyName = item.companyName
    this.editModel.companyCode = item.companyCode
    this.editModel.contactsPerson = item.contactsPerson
    this.editModel.contactsPhone = item.contactsPhone
    this.editModel.useAreaName = item.useAreaName.split('/')
    this.editModel.buyUserCount = item.buyUserCount
    this.editModel.buyVersion = item.buyVersion || []
    if (item.effectTime != null && item.failureTime != null) {
      this.editModel.dateRange = [
        this.$moment(item.effectTime),
        this.$moment(item.failureTime)
      ]
    } else {
      this.editModel.dateRange = []
    }
    this.editModel.isEnable = item.isEnable
    this.organizationDrawerShow()
  }

  removeOrganization(item: any) {
    Modal.confirm({
      title: '温馨提示',
      icon: 'exclamation-circle',
      content: `确认删除${item.companyName}该组织吗?`,
      onOk: () => {
        this.$httpRequester.post('company/delete', [item.id]).then(() => {
          this.$message.success('操作成功')
          this.getOrganizationData()
        })
      },
      onCancel: () => {
        this.$message.info('用户取消')
      }
    })
  }

  addOrganizationDrawer() {
    this.drawerTitle = '添加组织'
    this.companyId = 0
    this.editModel.companyName = ''
    this.editModel.companyCode = ''
    this.editModel.contactsPerson = ''
    this.editModel.contactsPhone = ''
    this.editModel.useAreaName = []
    this.editModel.dateRange = []
    this.editModel.buyUserCount = ''
    this.editModel.buyVersion = []
    this.editModel.isEnable = false
    this.organizationDrawerShow()
  }

  refreshList() {
    this.getOrganizationData()
  }

  removeAllSleclt() {
    if (this.deleteParams.idsList.length <= 0) {
      this.$message.info('请选择要删除的组织')
    } else {
      Modal.confirm({
        title: '删除组织',
        content: '确定要删除所选组织吗?',
        okText: '确认',
        okType: 'primary',
        cancelText: '取消',
        onOk: () => {
          this.$httpRequester
            .post('company/delete', this.deleteParams.idsList)
            .then(() => {
              this.$message.success('删除成功')
              this.getOrganizationData()
            })
        }
      })
    }
  }

  organizationDrawerShow() {
    this.$refs.organizationForm.clearValidate()
    this.drawerDisplay = true
  }

  closeDrawer() {
    this.drawerDisplay = false
    this.getOrganizationData()
  }
  selectBuyVersion(val: any) {
    if (val === 'V_PERSONAL_VIP') {
      this.userCountDisabled = true
      this.editModel.buyUserCount = 1
    } else if (val === 'V_ENTERPRISE_VIP') {
      this.userCountDisabled = true
      this.editModel.buyUserCount = 10
    } else {
      this.userCountDisabled = false
      this.editModel.buyUserCount = ''
    }
  }
  getOrganizationData() {
    this.params.companyName = this.searchKeyword
    this.$httpRequester.post('company/list', this.params).then((r: any) => {
      r.data.forEach((e: any) => {
        e.systemTime = e.effectTime
          ? this.$moment(e.effectTime).format('yyyy/MM/DD') +
            ' - ' +
            this.$moment(e.failureTime).format('yyyy/MM/DD')
          : ''
        e.isEnable = e.enable ? true : false
        switch (e.buyVersion) {
          case 'V_FREE_TEMP':
            e.buyVersionNum = ' 体验版'
            break
          case 'V_PERSONAL_VIP':
            e.buyVersionNum = 'VIP个人版'
            break
          case 'V_TEAM_VIP':
            e.buyVersionNum = 'VIP集团版'
            break
          case 'V_ENTERPRISE_VIP':
            e.buyVersionNum = 'VIP企业版'
            break
          default:
            e.buyVersionNum = ''
            break
        }
      })
      this.tableData = r.data
      this.total = r.totalSize
    })
  }
  //获取时间数组
  onChange(date: any, dateString: any) {
    this.params.startDate = dateString[0] ? dateString[0] + ' 00:00:00' : ''
    this.params.endDate = dateString[1] ? dateString[1] + ' 23:59:59' : ''
  }
  submitOrganizationEdit() {
    this.$refs.organizationForm.validate((val: boolean) => {
      if (val) {
        if (this.companyId === 0) {
          const params = {
            ...this.editModel,
            enable: this.editModel.isEnable,
            effectTime: this.$moment(this.editModel.dateRange[0]).format(
              'yyyy-MM-DD HH:mm:ss'
            ),
            failureTime: this.$moment(this.editModel.dateRange[1]).format(
              'yyyy-MM-DD HH:mm:ss'
            )
          }
          params.useAreaName = params.useAreaName.join('/')
          delete params.dateRange
          this.$httpRequester.post('company/add', params).then(() => {
            this.$message.success('操作成功')
            this.closeDrawer()
          })
        } else {
          const params = {
            ...this.editModel,
            enable: this.editModel.isEnable,
            id: this.companyId,
            effectTime: this.$moment(this.editModel.dateRange[0]).format(
              'yyyy-MM-DD HH:mm:ss'
            ),
            failureTime: this.$moment(this.editModel.dateRange[1]).format(
              'yyyy-MM-DD HH:mm:ss'
            )
          }
          params.useAreaName = params.useAreaName
            ? params.useAreaName.join('/')
            : ''
          delete params.dateRange
          this.$httpRequester.post('company/update', params).then(() => {
            this.$message.success('操作成功')
            this.closeDrawer()
          })
        }
      }
    })
  }

  getAreaList() {
    // this.$httpRequester.get('regionCode/selList').then((r: any) => {
    this.areaList = areaJSON
    // })
  }

  mounted() {
    this.getAreaList()
    this.getOrganizationData()
    if (userModule.roleId == 1) {
      this.btnPermDisplay = true
    } else if (userModule.roleId == 2) {
      this.btnPermDisplay = false
    }
  }

  handleItemSwitch(item: any, record: any) {
    if (record.id > 0) {
      this.$httpRequester
        .get('company/status/change?id=' + record.id + '&enable=' + item)
        .then(() => {
          this.$message.success('状态更新成功')
          this.getOrganizationData()
        })
    }
  }

  reset() {
    this.searchKeyword = ''
    this.createDateRange = []
    this.params.startDate = null
    this.params.endDate = null
    this.params.buyVersion = undefined
    this.getOrganizationData()
    // console.log('请求的数据',this.params);
  }

  removeRole(item: any) {
    Modal.confirm({
      title: '用户删除',
      content: '确定要删除该记录吗?',
      okText: '是',
      okType: 'danger',
      cancelText: '否',
      onOk: () => {
        this.$httpRequester.post('role/delete', [item.id]).then(() => {
          this.$message.success('删除成功')
          this.getOrganizationData()
        })
      }
    })
  }

  get rowSelection() {
    return {
      onChange: (selectedRowKeys: any, selectedRows: any) => {
        this.deleteParams.idsList = selectedRowKeys
      }
    }
  }
}
