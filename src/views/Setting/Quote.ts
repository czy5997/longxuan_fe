import {
  Table,
  Button,
  Drawer,
  Divider,
  TreeSelect,
  Input,
  FormModel,
  Modal,
  Select
} from 'ant-design-vue'
import { Component, Vue } from 'vue-property-decorator'
import { userModule } from '@/store/modules/user'
import Operate from '../../components/Operate.vue'
@Component({
  components: {
    aInput: Input,
    aTable: Table,
    aButton: Button,
    aDrawer: Drawer,
    aDivider: Divider,
    aTreeSelect: TreeSelect,
    aFormModel: FormModel,
    aFormModelItem: FormModel.Item,
    aSelect: Select,
    aOption: Select.Option,
    Operate
  }
})
export default class Quote extends Vue {
  $refs!: {
    firstForm: any
    secondForm: any
  }
  isSuperAdmin = userModule.roleLevel === 1
  allQuali = []
  allQualiOptions: any = []
  loading = false
  tableData = []
  pageSize = 10
  pageNum = 1
  total = 0
  drawDisplay = false
  destroy = true
  drawTitle = '添加报价'
  selectedRowKeys = [] // Check here to configure the default column
  //qualification = []
  qualificationOptions: any = []
  labelCol = { span: 4 }
  wrapperCol = { span: 14 }
  columns = [
    { title: 'ID', dataIndex: 'id', width: '15%' },
    { title: '所属公司', dataIndex: 'companyName', width: '20%' },
    { title: '资质名称', dataIndex: 'qualificationName', width: '30%' },
    { title: '参考价格（万元）', dataIndex: 'priceRange', width: '15%' },
    { title: '操作', width: 230, scopedSlots: { customRender: 'operate' } }
  ]
  get btns() {
    return [
      {
        label: '编辑',
        type: 'link',
        icon: 'form',
        click: this.editAnnouncement.bind(this)
      },
      {
        label: '删除',
        type: 'link',
        icon: 'delete',
        isShow: !this.isSuperAdmin,
        color: '#909399',
        click: this.removeAnnouncement.bind(this)
      }
    ]
  }

  form = {
    id: '',
    qualificationId: '',
    upperLimit: '',
    lowerLimit: ''
  }

  searchParams = {
    qualificationId: ''
  }
  data() {
    //最低报价的错误提示
    const validateNumber = (rule: any, value: any, callback: any) => {
      if (!value) {
        return callback(new Error('请输入整数,并且不能超过10位!'))
      }
      const reg = new RegExp('^\\d{1,10}$')
      if (!reg.test(value)) {
        callback(new Error('请输入整数,并且不能超过10位!'))
      } else {
        callback()
      }
    }
    //最高报价的错误提示
    const validateNumber1 = (rule: any, value: any, callback: any) => {
      const reg = new RegExp('^\\d{1,10}$')
      const lowerLimit: number = parseFloat(this.form.lowerLimit)
      if (!reg.test(value)) {
        callback(new Error('请输入整数,并且不能超过10位!'))
      } else if (value < lowerLimit) {
        callback(new Error('最高参考价不能低于最低参考价格!'))
      } else {
        callback()
      }
    }
    return {
      registerRules: {
        qualificationId: [
          { required: true, message: '请选择资质名称', trigger: 'change' }
        ],
        lowerLimit: [{ validator: validateNumber, trigger: 'change' }],
        upperLimit: [{ validator: validateNumber1, trigger: 'change' }]
      }
    }
  }
  get pagination(): any {
    return {
      total: this.total,
      pageSize: this.pageSize,
      currnet: this.pageNum,
      change: this.changePageNum.bind(this),
      showSizeChange: () => {
        this.changePageSize
      },
      showTotal: (total: number) => {
        return `共${total}条数据`
      },
      showSizeChanger: false
    }
  }
  changePageNum(pagination: any) {
    this.pageNum = pagination.current
    this.pageSize = pagination.pageSize
    this.search()
  }
  changePageSize(pagination: any) {
    this.pageNum = pagination.current
    this.pageSize = pagination.pageSize
    this.search()
  }

  //打开新增界面
  showEditDrawer() {
    this.drawDisplay = true
    this.drawTitle = '添加报价'
    this.form = {
      id: '',
      qualificationId: '',
      upperLimit: '',
      lowerLimit: ''
    }
  }

  edit = false

  //关闭新增或者修改界面
  onClose() {
    this.drawDisplay = false
    if (this.edit) {
      this.qualificationOptions.pop()
      this.edit = false
    }
    //this.qualification=[];
  }

  //打开编辑界面，并补充数据到界面
  editAnnouncement(item: any) {
    const form = this.form
    this.drawDisplay = true
    this.drawTitle = '修改报价'
    this.edit = true
    this.qualificationOptions.push({
      label: item.qualificationName,
      value: item.qualificationId
    })

    form.id = item.id
    form.qualificationId = item.qualificationId
    form.lowerLimit = item.lowerLimit
    form.upperLimit = item.upperLimit
    /*const qualification: any = this.qualification;
        qualification.push({
            label: item.qualificationName,
            value: item.qualificationId,
        })*/
  }

  //单个删除
  removeAnnouncement(item: any) {
    const id = item.id
    this.$httpRequester
      .delete(
        'company/sharesPrice/del',
        { ids: id },
        'application/x-www-form-urlencoded'
      )
      .then((r: any) => {
        this.$message.success('操作成功')
        this.refreshList()
      })
  }

  //批量删除
  removeAllSleclt() {
    const ids = this.selectedRowKeys.toString()
    if (ids.length <= 0) {
      this.$message.info('请选择要删除的选项')
    } else {
      Modal.confirm({
        title: '意见删除',
        content: '确定要删除所选报价吗?',
        okText: '是',
        okType: 'danger',
        cancelText: '否',
        onOk: () => {
          this.$httpRequester
            .delete(
              'company/sharesPrice/del',
              { ids },
              'application/x-www-form-urlencoded'
            )
            .then((r: any) => {
              this.$message.success('操作成功')
              this.refreshList()
            })
        }
      })
    }
  }

  //保存新增或者编辑界面
  saveQuote() {
    const form = this.form

    /*if(form.qualificationId.length==0){
            this.$message.info("请选择资质名称")
            return;
        }*/

    this.$refs.firstForm.validate((val: boolean) => {
      if (val) {
        if (this.form.id != '') {
          this.$httpRequester
            .put('company/sharesPrice/update', form)
            .then((r: any) => {
              this.$message.success('操作成功')
              this.refreshList()
            })
        } else {
          this.$httpRequester
            .post('company/sharesPrice/add', form)
            .then((r: any) => {
              this.$message.success('操作成功')
              this.refreshList()
            })
        }
        this.onClose()
        this.initQualification()
      }
    })
  }

  //搜索
  search() {
    const ids: any = []
    this.allQuali.forEach((e: any) => {
      ids.push(e.value)
    })
    this.getQuoteList({
      pageSize: this.pageSize,
      currentPage: this.pageNum,
      qualificationIds: ids.toString() //this.searchParams.qualificationId
    })
  }

  //重置
  reset() {
    /*this.searchParams = {
            qualificationId : ""
        }*/
    this.allQuali = []
    this.search()
  }

  //刷新
  refreshList() {
    this.search()
  }

  mounted() {
    this.search()
  }

  //获取资质报价详情
  getQuoteList(params = {}) {
    this.loading = true
    this.$httpRequester
      .get('company/sharesPrice/selList', params)
      .then((r: any) => {
        r.data.forEach((e: any) => {
          e.priceRange = e.lowerLimit + ' ~ ' + e.upperLimit
        })
        this.tableData = r.data
        this.total = r.totalSize
      })
    this.loading = false
  }

  //多选项事件触发
  onSelectChange(selectedRowKeys: any) {
    // console.log('selectedRowKeys changed: ', selectedRowKeys)
    this.selectedRowKeys = selectedRowKeys
  }

  //资质下拉框触发事件
  selectQualification(e: any) {
    console.log('selectQualification')
  }

  //初始化，加载资质的下拉框
  created() {
    this.initQualification()
    this.initAllQualification()
  }

  //加载未填写过的资质
  initQualification() {
    // console.log("initQualification")
    this.qualificationOptions = []
    this.$httpRequester.get('company/sharesPrice/selOption').then((r: any) => {
      r.forEach((e: any, i: number) => {
        this.qualificationOptions.push({
          title: e.qualificationName,
          label: e.qualificationName,
          value: e.qualificationName + i,
          key: e.qualificationName + i,
          selectable: false,
          checkable: false,
          children: e.qualificationLevels.map((q: any) => {
            return {
              label: e.qualificationName + q.level,
              title: q.level,
              value: q.id,
              key: q.id
            }
          })
        })
      })
    })
  }

  //加载所有资质
  initAllQualification() {
    this.$httpRequester
      .get('qualification/selQualificationAll')
      .then((r: any) => {
        // this.allQualiOptions = r.map((e: any) => {
        //   return {
        //     label: e.level,
        //     value: e.id,
        //   }
        // })
        r.forEach((e: any, i: number) => {
          this.allQualiOptions.push({
            title: e.qualificationName,
            label: e.qualificationName,
            value: e.qualificationName + i,
            key: e.qualificationName + i,
            selectable: false,
            checkable: false,
            children: e.qualificationLevels.map((q: any) => {
              return {
                label: e.qualificationName + q.level,
                title: q.level,
                value: q.id,
                key: q.id
              }
            })
          })
        })
      })
  }
}
