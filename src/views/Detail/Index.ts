import { Vue, Component, Prop, PropSync } from 'vue-property-decorator'
import {
  Icon,
  Button,
  Select,
  Popover,
  Spin,
  Modal,
  Tooltip
} from 'ant-design-vue'
import Business from './Business.vue'
import Risk from './Risk.vue'
import Assign from '../HomePage/Assign.vue'
import { userModule } from '@/store/modules/user'
import { homeModule } from '@/store/modules/home'
import { detailModule } from '@/store/modules/detail'
import BlockData from '@/components/BlockData.vue'
import qrImage from '@/assets/image/logo/qr_image.png'
import Follow from '../HomePage/Follow.vue'
import QualificationContent from './Components/Qualification.vue'
import ContactPhone from './Components/ContactPhone.vue'
import FollowList from './Components/FollowList.vue'
import back from '@/assets/image/bankrupt/back.png'
import refresh from '@/assets/image/bankrupt/ic_refresh.png'

// import qrModal from '@/components/qrCode.vue'
import AttentionModal from '../UserCenter/Components/AttentionModal.vue'
import formComponents from '@/components/Form/index.vue'
import poke from '@/assets/image/poke.png'
import qrCodePrimary from '@/assets/image/qr_code_primary.png'
import WxqrCode from '@/components/WxqrCode.vue'
import iconCopy from '@/assets/image/icon/ic_copy.png'
import copyIcon from '@/components/icons/copy.vue'
import low from '@/assets/image/menuIcon/ic_danger_low.png'
import hight from '@/assets/image/menuIcon/ic_danger_hight.png'
import centre from '@/assets/image/menuIcon/ic_danger_centre.png'
@Component({
  components: {
    aTooltip: Tooltip,
    aPopover: Popover,
    aSelect: Select,
    aSelectOption: Select.Option,
    aIcon: Icon,
    aButton: Button,
    Business,
    Risk,
    Assign,
    BlockData,
    Follow,
    aSpin: Spin,
    QualificationContent,
    ContactPhone,
    FollowList,
    // qrModal,
    AttentionModal,
    formComponents,
    WxqrCode
  }
})
export default class Detail extends Vue {
  $refs!: {
    assignModal: any
    followRef: any
    qr: any
    aM: any
    wxqr: any
    communicationForm: any
  }
  @Prop() isFromClient?: false
  copyIcon = copyIcon
  iconCopy = iconCopy
  qrCodePrimary = qrCodePrimary
  tableItemDefauletQua = [] as any
  qrImage = qrImage
  refresh = refresh
  back = back
  titleSelectIndex = 0
  showLoading = false
  enterpriseId = -1
  poke = poke
  currentDate = ''
  qualifications = []
  contactList = { all: [], mobile: [], phone: [] }
  followList = []
  businessInfo = [] as any
  isAttention = false
  attentionId = -1
  clueSources = 0 // 跟进来源
  // isReceive = false
  isSuperAdmin = userModule.roleLevel === 1
  isAdmin = userModule.roleLevel === 2
  isSales = userModule.roleLevel === 4
  isLead = userModule.isLead
  statusList = [
    { value: 'INTENTION', text: '有意向', disabled: false },
    { value: 'NO_INTENTION', text: '无意向', disabled: false },
    { value: 'DEAL', text: '已成交', disabled: false },
    { value: 'NO_DEAL', text: '未成交', disabled: false },
    { value: 'UNCONFIRMED', text: '未确认', disabled: true }
  ]
  followShow = false
  // isFromClient = false
  shareDisplay = false
  qrCode = ''
  followQualication: any = []
  riskLevel = ''
  wxQrCode = ''

  get mainTitleList() {
    return [
      { title: '企业资质', component: QualificationContent },
      {
        title: '联系电话',
        component:
          this.basicInfo.haveReceive || this.isSuperAdmin || this.isFromClient
            ? ContactPhone
            : BlockData
      },
      { title: '工商信息', component: Business },
      { title: '风险信息', component: Risk },
      { title: '跟进记录', component: FollowList }
    ]
  }

  get basicInfo() {
    return detailModule.enterpriseItem || ({} as any)
  }

  phoneIsTurnOn() {
    if (!homeModule.defaultQua) {
      homeModule.setATurnOnDefaultQua(this.tableItemDefauletQua)
    }
    //  homeModule.setTurnOnItem(this.basicInfo)
    homeModule.changeTurnPhoneStatus(true)
    homeModule.setPageHandleFunction(this.afterTurnOnHandle)
  }

  phoneIsNotTurnOn() {
    // homeModule.setTurnOnItem(this.basicInfo)
    homeModule.changeNotTurnPhoneStatus(true)
    homeModule.setPageHandleFunction(this.afterNotTurnOnHandle)
  }

  afterTurnOnHandle() {
    this.getFollowList()
    this.selectMainTitle(4)
    this.$emit('havePhoneOperate')
  }

  afterNotTurnOnHandle() {
    this.getFollowList()
    this.selectMainTitle(4)
    // this.basicInfo.haveReceive = false
    detailModule.changeEnterpriseKey({ prop: 'haveReceive', key: false })
    this.$emit('havePhoneOperate')
  }

  copyThisText(text: any) {
    copyThisText(text)
    this.$message.success('复制成功')
  }
  // 关闭
  handleCancel() {
    this.shareDisplay = false
  }

  // 关闭
  handleCancel1() {
    this.followShow = false
  }

  setColorFromRisk(riskLevel: any) {
    this.riskLevel = riskLevel
  }

  selectMainTitle(index: number) {
    this.titleSelectIndex = index
  }

  setBasicInfoReback() {
    this.titleSelectIndex = 0
  }

  assignSuccess() {
    this.getFollowList()
  }

  setLegalPersonToFilter(person: any) {
    this.$emit('setLegalPersonToFilter', person)
  }

  //
  setInvestmentCompany(per: any) {
    this.$emit('setLegalPersonToFilter', per)
  }

  setPhoneToFilter(phone: any) {
    this.$emit('setPhoneToFilter', phone)
  }

  showSharQrCode(basicInfo: any) {
    if (this.isSuperAdmin) {
      return
    }

    const host = 'http://192.168.0.108:9005/api/'
    const page = 'pages/detail/enterpriseDetail'
    const link =
      VUE_APP_TYPE === 'online' ? 'http://www.xnjx666.com:8888/api/' : host
    const pageUrl = `${apiUrl}wx/shared/applet/detail/qrcode?id=${basicInfo.enterpriseId}&page=${page}&width=200`
    this.wxQrCode = pageUrl
    this.$refs.wxqr.showModal()
  }

  receiveEmterprice() {
    const params = {
      clueSources: this.clueSources,
      // enterpriseName:
      //   this.basicInfo.companyName || this.basicInfo.enterpriseName,
      enterpriseIds: [this.enterpriseId]
    }

    this.$httpRequester
      .post('enterpriseClue/addReceive', params)
      .then((r: any) => {
        const phoneArray = Object.values(r)
        let phoneList = [] as any
        phoneArray.forEach((e: any) => {
          phoneList = e
        })
        detailModule.changeEnterpriseKey({ prop: 'haveReceive', key: true })
        detailModule.changeEnterpriseKey({ prop: 'phoneList', key: phoneList })
        this.enterpriseContact()
        this.getFollowList()
        this.$emit('detailReceive', params)
        // detailModule.changeEnterpriseKey({
        //   prop: 'receiveCount',
        //   key: this.basicInfo.receiveCount + 1,
        // })
        // this.basicInfo.haveReceive = true
        // this.basicInfo.receiveCount += 1
        // console.log('this.way ', this.basicInfo)
        // localStorage.setItem('einterpriseInfo', JSON.stringify(this.basicInfo))
        //  this.getFollowList()
        //  this.$emit('detailReceive', r)
        this.$message.success('查看成功')
        homeModule.getFreeTime()
      })
      .catch((err: any) => {
        // this.basicInfo.haveReceive = false
        detailModule.changeEnterpriseKey({ prop: 'haveReceive', key: false })
        // localStorage.setItem('einterpriseInfo', JSON.stringify(this.basicInfo))

        if (err.code === '5003') {
          // console.log(err.msg)
          Modal.info({
            title: '温馨提示',
            content: err.msg,
            okText: '确定',
            onOk: () => {
              // this.setReceiveDataInTable(err.data)
              console.log('is ok')
            }
          })
        } else if (err.code === '5008') {
          detailModule.setGoToVipTeamModalShow(true)
        } else if (err.code === '5013') {
          detailModule.setGoToVipModalShow(true)
        } else if (err.code === '2005') {
          detailModule.setExpriedModalShow(true)
        }
      })
  }

  addNewAssign() {
    this.followShow = true
    let qua = [] as any
    let lastFollow = {}
    let onlyDesign = false
    let onlyConstruction = true
    this.qualifications.forEach((e: any) => {
      if (e.industryName.indexOf('设计') > -1) {
        onlyDesign = e.num > 0
      } else {
        onlyConstruction = e.num > 0
      }
      e.certificates.forEach((c: any) => {
        qua = qua.concat(c.items)
      })
    })
    homeModule.setRowQuaStatus({ onlyDesign, onlyConstruction })

    this.$nextTick(() => {
      const params = {
        enterpriseId: this.enterpriseId,
        clueSources: this.clueSources,
        latest: true
      }
      this.$httpRequester
        .get('followRecord/selFollowRecordByEnterpriseId', params)
        .then((res: any) => {
          lastFollow = res[0].selFollowRecordVO[0]
          qua = qua.filter((q: any) => {
            return q.id
          })
          this.$refs.followRef.setFollowDefault({
            qua,
            lastFollow,
            clueSources: this.clueSources
          })
        })
    })
    // const qua = this.qualifications.map((e: any) => {
    //   return {
    //     ...e.items.map((i: any) => {
    //       return { ...i }
    //     })
    //   }
    // })
  }

  addFollow() {
    const follwData = this.$refs.followRef.getFollowModal()

    if (!follwData) {
      return
    }

    const params = {
      ...follwData,
      enterpriseId: this.enterpriseId,
      clueSources: this.clueSources,
      enterpriseName:
        this.basicInfo.companyName || this.basicInfo.enterpriseName
    }

    params.qualificationItemIds = params.qualificationItemIds.join(',')
    this.$httpRequester
      .post('followRecord/addFollowRecord', params)
      .then(() => {
        this.$message.success('操作成功')
        // this.$refs.followRef.setFollowDefault()
        this.setNewStatusInpage(follwData.followStatus)
        this.getFollowList()
        this.closeFollowDrawer()
        this.selectMainTitle(4)
        this.$emit('followStatusChange')
      })
      .catch(() => {
        this.$refs.followRef.setFollowDefault()
      })
  }

  closeFollowDrawer() {
    this.followShow = false
  }

  setSelectStaff(item: any) {
    // console.log(item)
    this.basicInfo.memberName = item.label
  }

  showAssignModal() {
    this.$refs.assignModal.showModal()
  }

  changeItemStatus(value: any) {
    // const params = {
    //   enterpriseId: this.basicInfo.enterpriseId,
    //   enterpriseName:
    //     this.basicInfo.companyName || this.basicInfo.enterpriseName,
    //   followStatus: value
    // }
    // this.$httpRequester
    //   .post('followRecord/addFollowRecord', params)
    //   .then(() => {
    //     this.setNewStatusInpage(value)
    //     this.statusList.forEach((s: any) => {
    //       if (s.value === value) {
    //         s.disabled = true
    //       } else {
    //         s.disabled = false
    //       }
    //     })
    //     this.$message.success('操作成功')
    //     if (value === '2' || value === '4') {
    //       this.$router.replace({
    //         name: 'detail',
    //         params: { id: this.enterpriseId.toString() }
    //       })
    //       //  location.reload()
    //     }
    //   })
  }

  setNewStatusInpage(value: string) {
    const find = this.statusList.find((e: any) => e.value === value)
    // console.log(find)
    this.basicInfo.followStatus = find ? find.text : ''
  }

  getQualification() {
    const params = {
      enterpriseId: this.enterpriseId
    }
    this.qualifications = []
    this.$httpRequester
      .get('qualification/selQualificationByEnterpriseId', params)
      .then((r: any) => {
        r.forEach((e: any) => {
          e.num = 0
          if (e.certificates) {
            if (!e.certificates[0].certificateType) {
              e.certificates = []
            } else {
              e.certificates.forEach((c: any) => {
                e.num += c.qualificationNum
              })
            }
          } else {
            e.certificates = []
          }

          // this.followQualication.push(...e.items)
        })
        this.qualifications = r

        // console.log(this.followQualication)
      })
      .catch((err: any) => {
        if (err === 'noqualification') {
          this.qualifications = []
        }
      })
  }

  setRiskLevelFilter(val: any) {
    this.$emit('setRiskLevelFilter', val)
  }

  enterpriseContact() {
    const params = {
      enterpriseId: this.enterpriseId
    }
    this.contactList = { all: [], mobile: [], phone: [] }
    if (this.basicInfo.isNotHavePhone) {
      return
    }
    this.$httpRequester
      .get('business/baseInfo/selEnterprisePhoneByEnterpriseId', params)
      .then((r: any) => {
        this.contactList.all = r
        this.contactList.mobile = r.filter((e: any) => e.contactsType === '1')
        this.contactList.phone = r.filter((e: any) => e.contactsType === '2')
        // console.log(this.contactList)
      })
  }

  async getFollowList() {
    this.followList = []
    await this.$httpRequester
      .get('followRecord/selFollowRecordByEnterpriseId', {
        enterpriseId: this.enterpriseId,
        clueSources: this.clueSources,
        latest: false
      })
      .then((r: any) => {
        this.followList = r
      })
      .catch((err: any) => {
        detailModule.setLookFollowListCodeData(err)
      })
    // .then((r: any) => {
    //     this.followList = r
    // })
  }

  async getBusinessInfo() {
    const r = (await this.$httpRequester.get(
      'business/baseInfo/selBusinessInfoById',
      { id: this.enterpriseId }
    )) as any
    this.businessInfo = r || {}
    this.businessInfo.beforeNameText = r.beforeNameList.join('、')
    const riskDetail = [
      {
        title: '无风险',
        icon: low,
        color: 'sysPrimary',
        detail: '企业经营状况良好'
      },
      {
        title: '低风险',
        icon: low,
        color: 'sysPrimary',
        detail: '企业存在工商异常、行政处罚等信息'
      },
      {
        title: '中风险',
        icon: centre,
        color: 'tipOrange',
        detail: '企业存在被执行信息'
      },
      {
        title: '高风险',
        icon: hight,
        color: '[#F30C0C]',
        detail: '企业存在失信被执行信息'
      }
    ]
    this.businessInfo.riskText = riskDetail[r.riskLevel].title
    this.businessInfo.riskIcon = riskDetail[r.riskLevel].icon
    this.businessInfo.riskColor = riskDetail[r.riskLevel].color
  }

  // async judgmentIsAttention() {
  //   const r = (await this.$httpRequester.get('attention/attentionStatus', {
  //     enterpriseId: this.enterpriseId,
  //   })) as any
  //   this.isAttention = r.status
  //   this.attentionId = r.attentionId
  //   //     console.log(r)
  // }
  changeEnterpriseAttention() {
    if (this.isAttention) {
      // 取消关注企业
      Modal.confirm({
        title: '温馨提示',
        content: '真的要将这个企业移除关注列表吗？',
        okText: '确认',
        cancelText: '取消',
        onOk: () => {
          this.$httpRequester
            .delete('attention/id', this.attentionId)
            .then(() => {
              this.$message.success('操作成功')
              this.isAttention = false
            })
        }
      })
    } else {
      this.$refs.aM.showModal()
      //去关注企业
    }
  }

  attentionToGroup(id: any) {
    this.$httpRequester
      .post('attention', {
        enterpriseId: this.enterpriseId,
        tagId: id
      })
      .then((r: any) => {
        this.$message.success('关注成功')
        this.$refs.aM.handleCancel()
        this.isAttention = true
        this.attentionId = r
      })
  }

  goBack() {
    this.$emit('closeDrawer')
  }

  refreshData() {
    this.showLoading = true
    // this.getAllData()
    this.getBusinessInfo() // 企业信息
    const params = {
      companyName:
        this.basicInfo.companyName || this.basicInfo.enterpriseName || ' ',
      socialCreditCode: this.businessInfo.socialCreditCode || ' ',
      areaCode: this.businessInfo.areaCode || ' '
    }
    this.$httpRequester
      .get('business/baseInfo/captureDetails', params)
      .then(() => {
        this.showLoading = false
        this.$message.success('正在获取最新数据，请稍后再来查看')
      })
      .catch(() => {
        this.showLoading = false
      })
    // this.getCaptureDetails()//实时抓取信息
    // setTimeout(() => {
    //
    // }, 1000)
  }

  async getAllData() {
    this.showLoading = true
    this.enterpriseId = this.basicInfo.enterpriseId

    // console.log(this.basicInfo)
    // parseInt(this.$route.params.id, 10)
    // this.isFromClient = !!parseInt(this.$route.query.isFromClient as string, 10)
    this.currentDate = this.$moment().format('yyyy/MM/DD')
    // this.basicInfo = JSON.parse(localStorage.getItem('einterpriseInfo') as string)
    // console.log(this.basicInfo)
    await this.getQualification() // 资质信息
    if (this.basicInfo.haveReceive || this.isFromClient) {
      await this.enterpriseContact() // 企业联系方式
    }
    if (this.basicInfo.isNotHavePhone) {
      // 没有电话
      await this.enterpriseContact()
      detailModule.changeEnterpriseKey({ prop: 'haveReceive', key: true })
    }
    await this.getBusinessInfo() // 企业信息
    // await this.judgmentIsAttention() //判断是否关注
    await this.getFollowList() // 跟进记录
    this.showLoading = false
    this.$nextTick(() => {
      const findIndex = this.statusList.findIndex(
        (s: any) => s.value === this.basicInfo.followStatusCode
      )
      if (findIndex > -1) {
        this.statusList[findIndex].disabled = true
      }
      this.$set(this.statusList, findIndex, this.statusList[findIndex])
    })
  }

  setBasicInfo(defaultIndex: any) {
    //  this.basicInfo = item
    // // console.log(this.isFromClient)

    if (!this.isFromClient) {
      if (this.$route.name === 'shipment') {
        this.clueSources = 2
      } else if (this.$route.name === 'receipt') {
        this.clueSources = 1
      }
    } else {
      this.clueSources = this.basicInfo.clueSources
    }
    this.titleSelectIndex = defaultIndex ? defaultIndex : 0
    this.getAllData().catch(() => {
      this.showLoading = false
    })
  }

  mounted() {
    // console.log(this.otherInfo)
    // this.$nextTick(() => {
    //     this.getAllData()
    // })
  }

  destroyed() {
    detailModule.setLookFollowListCodeData({})
  }
}
