import { Component, Vue, Prop } from 'vue-property-decorator'
import CustomerTable from '@/components/CustomerTable.vue'

@Component({
  components: {
    CustomerTable
  }
})
export default class Business extends Vue {
  @Prop() enterpriseId?: ''
  @Prop() businessInfo?: {}

  bussinessTable: any = [{ td: [] }]
  shareholderTh = [
    {
      title: '股东',
      key: 'shareholderName'
    },
    {
      title: '股东类型',
      key: 'shareholderType'
    },
    // {
    //   title: '持股比例',
    //   key: 'shareholdingRatio',
    // },
    {
      title: '认缴金额',
      key: 'subscribedAmount'
    },
    {
      title: '认缴出资日期',
      key: 'contributionDate'
    }
  ]
  shareholderTd: any = []
  mainPersonTh = [
    {
      title: '序号',
      key: 'index'
    },
    {
      title: '姓名',
      key: 'name'
    },
    {
      title: '职位',
      key: 'position'
    }
  ]
  mainPersonTd: any = []
  investmentTh = [
    {
      title: '被投资企业名',
      key: 'investmentEnterpriseName',
      slotText: 'investmentEnterpriseName'
    },
    { title: '法定代表人', key: 'legalPerson' },
    { title: '注册资本', key: 'capital' },
    // { title: '出资比例', key: 'proportion' },
    { title: '成立时间', key: 'establishmentDate' },
    { title: '经营状态', key: 'businessStatus' }
  ]
  investmentTd: any = []
  branchList: any = []
  manageChangeTh = [
    {
      title: '序号',
      key: 'index',
      width: '40px'
    },
    {
      title: '变更日期',
      key: 'changeDate',
      width: '100px'
    },
    {
      title: '变更事项',
      key: 'changeItem',
      width: '120px'
    },
    {
      title: '变更前',
      key: 'changePre'
    },
    {
      title: '变更后',
      key: 'changeAfter'
    }
  ]
  manageChangeTd: any = []
  getEnterpriseDetail() {
    // console.log('11', this.enterpriseId)
  }
  setLegalPerson(par: any) {
    this.$emit('setInvestmentCompany', par)
  }
  getManageChangeList() {
    const params = {
      currentPage: 1,
      pageSize: 200,
      enterpriseId: this.enterpriseId
    }
    this.$httpRequester
      .get('business/baseInfo/selManageChangeByEnterpriseId', params)
      .then((r: any) => {
        this.manageChangeTd = r.data || []
      })
  }
  getBranchOriganizationList() {
    const params = {
      enterpriseId: this.enterpriseId
    }
    this.$httpRequester
      .get('business/baseInfo/selBranchOrganizeByEnterpriseId', params)
      .then((r: any) => {
        this.branchList = r || []
        // this.branchList = [{ branchName: 'お疲れ様です。' }, { branchName: 'また明日' }]
      })
  }
  getInvestment() {
    const params = {
      currentPage: 1,
      pageSize: 200,
      enterpriseId: this.enterpriseId
    }
    this.$httpRequester
      .get('business/baseInfo/selForeignInvestmentByEnterpriseId', params)
      .then((r: any) => {
        this.investmentTd = r.data || []
      })
  }
  getMainPersonList() {
    const params = {
      currentPage: 1,
      pageSize: 200,
      enterpriseId: this.enterpriseId
    }
    this.$httpRequester
      .get('business/baseInfo/selMainPersonByEnterpriseId', params)
      .then((r: any) => {
        this.mainPersonTd = r.data || []
      })
  }
  getShareholderList() {
    const params = {
      currentPage: 1,
      pageSize: 200,
      enterpriseId: this.enterpriseId
    }
    this.$httpRequester
      .get('business/baseInfo/selShareholderByEnterpriseId', params)
      .then((r: any) => {
        this.shareholderTd = r.data || []
        // this.shareholderTd = [{ shareholderName: 'zege', shareholdingRatio: '100%', subscribedAmount: '100000w', contributionDate: '2019-03-04' }]
      })
  }
  setBusiness() {
    // const params = {
    //   id: this.enterpriseId,
    // }
    // this.$httpRequester
    //   .get('business/baseInfo/selBusinessInfoById', params)
    //   .then((r: any) => {
    const r = this.businessInfo as any
    this.bussinessTable = [
      {
        td: [
          { title: '统一社会信用代码' },
          { title: r.socialCreditCode || '暂无数据' },
          { title: '经营状态' },
          { title: r.businessStatus || '暂无数据' }
          // { title: '注册号' }, { title: r.registrationNo || '暂无数据' }
        ]
      },
      // {
      //     td: [
      //         // { title: '组织机构代码' }, { title: r.organizationCode || '暂无数据' },
      //         { title: '经营状态' }, { title: r.businessStatus || '暂无数据' }]
      // },
      {
        td: [
          { title: '法定代表人' },
          { title: r.legalPerson || '暂无数据' },
          { title: '注册资本' },
          { title: r.registeredCapital || '暂无数据' }
        ]
      },
      {
        td: [
          { title: '公司类型' },
          { title: r.companyType || '暂无数据' },
          { title: '成立日期' },
          { title: r.establishmentDate || '暂无数据' }
        ]
      },
      {
        td: [
          { title: '营业期限' },
          { title: r.businessTerm || '--' },
          { title: '登记机关' },
          { title: r.registrationAuthority || '暂无数据' }
        ]
      },
      {
        td: [
          { title: '核准日期' },
          { title: r.approvalDate || '暂无数据' ,colspan: 3},
          // { title: '公司规模' },
          // { title: r.companySize || '暂无数据' }
        ]
      },
      // {
      //   td: [
      //     { title: '所属行业' },
      //     { title: r.industry || '暂无数据', colspan: 3 }
      //   ]
      // },
      {
        td: [
          { title: '企业地址' },
          { title: r.businessAddress || '暂无数据', colspan: 3 }
        ]
      },
      {
        td: [
          { title: '经营范围' },
          { title: r.businessScope || '暂无数据', colspan: 3 }
        ]
      }
    ]
    //  })
  }
  mounted() {
    this.$nextTick(() => {
      this.getEnterpriseDetail()
      this.setBusiness() // 工商信息 公司简介
      this.getShareholderList() // 股东信息
      this.getMainPersonList() // 获取主要员工
      this.getInvestment() // 获取对外投资
      this.getBranchOriganizationList() // 获取分支机构
      this.getManageChangeList() // 工商变更
    })
  }
}
