import { Component, Vue, Prop } from 'vue-property-decorator'
import CustomerTable from '@/components/CustomerTable.vue'
import brakeFaithDetail from './Components/brakeFaithDetail.vue'

@Component({
  components: {
    CustomerTable,
    brakeFaithDetail
  }
})
export default class Risk extends Vue {
  @Prop() enterpriseId: any
  @Prop() riskLevel: any
  get defaultParams() {
    return {
      enterpriseId: this.enterpriseId,
      rows: 200,
      page: 1
    }
  }
  // 工商异常
  businessErrorTh = [
    { title: '序号', key: 'index', width: '40' },
    { title: '被列入日期', key: 'includedDate', width: '80' },
    { title: '列入经营异常名录原因', key: 'inExceptionReason', width: '140' },
    { title: '列入决定机关', key: 'inDecisionOffice', width: '110' },
    { title: '移除日期', key: 'removeDate', width: '70' },
    { title: '移除经营异常名单原因', key: 'removeExceptionReason' },
    { title: '移出决定机关', key: 'removeDecisionOffice', width: '110' }
  ]
  businessErrorTd: any = []
  businessExceptionCount = 0 as any
  // 失信
  breakFaithTh = [
    { title: '序号', key: 'index', width: '40' },
    { title: '案号', key: 'caseNumber' },
    { title: '执行法院', key: 'court' },
    // { title: '执行依据文号', key: 'caseNumber' },
    { title: '履行情况', key: 'performance' },
    { title: '立案日期', key: 'filingDate', width: '100' },
    // { title: 'id', key: 'id', width: '70' },
    { title: '操作', key: 'operate', slot: brakeFaithDetail }
  ]
  breakFaithTd: any = []
  // 执行人
  executionTh = [
    { title: '序号', key: 'index', width: '40' },
    { title: '案号', key: 'caseNumber' },
    { title: '立案日期', key: 'createCaseDate', width: '100' },
    { title: '执行法院', key: 'executionCourt' },
    { title: '执行标的(元)', key: 'executionTarget' }
    // { title: '案件身份', key: 'courtName' },
  ]
  executionTd: any = []
  // 裁判文书
  refereeTh = [
    { title: '序号', key: 'index', width: '40' },
    { title: '裁判文书', key: 'judgmentTitle' },
    { title: '案由', key: 'causeAction' },
    { title: '发布日期', key: 'releaseTime', width: '100' },
    { title: '案号', key: 'caseNo' },
    { title: '案件身份', key: 'caseStatus' },
    { title: '法院名称', key: 'courtName' }
  ]
  refereeTd: any = []
  // 行政处罚
  penaltiesTh = [
    { title: '序号', key: 'index', width: '40' },
    { title: '公示日期', key: 'publicityDate', width: '100' },
    { title: '决定文书号', key: 'decisionDocumentNo' },
    { title: '行政处罚内容', key: 'administrativePunishment' },
    { title: '违法行为类型', key: 'violationsType' },
    { title: '决定日期', key: 'decisionDate', width: '100' },
    { title: '决定机关', key: 'decisionOffice' }
  ]
  penaltiesTd: any = []
  getBusinessErrorList() {
    this.$httpRequester
      .get(
        'business/riskinformation/queryBusinessExceptionByPage',
        this.defaultParams
      )
      .then((r: any) => {
        // console.log('工商异常',r)
        if (r.businessExceptionVO.list) {
          this.businessErrorTd = r.businessExceptionVO.list
        } else {
          this.businessErrorTd = []
        }
        this.businessExceptionCount = r.businessExceptionCount
      })
  }
  //  企业失信
  getBreakFaithList() {
    this.$httpRequester
      .get(
        'business/riskinformation/queryBreakfaithInfoByPage',
        this.defaultParams
      )
      .then((r: any) => {
        // console.log('失信list',r)
        this.breakFaithTd = r.records
      })
  }
  //被执行人
  getExecutionPeople() {
    this.$httpRequester
      .get(
        'business/riskinformation/queryExecutedPersonByPage',
        this.defaultParams
      )
      .then((r: any) => {
        this.executionTd = r.records
      })
  }
  getRefereeList() {
    this.$httpRequester
      .get(
        'business/riskinformation/queryJudgmentDocumentByPage',
        this.defaultParams
      )
      .then((r: any) => {
        this.refereeTd = r.records
      })
  }
  getPenalties() {
    this.$httpRequester
      .get(
        'business/riskinformation/queryPoliticsPenalizeByPage',
        this.defaultParams
      )
      .then((r: any) => {
        this.penaltiesTd = r.records
      })
  }
  mounted() {
    this.$nextTick(() => {
      // console.log(this.enterpriseId)
      this.getBusinessErrorList() // 工商异常
      this.getBreakFaithList() //  企业失信
      this.getExecutionPeople() // 被执行人
      this.getRefereeList() // 裁判文书
      this.getPenalties() // 行政处罚
    })
  }
}
