import {Component, Prop, Vue} from 'vue-property-decorator'
import { Steps, Cascader, FormModel, Input, Modal, Spin } from 'ant-design-vue'
import ChangePwd from './ChangePwd.vue'
// import BgImage from '@/assets/image/loginBg.png'
import jsonp from 'jsonp'
import logo from '@/assets/svg/logo.svg'
import { JSEncrypt } from 'jsencrypt'
import successBtn from '@/assets/image/goToLoginBg.png'
import successIcon from '@/assets/image/loginSuccessIcon.png'

@Component({
  components: {
    aSteps: Steps,
    aStep: Steps.Step,
    aCascader: Cascader,
    aFormModel: FormModel,
    aInputPassword:Input.Password,
    aInput: Input,
    aSpin: Spin,
    aModal: Modal,
    ChangePwd,
    aFormModelItem: FormModel.Item
  }
})
export default class Registered extends Vue {
  $refs!: {
    firstForm: any
    secondForm: any
  }
  @Prop({ default: false, type: Boolean }) isFormLoginModal?: boolean
  successBtn = successBtn
  successIcon = successIcon
  logo = logo
  isLoading = false
  currentStep = 0
  cityList = []
  city = []
  //注册公司名称输入中文
  inputValue = ''
  // BgImage = BgImage
  isChagePwd = true
  codeDisabled = false
  provinceName = ''
  cityName = ''
  registerModel = {
    city: [],
    companyName: '',
    memberName: '',
    userAccount: '',
    verifycode: '',
    password: '',
    validPassword: ''
  }
  publicKey = ''
  codeSIV: any = null
  validMobile(rule: any, value: any, callback: any) {
    const mobileTest = /^1(3|4|5|6|7|8|9)\d{9}$/
    if (value === '') {
      callback(new Error('请输入手机号码'))
    } else if (!mobileTest.test(value)) {
      callback(new Error('请输入正确的手机号码'))
    } else {
      callback()
    }
  }
  validPassword(rule: any, value: any, callback: any) {
    const pwdTest = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,12}$/
    if (!pwdTest.test(value)) {
      callback(new Error('密码只能是6-12位的数字+字母组合'))
    } else {
      callback()
    }
  }
  validPasswordConfirm(rule: any, value: any, callback: any) {
    // console.log('passd', value)
    if (value !== this.registerModel.password) {
      callback(new Error('两次密码输入不一致'))
    } else {
      callback()
    }
  }
  campanyValid(rule: any, value: any, callback: any) {
    const reg = new RegExp("^[\u4E00-\u9FFF]+$");
    if (value.trim().length === 0) {
      callback(new Error('名称不合法，公司名称不能为空'))
    } else if (value.length > 50) {
      callback(new Error('名称不合法，公司名称不得超过50位'))
    } else if (!reg.test(value)) {
      callback(new Error('名称不合法，公司名称只能为中文字符'))
    } else {
      callback()
    }
  }
  validMemberName(rule: any, value: any, callback: any) {
    const reg = new RegExp("^[\u4E00-\u9FFF]+$");
    if (value.trim().length < 2) {
      callback(new Error('姓名过短,姓名至少为2位'))
    } else if (value.length > 10) {
      callback(new Error('姓名过长，姓名不能超过10位'))
    } else if (!reg.test(value)) {
      callback(new Error('用户姓名只能为中文字符'))
    } else {
      callback()
    }
  }
  registerRules = {
    companyName: [
      {
        required: true,
        validator: this.campanyValid,
        // message: '请输入公司名称',
        trigger: 'change'
      }
    ],
    city: [{ required: true, message: '请选择所在区域', trigger: 'input' }],
    memberName: [
      { required: true, validator: this.validMemberName, trigger: 'input' }
    ],
    userAccount: [
      { validator: this.validMobile, required: true, trigger: 'input' }
    ],
    verifycode: [{ required: true, message: '请输入验证码', trigger: 'input' }],
    password: [
      { validator: this.validPassword, required: true, trigger: 'input' }
    ],
    validPassword: [
      {
        validator: this.validPasswordConfirm,
        required: true,
        trigger: 'input'
      }
    ]
    // password：[
    //   {validator: this.validPassword,required: true,trigger:'input'}
    // ]
  }
  codeBtnText = '获取验证码'
  get btText() {
    return this.currentStep < 2 ? '下一步' : '去登录'
  }
  backToLogin() {
    if (this.isFormLoginModal){
      this.$emit('backToLogin')
    }else {
      this.$router.push({
        name: 'login'
      })
    }

  }
  getCityRegion() {
    this.isLoading = true
    this.$httpRequester.get('regionCode/selList').then((r: any) => {
      this.cityList = r
      this.getLocaltionFromServe()
      // this.getCurrentLocation()
    })
  }
  getLocaltionFromServe() {
    this.$httpRequester.get('areaIp/get').then((res: any) => {
      if (!res) {
        this.getCurrentLocation()
      } else {
        this.provinceName = res.province
        this.cityName = res.city
        this.findProvinceAndCity()
      }
    })
  }
  getCurrentLocation() {
    const baiduApi =
      'http://api.map.baidu.com/location/ip?ak=fGC7lrCC48g76vMdqsMQb34I8bXnx9jp&sn=d778766514d7fc4009ff0a7fc153d1e7'
    jsonp(baiduApi, null, (err: any, data: any) => {
      if (err) {
        this.$message.error(err)
      } else {
        this.provinceName = data.content.address_detail.province
        this.cityName = data.content.address_detail.city
        this.findProvinceAndCity()
      }
    })
  }
  findProvinceAndCity() {
    // console.log(this.provinceName)
    const findProvince = this.cityList.find((c: any) => {
      return c.areaName.indexOf(this.provinceName) > -1
    }) as any
    let findCity = null
    const specialCity = ['北京', '上海', '重庆', '天津']
    let isSpecial = false as any
    if (findProvince) {
      isSpecial = specialCity.find((e: any) => {
        return findProvince.areaName.indexOf(e) > -1
      })
      if (isSpecial) {
        findCity = findProvince.cityList[0]
      } else {
        findCity =
          findProvince.cityList.find((c: any) => {
            return this.cityName && c.areaName.indexOf(this.cityName) > -1
          }) || {}
      }
    }
    this.registerModel.city = findCity.areaName
      ? ([findProvince.areaName, findCity.areaName] as any)
      : [findProvince.areaName]
    this.isLoading = false
  }
  setVerifycode() {
    // console.log(this.codeSIV)
    if (this.codeSIV) {
      return
    }
    this.$refs.secondForm.validateField('userAccount', (message: string) => {
      if (!message) {
        this.$httpRequester
          .post(
            'user/sms/verificte/send',
            { cellPhone: this.registerModel.userAccount },
            'application/x-www-form-urlencoded'
          )
          .then((r: any) => {
            this.codeDisabled = true
            let time = 120
            this.codeSIV = setInterval(() => {
              time -= 1
              this.codeBtnText = `重新发送(${time}S)`
              if (time === 1) {
                this.codeBtnText = '获取验证码'
                this.codeDisabled = false
                clearInterval(this.codeSIV)
                this.codeSIV = null
              }
            }, 1000)
          })
      }
    })
  }

  nextSteps() {
    this.$nextTick(() => {
      if (this.currentStep === 0) {
        this.$formValidate.listValidata(
          this.$refs.firstForm,
          ['companyName', 'city'],
          (val: boolean) => {
            if (val) {
              Modal.confirm({
                title: '温馨提示',
                icon: 'exclamation-circle',
                centered: true,
                content: (h: any) => {
                  return h('div', [
                    h(
                      'span',
                      {},
                      '使用地区一旦选择，不可更改！后续系统的使用都与选择的使用地区有关，'
                    ),
                    h(
                      'span',
                      {
                        style: {
                          color: '#f5222d',
                          fontWeight: 500
                        }
                      },
                      '请谨慎选择！'
                    )
                  ])
                },
                okText: '确认',
                cancelText: '取消',
                onOk: () => {
                  this.currentStep += 1
                },
                onCancel: () => {
                  console.log('操作取消')
                }
              })
            }
          }
        )
      } else if (this.currentStep === 1) {
        this.$formValidate.listValidata(
          this.$refs.secondForm,
          [
            'memberName',
            'userAccount',
            'verifycode',
            'password',
            'validPassword'
          ],
          (val: boolean) => {
            if (val) {
              this.creatNewUser()
            }
          }
        )
        // this.$refs.secondForm.validate((val: boolean) => {
        //   if (val) {
        //     this.creatNewUser()
        //   }
        // })
      } else {
        if (this.isFormLoginModal){
          this.$emit('nextSteps')
        }else {
          this.$router.push({
            name: 'login'
          })
        }
      }
    })
  }
  backPreviousStep() {
    if (this.currentStep === 1) {
      this.$refs.secondForm.clearValidate()
    }
    this.currentStep -= 1
  }
  selectCity(value: any) {
    this.registerModel.city = value
    // console.log(value)
  }
  creatNewUser() {
    // this.getPublicKey().then(() => {
    const params = {
      registerSource: 1,
      companyName: this.registerModel.companyName,
      memberName: this.registerModel.memberName,
      userAccount: this.registerModel.userAccount,
      verifyCode: this.registerModel.verifycode,
      useAreaName: this.registerModel.city.join('/'),
      // userPassword: this.registerModel.password,
      userPassword: this.setPasswordRSA()
    }
    this.$httpRequester.post('user/register', params).then(() => {
      this.currentStep += 1
    })
    // })
  }
  setPasswordRSA() {
    const encryptor = new JSEncrypt()
    // 设置公钥
    encryptor.setPublicKey(this.publicKey)
    // 加密数据
    return encryptor.encrypt(this.registerModel.password)
  }
  getPublicKey() {
    this.publicKey =
      'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoUSnVj2D7J0WECHHF/rrjbO/x692gNJUhtYD/6Te2NKOW+0Hjq5lkk6lz5zO80Bikv9aCk2bDTDkh2xuUugkP26d2ZYcQCKibc2+di1AX6M56IJlKdnyY5gXM+L5bv6urGfpLqf1X1XJNoJP+WAUliA5RtBI7S1f+4zwycmm86HQazPVsARQGX2Bm8fvxNMlYxRM5rSzrZjbCYwL70/KVpow5mXr/n9N+zk+WnkXWq8UlkBd2KS/bsQbXJ3my/fkk2nWOZvpEyBdekYykrv4iw9x/9v/4bloRJvy6+Dbb4TZo0TxsGEUHIhHJ8kQ5gKPih32TdCSPGyUwSgYcWLSHwIDAQAB'
  }
  created() {
    this.getCityRegion()
  }
  mounted() {
    // this.getCityRegion()
    this.getPublicKey()
    this.$nextTick(() => {
      this.isChagePwd = !!this.$route.query.isFindPwd
    })
  }
}
