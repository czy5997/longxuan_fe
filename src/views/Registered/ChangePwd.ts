import { Vue, Component, Prop } from 'vue-property-decorator'
import { Steps, Cascader, FormModel, Input } from 'ant-design-vue'
import { JSEncrypt } from 'jsencrypt'
import { userModule } from '@/store/modules/user'
import successBtn from '@/assets/image/goToLoginBg.png'
import successIcon from '@/assets/image/ic_succeed.png'
import loginSuccessIcon  from '@/assets/image/loginSuccessIcon.png'

@Component({
  components: {
    aSteps: Steps,
    aStep: Steps.Step,
    aCascader: Cascader,
    aFormModel: FormModel,
    aInput: Input,
    aInputPassword: Input.Password,
    aFormModelItem: FormModel.Item
  }
})
export default class ChangePwd extends Vue {
  $refs!: {
    firstForm: any
    secondForm: any
  }
  successBtn = successBtn
  successIcon = successIcon
  loginSuccessIcon=loginSuccessIcon
  publicKey = ''
  codeSIV: any = null
  codeDisabled = false
  currentStep = 0
  changeModel = {
    userName: '',
    verifycode: '',
    encryptNewPassword: '',
    confirmNewPassword: ''
  }
  @Prop({ type: Boolean, default: true }) isFromLogin: any
  @Prop({ type: Boolean, default: false }) isFromLoginModal: any

  validMobile(rule: any, value: any, callback: any) {
    const mobileTest = /^1(3|4|5|6|7|8|9)\d{9}$/
    const testValue = this.isFromLogin ? value : userModule.account
    if (testValue === '') {
      callback(new Error('请输入手机号码'))
    } else if (!mobileTest.test(testValue)) {
      callback(new Error('请输入正确的手机号码'))
    } else {
      callback()
    }
  }
  validPassword(rule: any, value: any, callback: any) {
    // const pwdTest = /^\S*(?=\S{6,12})(?=\S*\d)(?=\S*[a-zA-Z])\S*$/
    const pwdTest = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,12}$/
    if (!pwdTest.test(value)) {
      callback(new Error('请至少包含数字和字母，至少6位，至多12位'))
    } else {
      callback()
    }
  }
  validPasswordConfirm(rule: any, value: any, callback: any) {
    if (value !== this.changeModel.encryptNewPassword) {
      callback(new Error('确认密码输入不一致'))
    } else {
      callback()
    }
  }
  changeRules = {
    userName: [
      { required: true, validator: this.validMobile, trigger: 'blur' }
    ],
    verifycode: [{ required: true, message: '请输入验证码', trigger: 'blur' }],
    encryptNewPassword: [
      { required: true, validator: this.validPassword, trigger: 'blur' }
    ],
    confirmNewPassword: [
      { required: true, validator: this.validPasswordConfirm, trigger: 'blur' }
    ]
  }
  codeBtnText = '获取验证码'
  get btText() {
    return this.currentStep < 2 ? '下一步' : '去登录'
  }
  backToLogin() {
    if (this.isFromLoginModal){
      this.$emit('backToLogin')
    }else{
      this.$router.push({
        name: 'login'
      })
    }

  }
  sendVerifycode() {
    // console.log(this.codeSIV)
    if (this.codeSIV) {
      return
    }
    this.$refs.firstForm.validateField('userName', (message: string) => {
      if (!message) {
        const phone = this.isFromLogin
          ? this.changeModel.userName
          : userModule.account
        this.$httpRequester
          .post(
            'user/sms/verificte/send',
            { cellPhone: phone },
            'application/x-www-form-urlencoded'
          )
          .then((r: any) => {
            this.codeDisabled = true
            let time = 120
            this.codeSIV = setInterval(() => {
              time -= 1
              this.codeBtnText = `重新发送(${time}S)`
              if (time === 1) {
                this.codeBtnText = '获取验证码'
                this.codeDisabled = false
                clearInterval(this.codeSIV)
                this.codeSIV = null
              }
            }, 1000)
          })
      }
    })
  }
  nextSteps() {
    if (this.currentStep === 0) {
      this.$refs.firstForm.validate((value: boolean) => {
        if (value) {
          this.currentStep += 1
        }
      })
    } else if (this.currentStep === 1) {
      this.$refs.secondForm.validate((value: boolean) => {
        if (value) {
          this.changePassword()
        }
      })
    } else {
      if (this.isFromLoginModal){
        this.$emit('nextSteps')
      }else {
        this.$router.push({
          name: 'login'
        })
      }


    }
  }
  setPasswordRSA() {
    const encryptor = new JSEncrypt()
    // 设置公钥
    encryptor.setPublicKey(this.publicKey)
    // 加密数据
    return encryptor.encrypt(this.changeModel.confirmNewPassword)
  }
  changePassword() {
    const params = {
      verificteCode: this.changeModel.verifycode,
      encryptNewPassword: this.setPasswordRSA(),
      userAccount: this.isFromLogin
        ? this.changeModel.userName
        : userModule.account
    }
    this.$httpRequester
      .post('user/forget/password', params, 'application/x-www-form-urlencoded')
      .then((r: any) => {
        this.currentStep += 1
        if (!this.isFromLogin) {
          setTimeout(() => {
            this.$router.push({ name: 'login' })
          }, 2000)
        }
      })
  }
  getPublicKey() {
    this.publicKey =
      'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoUSnVj2D7J0WECHHF/rrjbO/x692gNJUhtYD/6Te2NKOW+0Hjq5lkk6lz5zO80Bikv9aCk2bDTDkh2xuUugkP26d2ZYcQCKibc2+di1AX6M56IJlKdnyY5gXM+L5bv6urGfpLqf1X1XJNoJP+WAUliA5RtBI7S1f+4zwycmm86HQazPVsARQGX2Bm8fvxNMlYxRM5rSzrZjbCYwL70/KVpow5mXr/n9N+zk+WnkXWq8UlkBd2KS/bsQbXJ3my/fkk2nWOZvpEyBdekYykrv4iw9x/9v/4bloRJvy6+Dbb4TZo0TxsGEUHIhHJ8kQ5gKPih32TdCSPGyUwSgYcWLSHwIDAQAB'
  }
  mounted() {
    this.getPublicKey()
  //  console.log(this.isFromLogin)
    if (!this.isFromLogin) {
      const first = userModule.account.substr(0, 3)
      const last = userModule.account.substr(7, userModule.account.length - 1)
      this.changeModel.userName = `${first}****${last}`
    }
  }
}
