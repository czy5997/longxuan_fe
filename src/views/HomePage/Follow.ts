import { Vue, Component, Prop } from 'vue-property-decorator'
import { FormModel, Select, Input, TreeSelect } from 'ant-design-vue'
import { universalModule } from '@/store/modules/universal'
import { homeModule } from '@/store/modules/home'

import TreeCheckboxSelect from '@/components/TreeCheckboxSelect.vue'

@Component({
  components: {
    aFormModel: FormModel,
    aFormModelItem: FormModel.Item,
    aInput: Input,
    aTreeSelect: TreeSelect,
    aTreeSelectNode: TreeSelect.TreeNode,
    aTextarea: Input.TextArea,
    aSelect: Select,
    aSelectOption: Select.Option,
    TreeCheckboxSelect
  }
})
export default class Follow extends Vue {
  @Prop({
    default() {
      return []
    },
    type: Array
  })
  followQualication?: []

  get fakeQualification() {
    return universalModule.fakeQualificationOptions
  }
  get qualificationOptions() {
    if (this.rowQuaStatus.onlyConstruction) {
      return universalModule.typeQualificationOptions
    } else {
      return []
    }
    //  return universalModule.typeQualificationOptions
  }
  get rowQuaStatus() {
    return homeModule.rowQuaStatus
  }
  get designQualificationOptions() {
    if (this.followModel.clueSources === 1) {
      if (this.rowQuaStatus.onlyDesign) {
        return universalModule.designQualificationOptions
      } else {
        return []
      }
    } else {
      return []
    }
    // return this.followModel.clueSources === 1
    //   ? universalModule.designQualificationOptions
    //   : []
  }
  $refs!: {
    followForm: any
  }
  followStatusOptions = [
    // { label: '无意向', value: 2, disabled: false },
    { label: '有意向', value: 'INTENTION', disabled: false },
    { label: '未成交', value: 'NO_DEAL', disabled: false },
    { label: '已成交', value: 'DEAL', disabled: false }
  ]
  phoneStatusOptions = [
    { value: 0, label: '无效' },
    { value: 1, label: '有效' }
  ]
  followModel: any = {
    remarks: '',
    contacts: '',
    followStatus: [],
    outcallOrderId: '',
    phone: '',
    phoneStatus: 1,
    qualificationItemIds: []
  }
  rules = {
    followStatus: [
      { required: true, message: '请选择跟进状态', trigger: 'blur' }
    ],
    contacts: [
      { required: true, message: '请输入跟进联系人', trigger: 'blur' }
    ],
    phone: [{ validator: this.validMobile, required: true, trigger: 'blur' }],
    remarks: [{ required: true, message: '请输入跟进记录', trigger: 'blur' }],
    qualificationItemIds: [
      { required: true, message: '请选择跟进的资质', trigger: 'change' }
    ]
  }
  validMobile(rule: any, value: any, callback: any) {
    const isTelephone = /^\d{3}-\d{8}$|^\d{4}-\d{7,8}$/
    const isCellPhone = /^1(3|4|5|6|7|8|9)\d{9}$/
    if (!isCellPhone.test(value) && !isTelephone.test(value)) {
      callback(new Error('请输入正确的固话或者手机号码'))
    } else {
      callback()
    }
  }
  getFollowModal() {
    // console.log(this.followModel)
    let data = null
    this.$refs.followForm.validate((val: boolean) => {
      if (val) {
        data = Object.assign({}, this.followModel)
      } else {
        data = null
      }
    })
    return data
  }
  setFollowDefault(defaultVal: any) {
    //   console.log(defaultVal)
    const lastFollow = defaultVal.lastFollow
      ? defaultVal.lastFollow.followRecordList[0]
      : null

    // const lastFollowList = lastFollowRecord
    //   ? lastFollowRecord.followRecordList
    //   : []
    // const lastFollow = lastFollowList ? lastFollowList[0] : []
    let defaultQua = [] as any
    let defaultPhone = ''
    let defaultContacts = ''
    if (lastFollow) {
      defaultPhone = lastFollow.phone
      defaultContacts = lastFollow.contacts
      defaultQua = defaultVal.qua.map((q: any) => {
        return q.id
      })
      // lastFollow.qualificationNames.forEach((d: any) => {
      //   const find = this.fakeQualification.find((c: any) => {
      //     console.log(d)
      //     return c.label === d
      //     //  parseInt(c.value, 10) === parseInt(d.qualificationItemId, 10)
      //   })
      //   if (find) {
      //     defaultQua.push(find.value)
      //   }
      // })
    }
    this.followModel = {
      content: '',
      contacts: defaultContacts,
      followStatus: [],
      outcallOrderId: '',
      phone: defaultPhone,
      remarks: '',
      phoneStatus: 1,
      clueSources: defaultVal.clueSources,
      qualificationItemIds: defaultQua
    }
    // console.log(this.followModel)
    this.$refs.followForm.clearValidate()
  }
}
