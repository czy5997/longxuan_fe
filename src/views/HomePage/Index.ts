import {
  DatePicker,
  Input,
  Icon,
  Drawer,
  Select,
  Table,
  Button,
  Popover,
  Modal,
  Spin,
  Pagination,
  Anchor,
  FormModel
} from 'ant-design-vue'
import { Component, Vue } from 'vue-property-decorator'
import SearchTool from './SearchTool.vue'
import GroupDrawer from './GroupDrawer.vue'
import Operate from '../../components/Operate.vue'
import Follow from './Follow.vue'
import DetailIndex from '../Detail/Index.vue'
import TableFilter from './Components/TableFilter.vue'
import TableCard from './Components/TableCard.vue'
import { homeModule } from '@/store/modules/home'
import Assign from './Assign.vue'
import { userModule } from '@/store/modules/user'
import { detailModule } from '@/store/modules/detail'
import formComponents from '@/components/Form/index.vue'
import qrModal from '@/components/qrCode.vue'
import notFind from '@/assets/image/no_data_block.png'
import BlockData from '@/components/BlockData.vue'
import { areaJSON } from '@/utils/area.js'

@Component({
  components: {
    aDatePicker: DatePicker,
    aPagination: Pagination,
    aInput: Input,
    aInputSearch: Input.Search,
    aIcon: Icon,
    aSelect: Select,
    aSelectOption: Select.Option,
    aDrawer: Drawer,
    SearchTool,
    aTable: Table,
    aButton: Button,
    aPopover: Popover,
    Operate,
    GroupDrawer,
    aModal: Modal,
    aFormModel: FormModel,
    aFormModelItem: FormModel.Item,
    aTextarea: Input.TextArea,
    Follow,
    Assign,
    DetailIndex,
    TableFilter,
    TableCard,
    ASpin: Spin,
    AAnchor: Anchor,
    AAnchorLink: Anchor.Link,
    formComponents,
    qrModal,
    BlockData
  }
})
export default class Index extends Vue {
  $refs!: {
    companyDrawer: any
    filterGroup: any
    tool: any
    qr: any
    assignForm: any
    followRef: any
    feedBackRef: any
    assignModal: any
    tableFilterREF: any
    backToFilterLink: any
    communicationForm: any
  }
  isNotSpecialProvince = true
  notFind = notFind

  setTurnDetaultQua(row: any) {
    if (this.filters.must.qualification.data.length) {
      // console.log(this.filters.must.qualification.data)
      this.filters.must.qualification.data.forEach((e: any) => {
        const findQua = row.qualificationsList.find((q: any) => {
          return parseInt(q.id, 10) === e
        })
        if (findQua) {
          this.tableItemDefauletQua.push({
            value: findQua.id,
            label: findQua.qualification
          })
        }
      })
      //  this.tableItemDefauletQua = this.filters.must.qualification.data
    } else {
      this.tableItemDefauletQua = row.qualificationsList.map((e: any) => {
        return {
          value: e.id,
          label: e.qualificationName
        }
      })
    }
    homeModule.setATurnOnDefaultQua(this.tableItemDefauletQua)
  }

  phoneIsTurnOn(row: any) {
    detailModule.setEnterpriseInfo(row)
    // homeModule.setTurnOnItem(row)
    this.setTurnDetaultQua(row)
    homeModule.changeTurnPhoneStatus(true)
    homeModule.setPageHandleFunction(this.afterTurnOnHandle)
  }

  phoneIsNotTurnOn(row: any) {
    detailModule.setEnterpriseInfo(row)
    homeModule.changeNotTurnPhoneStatus(true)
    homeModule.setPageHandleFunction(this.afterTurnOnHandle)
  }

  tableItemDefauletQua = [] as any // 默认的资质
  cardRow = {} as any

  afterTurnOnHandle() {
    this.getComponyList()
  }

  timeDesc = 0

  filterIsEstablisTime() {
    this.timeDesc += 1
    if (this.timeDesc > 2) {
      this.timeDesc = 0
    }
    this.getComponyList()
  }

  getDetailDom() {
    return document.getElementById('routerContent')
  }

  showWatchBtn = true
  clickItems: any = []
  shareDisplay = false
  keyword = '' //关键字
  isSuperAdmin = userModule.roleLevel === 1
  isAdmin = userModule.roleLevel === 2
  isSales = userModule.roleLevel === 4
  isLead = userModule.isLead
  showSelectAllBtn = true
  isRisk = 1
  isReceiveCount = 1
  pageSize = 10
  pageNum = 1
  total = 0
  tableData: any = []
  tipText = ''
  dataLoading = false
  followShow = false
  qrCode = ''
  followName = ''
  isBatch = false
  filters: any = {}

  get pagination() {
    return {
      total: this.total,
      pageSize: this.pageSize,
      current: this.pageNum,
      showTotal: (total: number) => {
        return `共${total}条数据`
      },
      showSizeChanger: false,
      onChange: () => {
        console.log('on change ')
      }
    }
  }

  showFilterGroup = false
  filterKeyword = ''
  filterStaff = ''
  receiveList = []
  filterSort = ''

  btns = [
    {
      label: '跟进记录',
      type: 'link',
      icon: 'edit',
      click: this.staffFollowUp.bind(this)
    },
    {
      label: '分享',
      type: 'link',
      icon: 'share-alt',
      click: this.showSharQrCode.bind(this)
    }
  ]
  sortOptions = [
    { label: '保存时间从近到远', value: '0' },
    { label: '保存时间从远到近', value: '1' },
    { label: '使用次数从少到多', value: '2' },
    { label: '使用次数从多到少', value: '3 ' }
  ]
  selectedRowKeys: any = []
  isShow = false
  areaOptions: any = []
  assignStaff: any = []
  companyDetailTitle = ''
  companyDetail = {}
  followQualication = []
  followId = ''
  reamrk = ' '
  batchNum = 0
  scrollTop = 0
  showCompanyDetailDrawer = false

  get rowSelection() {
    return {
      selectedRowKeys: this.selectedRowKeys,
      onChange: this.selectRows,
      getCheckboxProps: (record: any) => {
        return {
          props: {
            // disabled: record.haveReceive,
            checked: this.selectedRowKeys.includes(record.enterpriseId)
          }
        }
      }
    }
  }

  setDrawerContainer() {
    return document.getElementById('tableFilter')
  }

  paginationShowTotal(total: number) {
    return ''
    // `共${total}条数据`
  }

  changeChecked(index: number) {
    this.tableData[index].isChecked = !this.tableData[index].isChecked
    this.$set(this.tableData, index, this.tableData[index])
    const checkList = this.tableData.filter((e: any) => {
      return e.isChecked
    })
    this.batchNum = checkList.length
    this.receiveList = checkList
  }

  selectAllTableCard() {
    this.tableData.forEach((e: any) => {
      e.isChecked = true
    })
    this.tableData = [...this.tableData]
    this.batchNum = this.tableData.length
    this.receiveList = this.tableData
    this.showSelectAllBtn = false
    // console.log(this.tableData)
  }

  cleanSelectAll() {
    this.tableData.forEach((e: any) => {
      e.isChecked = false
    })
    this.tableData = [...this.tableData]
    this.batchNum = 0
    this.receiveList = []
    this.showSelectAllBtn = true
  }

  changeBatchStatus() {
    this.isBatch = !this.isBatch
    if (!this.isBatch) {
      this.cleanSelectAll()
      // this.tableData.forEach((e: any) => {
      //   e.isChecked = false
      // })
    }
  }

  showSharQrCode(item: any) {
    if (this.isSuperAdmin) {
      return
    }
    this.$httpRequester
      .get('enterpriseClue/shareEnterpriseTime', {
        enterpriseId: item.enterpriseId,
        userId: userModule.userId
      })
      .then((res: any) => {
        const href = window.location.href
        const find = href.indexOf('#')
        const host = href.substr(0, find + 1)
        const currentDate = new Date().getTime()
        // encodeURIComponent(
        const link =
          VUE_APP_TYPE === 'online'
            ? 'http://www.xnjx666.com:9000/index.html#'
            : host
        const pageUrl = `${link}/enterprise/share?enterpriseId=${item.enterpriseId}&dateTime=${currentDate}&uuid=${res}`
        // )
        const url =
          'http://www.xnjx666.com/shareIframe.html?src=' +
          encodeURIComponent(pageUrl)
        this.qrCode = url
        this.$refs.qr.showModal()
      })
  }

  filterIsRisk() {
    this.isRisk = this.isRisk === 2 ? 1 : 2
    this.getComponyList()
  }

  filterIsReceiveCount() {
    this.isReceiveCount = this.isReceiveCount === 2 ? 1 : 2
    this.getComponyList()
  }

  assignItems() {
    // if (!this.isLead && !this.isAdmin) {
    //   this.$message.error('您没有权限使用此功能')
    //   return
    // }
    if (!this.receiveList.length) {
      this.$message.error('请先选择线索')
      return
    }
    this.$refs.assignModal.showModal()
  }

  findClickedItem(enterpriseId: any) {
    // 判断有没有点过
    const have = this.clickItems.find((c: any) => c === enterpriseId)
    return have
  }

  goToCompanyDetail(item: any) {
    this.scrollTop =
      document.body.scrollTop + document.documentElement.scrollTop // 滚动条高度

    if (!this.isNotSpecialProvince) {
      return
    }
    if (!this.findClickedItem(item.enterpriseId)) {
      this.clickItems.push(item.enterpriseId)
    }
    this.setTurnDetaultQua(item)
    this.companyDetailTitle = item.companyName
    this.showCompanyDetailDrawer = true

    detailModule.setEnterpriseInfo(item)
    this.$nextTick(() => {
      this.$refs.companyDrawer.setBasicInfo()
    })
  }

  closeCompanyDetailDrawer() {
    this.$refs.companyDrawer.setBasicInfoReback()
    this.showCompanyDetailDrawer = false
    // this.$nextTick(() => {
    //   setTimeout(() => {
    //     document.documentElement.scrollTo(0, this.scrollTop)
    //   }, 330)
    // })
  }

  assignSuccess() {
    this.receiveList = []
    this.cleanSelectAll()
    this.getComponyList()
  }

  selectRows(key: any, rows: any) {
    // console.log(key, rows) // 一整行
    this.selectedRowKeys = key
    this.receiveList = rows
  }

  receiveListItems() {
    if (this.receiveList.length === 0) {
      this.$message.error('请先选择数据条目再领取')
      return
    }
    this.isShow = true
    // this.rowSelection.get
    Modal.confirm({
      title: '是否确认领取所选择的客户？',
      content: '领取后1天后未确认状态，客户将自动放回公海',
      icon: 'info-circle',
      onOk: () => {
        const params = this.receiveList.map((e: any) => {
          return {
            enterpriseName: e.companyName,
            enterpriseId: e.enterpriseId
          }
        })

        this.$httpRequester
          .post('enterpriseClue/addReceive', params)
          .then((r: any) => {
            this.$message.success('领取成功')
            this.selectedRowKeys = []
            this.setTableItemReceive(this.receiveList)
            homeModule.getFreeTime()
          })
          .catch((err: any) => {
            if (err.data.length) {
              const receiveList: any = []
              this.receiveList.forEach((r: any) => {
                const find = err.data.find(
                  (e: any) => e.enterpriseId === r.enterpriseId
                )
                if (!find) {
                  receiveList.push(r)
                }
              })
              this.setTableItemReceive(receiveList)
            }
            if (err.code === '2005') {
              detailModule.setExpriedModalShow(true)
            }
          })
      },
      onCancel: () => {
        this.$message.info('操作取消')
      }
    })
  }

  setTableItemReceive(receiveList: any) {
    this.tableData.forEach((t: any) => {
      const find = receiveList.find(
        (e: any) => t.enterpriseId === e.enterpriseId
      )
      if (find) {
        t.haveReceive = true
        t.receiveCount += 1
      }
    })
    this.tableData = [...this.tableData]
  }

  receiveThisItem(item: any) {
    if (this.isSuperAdmin) {
      this.$message.warning('超级管理员无法查看电话')
      return
    }
    const params = [
      {
        enterpriseName: item.companyName || item.enterpriseName,
        enterpriseId: item.enterpriseId
      }
    ]
    this.$httpRequester
      .post('enterpriseClue/addReceive', params)
      .then((r: any) => {
        // this.tableData.forEach((t: any) => {
        //   if (t.enterpriseId === r[0].enterpriseId) {
        //     t.haveReceive = true
        //     t.phoneNumDisplay = r[0].phoneList.join(',')
        //   }
        // })
        // detailModule.changeEnterpriseKey({ prop: 'haveReceive', key: true })
        this.getComponyList()
        // item.haveReceive = true
        // item.receiveCount += 1
        // item.phoneNumDisplay = r[0].phoneList.join(',')
        // this.tableData = [...this.tableData]

        // const phoneText = item.phoneText
        // if (phoneText.indexOf('暂') > -1) {
        //   this.$message.success('领取成功')
        // } else {
        //   const inputDom = document.createElement('input')
        //   document.body.appendChild(inputDom)
        //   inputDom.setAttribute('value', phoneText)
        //   inputDom.select()
        //   document.execCommand('copy')
        //   document.body.removeChild(inputDom)
        //   this.$message.success('领取成功，已复制第一个电话到剪贴板')
        // }
        homeModule.getFreeTime()
      })
      .catch(err => {
        if (err.code === '2005') {
          detailModule.setExpriedModalShow(true)
        }
        if (err.code === '5003') {
          // console.log(err.msg)
          Modal.info({
            title: '温馨提示',
            content: err.msg,
            okText: '确定',
            onOk: () => {
              // this.setReceiveDataInTable(err.data)
              console.log('is ok')
            }
          })
        }
      })
  }

  clickFindBtns() {
    this.isRisk = 1
    // this.timeDesc = 0
  }

  tableChange(page: any, pageSize: any) {
    this.pageSize = pageSize
    this.pageNum = page
    // const currentPath = this.$route.path
    this.$setQuery.addQuery(this.$route, {
      pageNum: this.pageNum,
      pageSize: this.pageSize
    })
    this.$refs.tableFilterREF.saveCurrentPageSizeAndPageNum(
      this.pageNum,
      this.pageSize
    )

    this.$refs.backToFilterLink.handleClick()
    const pageDom = document.getElementsByClassName(
      'ant-pagination-options-quick-jumper'
    )[0]
    const pageInput = pageDom.getElementsByTagName('input')

    this.$nextTick(() => {
      pageInput[0].value = ''
    })
    // console.log(this.$refs.backToFilterLink)
    // const pagePath =
    //   this.$route.path + `?pageNum=${this.pageNum}&pageSize=${this.pageSize}`
    // this.$router.push(pagePath)
    // this.getComponyList()
  }

  setFilterOption(info: any) {
    // this.isNotSpecialProvince = info.isNotSpecialProvince
    // console.log(info.options.printNo.data)
    if (info.options.printNo.data.length) {
      // 发证日期在五年后
      info.options.printNo.data = [
        this.$moment(info.options.printNo.data[0])
          .add(5, 'years')
          .format('YYYY-MM-DD'),
        this.$moment(info.options.printNo.data[1])
          .add(5, 'years')
          .format('YYYY-MM-DD')
      ]
    }
    this.filters = {
      must: info.options || {
        isReceived: {
          data: ''
        },
        qualificationCount: {
          data: []
        },
        qualification: {
          isContain: 1,
          data: []
        },
        certificateNo: {
          data: ''
        },
        effectiveDate: {
          data: []
        },
        companyName: {
          data: ''
        },
        legalPerson: {
          data: ''
        },
        areaCode: {
          data: []
        },
        registeredCapital: {
          data: []
        },
        shareholderName: {
          data: []
        },
        establishmentDate: {
          data: []
        },
        keywords: {
          data: '',
          type: 0
        }, //关键字
        printNo: {
          data: []
        }
      }
    }
    this.pageNum = info.pageNum
    this.pageSize = info.pageSize
    this.getComponyList()
  }

  gangAoTai = [810000, 820000, 710000]

  getComponyList() {
    const keyWords = this.filters.must.keywords.data
    this.dataLoading = true
    const params = {
      companyId: 1,
      isRisk: this.isRisk,
      establishmentSort: this.timeDesc,
      isReceiveCount: this.isReceiveCount,
      pageNum: this.pageNum,
      pageSize: this.pageSize,
      queryJson: JSON.stringify(this.filters)
    }
    const areaCode = this.filters.must.areaCode.data
    this.$httpRequester
      .post('enterpriseClue/list', params)
      .then((r: any) => {
        if (areaCode.length) {
          areaCode.forEach((area: any) => {
            if (this.gangAoTai.includes(area)) {
              this.isNotSpecialProvince = false
            } else {
              this.isNotSpecialProvince = true
            }
          })
        }
        const filtersQlificationId = this.filters
          ? this.filters.must.qualification.data
          : []
        // 关键字高亮
        // r.data.map((item: any) => {
        // console.log(item);
        // })
        // console.log(filtersQlificationId)

        r.data.forEach((e: any, i: any) => {
          e.number = i + 1
          /// (this.pageNum - 1) * this.pageSize + i + 1
          e.qualificationsList = e.qualificationsList.filter((q: any) => {
            return q.id
          })
          // 匹配关键字正则
          const replaceReg = new RegExp(keyWords, 'g')
          // 高亮替换v-html值
          const replaceString =
            '<span class="hightlight">' + keyWords + '</span>'
          e.companyNameHTML = keyWords
            ? e.companyName.replace(replaceReg, replaceString)
            : e.companyName // 开始替换
          e.qualificationsListText = e.qualificationsList
            .map((q: any) => {
              return q.qualificationName
            })
            .join('、') // 悬浮窗的资质列表
          // if (e.qualificationsList.length > 5) {
          if (filtersQlificationId.length > 0) {
            const findDisplay = [] as any
            const newList = e.qualificationsList.concat([])

            filtersQlificationId.forEach((f: any) => {
              const index = newList.findIndex(
                (q: any) => parseInt(q.id, 10) === parseInt(f, 10)
              )
              newList[index] = {}
              if (index > -1) {
                findDisplay.push(
                  '<span class="qua-hightlight">' +
                    e.qualificationsList[index].qualificationName +
                    '</span>' //找到所有筛选的资质
                )
                //  newList.splice(index, 1)不用slice保证数组长度
              }
            })
            const otherList = newList.filter((n: any) => {
              return n.qualificationName
            }) // 避免显示混乱 重新取一个数组
            const display =
              findDisplay.length > 5
                ? findDisplay.slice(0, 5).join('、')
                : findDisplay.join('、') // 取前五个
            const text =
              findDisplay.length > 5
                ? ''
                : otherList
                    .slice(0, 5 - findDisplay.length)
                    .map((q: any) => {
                      return q.qualificationName
                    })
                    .join('、')
            const tooltipText = otherList
              .map((q: any) => {
                return q.qualificationName
              })
              .join('、') // 悬浮窗也要高亮
            e.qualificationsListText =
              display + (tooltipText ? '、' + tooltipText : '')
            e.qualificationsText =
              e.qualificationsList.length > 5
                ? `${display}${text ? '、' + text : ''}等${
                    e.qualificationsList.length
                  }项`
                : `${display}${text ? '、' + text : ''}`
          } else {
            const text = e.qualificationsList
              .slice(0, 5)
              .map((q: any) => {
                return q.qualificationName
              })
              .join('、')
            e.qualificationsText =
              e.qualificationsList.length > 5
                ? `${text}等${e.qualificationsList.length}项`
                : text
          }
          // } else {
          //   e.qualificationsText = e.qualificationsList
          //     .map((q: any) => {
          //       return q.qualificationName
          //     })
          //     .join('、')
          // }

          // if (!e.isDishonesty && !e.isExecuted) {
          //   e.legalRisks = '无'
          // } else if (!!e.isDishonesty && !e.isExecuted) {
          //   e.legalRisks = '失信信息'
          // } else if (!!e.isExecuted && !e.isDishonesty) {
          //   e.legalRisks = '被执行人'
          // } else if (!!e.isDishonesty && !!e.isExecuted) {
          //   e.legalRisks = '失信信息、被执行人'
          // }
          if (e.contactInformationList && e.contactInformationList.length) {
            if (e.haveReceive) {
              e.phoneNumDisplay = e.contactInformationList
                .map((c: any) => {
                  return (
                    '<span ondblclick="copyThisText(' +
                    c +
                    ')">' +
                    c +
                    '</span>'
                  )
                })
                .join('、')
            } else {
              e.phoneNumDisplay = e.contactInformationList.join('、')
            }
            //  : '***********'
          } else {
            e.phoneNumDisplay = '暂无联系方式'
          }

          // e.contactInformationListText = e.contactInformationList.join('、')
          e.receiveCount = e.receiveCount || 0
          e.dealCount = e.dealCount || 0
          // e.haveReceive
          e.checked = false
        })
        this.tableData = r.data
        const totalLength = r.totalSize.toString().length
        const totalNum = Math.pow(10, totalLength - 1)
        const tipText = Math.floor(r.totalSize / totalNum) // 去除零头
        this.tipText =
          r.totalSize > 1000 ? tipText * totalNum + '+' : r.totalSize
        this.total = r.totalSize
        this.dataLoading = false
        this.isNeedShowWatchBtn()
      })
      .catch(() => {
        this.dataLoading = false
      })
  }

  isNeedShowWatchBtn() {
    const receiveLength = this.tableData.filter((e: any) => {
      return e.haveReceive
    }).length
    this.showWatchBtn = receiveLength !== this.tableData.length
  }

  getAreaList() {
    const r = areaJSON
    // this.$httpRequester.get('regionCode/selList').then((r: any) => {
    this.areaOptions = []
    r.forEach((e: any) => {
      this.areaOptions.push({
        title: e.areaName,
        value: e.areaCode,
        key: e.areaCode,
        children: e.cityList.map((c: any) => {
          return {
            title: c.areaName,
            value: c.areaCode,
            key: c.areaCode
          }
        })
      })
    })
    homeModule.setAreaList(this.areaOptions)
    // })
  }

  staffFollowUp(item: any) {
    // this.$refs.followRef.setFollowDefault()
    this.followShow = true
    this.$nextTick(() => {
      this.$refs.followRef.setFollowDefault()
    })
    this.followId = item.enterpriseId
    this.followName = item.companyName
    this.followQualication = item.qualificationsList
  }

  closeFollowDrawer() {
    this.followShow = false
    this.followId = ''
  }

  addFollow() {
    const follwData = this.$refs.followRef.getFollowModal()
    if (!follwData) {
      return
    }
    const params = {
      ...follwData,
      enterpriseId: this.followId,
      enterpriseName: this.followName
    }
    this.$httpRequester
      .post('followRecord/addFollowRecord', [params])
      .then(() => {
        this.$message.success('操作成功')
        this.$refs.followRef.setFollowDefault()
        this.closeFollowDrawer()
      })
  }

  receiveAllList() {
    // console.log(this.tableData)
    const params = this.tableData.map((e: any) => {
      //  console.log(e)
      return {
        enterpriseName: e.companyName,
        enterpriseId: e.enterpriseId
      }
      // return e.enterpriseId
    })
    // console.log(params)
    this.$httpRequester
      .post('enterpriseClue/addReceive', params)
      .then((res: any) => {
        // this.getComponyList()
        // res.forEach((r: any) => {
        //   const find = this.tableData.find((t: any) => {
        //     return t.enterpriseId === r.enterpriseId
        //   })
        //   if (find) {
        //     find.haveReceive = true
        //     find.phoneNumDisplay = r.phoneList.join(',')
        //     find.receiveCount += 1
        //   }
        // })
        this.setReceiveDataInTable(res)
        // this.tableData.forEach((t: any) => {})
        // this.showWatchBtn = false
        this.$message.success('操作成功')
        homeModule.getFreeTime()
      })
      .catch((err: any) => {
        if (err.code === '2005') {
          detailModule.setExpriedModalShow(true)
        }
        if (err.code === '5004') {
          // 本页全都被领取光了
          Modal.confirm({
            title: '温馨提示',
            content: err.msg,
            okText: '下一页',
            cancelText: '取消',
            onOk: () => {
              if (this.total / this.pageSize > this.pageNum) {
                this.pageNum += 1
                this.tableChange(this.pageNum, this.pageSize)
              } else {
                this.$message.warning('已经是最后一页了！')
                this.showWatchBtn = false
              }
            },
            onCancel: () => {
              console.log('on cancel')
            }
          })
        } else if (err.code === '5005') {
          Modal.info({
            title: '温馨提示',
            content: err.msg,
            okText: '确定',
            onOk: () => {
              this.setReceiveDataInTable(err.data)
              console.log('is ok')
            }
          })
        }
      })
  }

  setReceiveDataInTable(r: any) {
    const companyIdList = Object.keys(r) as []
    // console.log(companyIdList)
    this.tableData.forEach((t: any) => {
      if (
        companyIdList.find((c: any) => {
          return t.enterpriseId === c
        })
      ) {
        t.receiveCount += 1
        t.haveReceive = true
        t.phoneNumDisplay = r[t.enterpriseId].join(',')
      }
    })
    this.showWatchBtn = false
    // this.getComponyList()
  }

  mounted() {
    // this.setFilterOption(undefined)
    this.$nextTick(() => {
      this.isShow = true
      //  this.getAreaList()
    })
  }
}
