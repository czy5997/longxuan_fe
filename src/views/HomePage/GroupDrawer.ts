import { Input, Select, Pagination, Button, Modal, Icon } from 'ant-design-vue'
import { Component, Vue, Emit } from 'vue-property-decorator'
import { userModule } from '@/store/modules/user'
import SearchFilter from './SearchFilter.vue'
@Component({
  components: {
    aInputSearch: Input.Search,
    aSelect: Select,
    aIcon: Icon,
    aButton: Button,
    aModal: Modal,
    aPagination: Pagination,
    aSelectOption: Select.Option,
    SearchFilter
  }
})
export default class GroupDrawer extends Vue {
  $refs!: {
    searchRef: any
    filterDrawer: any
  }
  sortOptions = [
    { label: '保存时间从近到远', value: '0' },
    { label: '保存时间从远到近', value: '1' },
    { label: '使用次数从少到多', value: '2' },
    { label: '使用次数从多到少', value: '3' }
  ]
  filterGroup = []
  pageNum = 1
  total = 0
  staffId = ''
  staffList = []
  previewModal = false
  previewTitle = ''
  filterKeyword = ''
  sortParams = {
    sortBy: '',
    desc: true
  }
  filterItemId = ''
  staffModel = []
  sortModel = []
  containNode() {
    return this.$refs.filterDrawer
  }
  searchFilterGroup() {
    console.log(this.filterKeyword)
    this.getFilterGroup()
  }
  changeStaffList(e: any) {
    this.staffId = e
    this.getFilterGroup()
  }
  changeSort(e: any) {
    switch (e) {
      case '0':
        this.sortParams = {
          sortBy: 'saveTime',
          desc: true
        }
        break
      case '1':
        this.sortParams = {
          sortBy: 'saveTime',
          desc: false
        }
        break
      case '2':
        this.sortParams = {
          sortBy: 'usedCount',
          desc: false
        }
        break
      case '3':
        this.sortParams = {
          sortBy: 'usedCount',
          desc: true
        }
        break
    }
    this.getFilterGroup()
  }
  changePage(pageNum: number) {
    this.pageNum = pageNum
    this.getFilterGroup()
  }
  getFilterGroup() {
    const params = {
      ...this.sortParams,
      rows: 5,
      conditionName: this.filterKeyword,
      page: this.pageNum,
      userId: this.staffId,
      companyId: userModule.companyId
    }
    this.$httpRequester
      .get('filter/queryConditionByPage', params)
      .then((r: any) => {
        r.records.forEach((e: any) => {
          e.saveTime = this.$moment(e.saveTime).format('yyyy-MM-DD')
        })
        this.filterGroup = r.records
        this.total = r.total
      })
  }
  preview(item: any) {
    const filterOptions = JSON.parse(item.filterCondition)
    // console.log(filterOptions)
    this.previewTitle = item.conditionName
    this.filterItemId = item.id
    this.previewModal = true
    this.$nextTick(() => {
      this.$refs.searchRef.setFilterOptionFromModal(
        filterOptions as any,
        this.previewTitle,
        this.filterItemId
      )
    })
  }
  handleCancelPreview() {
    this.previewModal = false
  }
  @Emit('update-filter-group')
  updateFilterGroup() {
    const filterOptions = this.$refs.searchRef.getFilterOptions()
    this.$refs.searchRef.saveCurrentFilters()
    this.previewModal = false
    return { filterOptions, id: this.filterItemId }
  }

  removeThisItem(item: any) {
    Modal.confirm({
      title: '温馨提示',
      content: `确认删除${item.conditionName}筛选组吗？`,
      onOk: () => {
        this.$httpRequester.delete('filter/id', item.id).then(() => {
          this.$message.success('操作成功')
          this.getFilterGroup()
        })
      },
      onCancel: () => {
        this.$message.info('操作取消')
      }
    })
  }
  @Emit('update-filter-group')
  useThisGroup(item: any) {
    const filterOptions = JSON.parse(item.filterCondition)
    const findIndex = filterOptions.findIndex(
      (e: any) => e.key === 'qualification'
    )
    if (findIndex > -1) {
      filterOptions.splice(findIndex, 1)
    }
    return { filterOptions, id: item.id }
  }
  getStaffList() {
    this.$httpRequester.get('filter/queryFilterUser').then((r: any) => {
      this.staffList = r.map((l: any) => {
        return {
          label: l.saveUserName,
          value: l.saveUserId
        }
      })
    })
  }
  setDefaultFilter() {
    this.filterKeyword = ''
    this.sortParams = {
      sortBy: '',
      desc: true
    }
    this.staffId = ''
    this.staffModel = []
    this.sortModel = []
  }
}
