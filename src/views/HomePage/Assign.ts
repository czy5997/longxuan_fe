import { FormModel, Select, Input, Modal } from 'ant-design-vue'
import { Vue, Component, Prop } from 'vue-property-decorator'
import { userModule } from '@/store/modules/user'
@Component({
  components: {
    aModal: Modal,
    aFormModel: FormModel,
    aFormModelItem: FormModel.Item,
    aSelect: Select,
    aSelectOption: Select.Option,
    aTextarea: Input.TextArea
  }
})
export default class Assign extends Vue {
  $refs!: {
    assignForm: any
  }
  assignModel = {
    assignStaff: [],
    reamrk: ''
  }
  assignModal = false
  staffList: any = []
  assignStaff: any = []
  userId: any = userModule.userId
  @Prop(Array) receiveList!: []
  getStaffList() {
    const params = {
      companyId: userModule.companyId
    }
    this.$httpRequester.get('member/listAll', params).then((r: any) => {
      this.staffList = r.map((e: any) => {
        return {
          value: e.id,
          label: e.memberName,
          key: e.id
        }
      })
      const index = this.staffList.findIndex(
        (e: any) => parseInt(e.value, 10) === parseInt(this.userId, 10)
      )
      this.staffList.splice(index, 1)
    })
  }
  getLeadStaff() {
    const params = {
      upleadId: userModule.userId
    }
    this.$httpRequester.get('member/listAll', params).then((r: any) => {
      this.staffList = r.map((e: any) => {
        return {
          value: e.id,
          label: e.memberName,
          key: e.id
        }
      })
    })
    const index = this.staffList.findIndex(
      (e: any) => parseInt(e.value, 10) === parseInt(this.userId, 10)
    )
    this.staffList.splice(index, 1)
  }
  seletAssignStaff(e: any) {
    this.assignStaff = e
  }
  assignToStaff() {
    this.$refs.assignForm.validate((val: any) => {
      if (val) {
        const notReceive = this.receiveList.find(
          (r: any) => r.haveReceive === false
        )
        if (notReceive) {
          this.$message.error('请先领取资源后再进行分配')
        } else {
          const params = this.receiveList.map((r: any) => {
            return {
              appointMemberId: this.assignStaff.key,
              appointMemberName: this.assignStaff.label,
              enterpriseId: r.enterpriseId,
              enterpriseName: r.enterpriseName || r.companyName,
              remarks: this.assignStaff.reamrk
            }
          })
          this.$httpRequester
            .post('followRecord/distributionEnterprise', params)
            .then(() => {
              this.$message.success('操作成功')
              // this.receiveList = []
              this.$emit('assignSuccess')
              this.$emit('setSelectStaff', this.assignStaff)
              this.cancelAssign()

              //  this.getComponyList(undefined)
            })
        }
      }
    })
  }
  showModal() {
    this.assignModal = true

    this.$nextTick(() => {
      this.$refs.assignForm.clearValidate()
      //超管没有公司id，不能获取某公司下的所有成员
      if (userModule.roleLevel !== 1 || userModule.isLead) {
        this.getStaffList()
      }
    })
    // console.log(this.receiveList)
  }
  cancelAssign() {
    this.assignStaff = []
    this.assignModel.assignStaff = []
    this.assignModal = false
  }
}
