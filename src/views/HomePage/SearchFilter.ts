import {
  TreeSelect,
  Input,
  Icon,
  Button,
  Select,
  DatePicker,
  Radio,
  InputNumber,
  Modal
} from 'ant-design-vue'
import { Component, Vue, Prop } from 'vue-property-decorator'
import { userModule } from '@/store/modules/user'
import { homeModule } from '@/store/modules/home'

const DATE_RANGE = 1
const SELECT = 2
const INPUT = 3
const RADIO = 4
const NUMBER = 5
const DATE = 6
const MULIT_SELECT = 7
const NUMBER_RANG = 8
const MULIT_INPUT = 9
@Component({
  components: {
    aInput: Input,
    aModal: Modal,
    aTreeSelect: TreeSelect,
    aButton: Button,
    aSelect: Select,
    aSelectOption: Select.Option,
    aIcon: Icon,
    aDatePicker: DatePicker,
    aRangePicker: DatePicker.RangePicker,
    aRadio: Radio,
    aRadioGroup: Radio.Group,
    aInputNumber: InputNumber,
    aTreeSelectNode: TreeSelect.TreeNode
  }
})
export default class SearchFilter extends Vue {
  DATE_RANGE = DATE_RANGE
  SELECT = SELECT
  INPUT = INPUT
  RADIO = RADIO
  NUMBER = NUMBER
  DATE = DATE
  MULIT_SELECT = MULIT_SELECT
  NUMBER_RANG = NUMBER_RANG
  MULIT_INPUT = MULIT_INPUT
  @Prop(Array) fatherInputOptions?: []
  @Prop(Array) fatherFilterOption?: []
  saveModal = false
  inputArray = []
  inputOptions: any[] = []
  filterOption: any[] = []
  qualification = []
  treeSelect = []
  qualificationOptions: any[] = []
  haveAdd = false
  defaultExpend: any = []
  filterData: any = {
    isReceived: {
      data: ''
    },
    qualificationCount: {
      data: []
    },
    qualification: {
      data: []
    },
    certificateNo: {
      data: ''
    },
    effectiveDate: {
      data: []
    },
    companyName: {
      data: ''
    },
    legalPerson: {
      data: ''
    },
    areaCode: {
      data: []
    },
    registeredCapital: {
      data: []
    },
    shareholderName: {
      data: []
    },
    establishmentDate: {
      data: []
    }
  }
  isfromModal = false
  groupName = ''
  requestor: any = ''
  editId = ''
  areaOptions: any = []
  minInput = 0
  setFilterOptionFromModal(item: any, name: string, id: any) {
    this.$nextTick(() => {
      this.filterOption = item
      this.groupName = name || ''
      this.isfromModal = name ? true : false
      this.editId = id
      // let quaIndex = -1
      this.filterOption.forEach((e: any, index: number) => {
        if (e.key === 'areaCode') {
          e.options = homeModule.areaList
          this.treeSelect = e.value
        } else if (e.type === this.DATE_RANGE) {
          e.value = [this.$moment(e.value[0]), this.$moment(e.value[1])]
        } else if (e.key === 'qualification') {
          this.qualification = e.value
          this.filterData.qualification.data = e.value.map((v: any) => {
            return v.value
          })
        } else if (e.key === 'registeredCapital') {
          e.options = [{ text: '万元' }, { text: e.value['min'] || 0 }]
        } else if (e.key === 'qualificationCount') {
          e.options = [{ text: '个' }, { text: e.value['min'] || 0 }]
        } else if (e.key === 'isReceived') {
          e.options = [
            { label: '有', value: '1' },
            { label: '无', value: '2' }
          ]
          this.$set(this.filterOption, index, e)
        } else if (e.type === this.mounted) {
          e.value = e.value.join('，')
        }
      })
      // this.filterOption.splice(quaIndex, 1)
      // console.log(this.filterOption)
    })
  }
  getFilterOptions() {
    return this.filterOption
  }
  saveCurrentFilters() {
    let saveOb: any = []
    if (this.qualification.length) {
      // console.log('11',this.filterOption)
      saveOb = this.filterOption.map((e: any) => {
        return {
          title: e.title,
          key: e.key,
          value: e.value,
          type: e.type,
          optionValue: e.optionValue
        }
      })
      saveOb.push({
        title: '资质类别及等级',
        key: 'qualification',
        value: this.qualification
      })
    } else {
      saveOb = this.filterOption.map((e: any) => {
        return {
          title: e.title,
          key: e.key,
          value: e.value,
          type: e.type,
          optionValue: e.optionValue
        }
      })
    }
    const findBlockIndex = saveOb.findIndex((e: any) => e.type === -1)
    if (findBlockIndex > -1) {
      saveOb.splice(findBlockIndex, 1)
    }
    // console.log(saveOb)
    if (!this.groupName) {
      this.$message.error('请输入条件组名称')
      return
    } else {
      if (this.isfromModal) {
        const params = {
          id: this.editId,
          filterCondition: JSON.stringify(saveOb),
          companyId: userModule.companyId,
          saveUserName: userModule.name,
          saveUserId: userModule.userId,
          conditionName: this.groupName
        }
        if (this.requestor) {
          clearTimeout(this.requestor)
        }
        this.requestor = setTimeout(() => {
          this.$httpRequester.put('filter', params).then(() => {
            // console.log('success')
          })
        }, 1000)
      } else {
        const params = {
          filterCondition: JSON.stringify(saveOb),
          companyId: userModule.companyId || 1, // todo:测试数据
          saveUserName: userModule.name,
          saveUserId: userModule.userId,
          conditionName: this.groupName
        }
        if (this.requestor) {
          clearTimeout(this.requestor)
        }
        this.requestor = setTimeout(() => {
          this.$httpRequester.post('filter', params).then(() => {
            this.$message.success('操作成功')
            this.handleCancelSave()
          })
        }, 1000)
      }
    }
  }
  showSaveModl() {
    this.groupName = ''
    const findBlockIndex = this.filterOption.findIndex(
      (e: any) => e.type === -1
    )
    if (
      (this.filterOption.length === 0 && !this.qualification.length) ||
      (findBlockIndex > -1 && !this.qualification.length)
    ) {
      this.$message.error('请选择条件再保存')
    } else {
      this.saveModal = true
    }
  }
  handleCancelSave() {
    this.saveModal = false
  }
  setFilterName(value: string) {
    // console.log(value)
  }
  selectQualification(e: any) {
    this.filterData.qualification.data = e.map((s: any) => {
      return s.value
    })
  }
  expendNode(key: any) {
    const expendIndex = this.defaultExpend.findIndex((e: any) => e === key)
    if (expendIndex > -1) {
      this.defaultExpend.splice(expendIndex, 1)
    } else {
      this.defaultExpend.push(key)
    }
  }
  selectTreeNode(e: any, index: number) {
    // console.log(e, index)
    this.filterOption[index].value = e
  }
  searchTable() {
    // console.log(this.filterOption)
    const keys = Object.keys(this.filterData)
    const arryTypes = [
      'qualificationCount',
      'qualification',
      'effectiveDate',
      'areaCode',
      'shareholderName',
      'establishmentDate',
      'registeredCapital'
    ]
    const strTypes = [
      'isReceived',
      'certificateNo',
      'companyName',
      'legalPerson'
    ]
    keys.forEach((e: any) => {
      const find = this.filterOption.find((f: any) => f.key === e)
      if (e === 'qualification') {
        return
      } else if (find) {
        this.filterData[e].data = find.value
        if (find.key === 'establishmentDate' || find.key === 'effectiveDate') {
          // console.log('find', find)
          this.filterData[e].data = [
            this.$moment(find.value[0]).format('YYYY-MM-DD'),
            this.$moment(find.value[1]).format('YYYY-MM-DD')
          ]
          // console.log(this.filterData[e])
        } else if (find.type === this.NUMBER_RANG) {
          // console.log(find)
          this.filterData[e].data = [find.value.min || 0, find.value.max || 0]
        } else if (find.type === this.MULIT_INPUT) {
          this.filterData[e].data = find.value.split(/，|,/)
        } else if (find.key === 'isReceived') {
          this.filterData[e].data = find.value.length ? find.value : ''
        }
      } else {
        const findArray = arryTypes.find((a: any) => a === e)
        const findStr = strTypes.find((s: any) => s === e)
        // console.log(findArray)
        if (findArray) {
          this.filterData[e].data = []
        } else if (findStr) {
          this.filterData[e].data = ''
        }
        //   console.log(e, this.filterData[e].data)
      }
    })
    // console.log(this.filterData)
    this.$emit('setFilterOptions', this.filterData)
  }
  selectOptions(val: any, index: any, item: any) {
    this.filterOption[index].value = val
    this.$set(this.filterOption, index, this.filterOption[index])
  }
  selectFilterOption(index: number, e: any) {
    const inputInfo = this.inputOptions[e]
    this.filterOption[index].title = inputInfo.label
    this.filterOption[index].key = inputInfo.value
    this.filterOption[index].input = inputInfo.input
    this.filterOption[index].type = inputInfo.input.type
    this.filterOption[index].optionValue = e
    switch (inputInfo.value) {
      case 'companyName': // 企业名称,法定代表人名称
      case 'legalPerson':
      case 'certificateNo':
      case 'shareholderName':
        this.filterOption[index].value = ''
        break
      case 'establishmentDate':
      case 'effectiveDate':
        this.filterOption[index].value = []
        this.filterOption[index].options = []
        break
      case 'isReceived':
        this.filterOption[index].value = []
        this.filterOption[index].options = [
          { label: '有', value: '1' },
          { label: '无', value: '2' }
        ]
        break
      case 'areaCode':
        this.filterOption[index].value = []
        this.filterOption[index].options = homeModule.areaList
        break
      case 'registeredCapital':
        this.filterOption[index].value = { min: '', max: '' }
        this.filterOption[index].options = [{ text: '万元' }, { text: 0 }]
        break
      case 'qualificationCount':
        this.filterOption[index].value = { min: '', max: '' }
        this.filterOption[index].options = [{ text: '个' }, { text: 0 }]
        break
    }
    // console.log(this.filterOption)
    this.setInputOptions()
  }
  setInputOptions() {
    this.inputOptions.forEach(e => {
      const findIndex = this.filterOption.findIndex(i => e.value === i.key)
      if (findIndex > -1) {
        e.disabled = true
      } else {
        e.disabled = false
      }
    })
  }
  removeThisFilter(index: number) {
    this.filterOption.splice(index, 1)
    this.setInputOptions()
  }
  disabledEndDate(endValue: any) {
    const currentDate = new Date().getTime()
    return endValue.valueOf() > currentDate
  }
  changeRangeDate(index: number, e: any) {
    const oldValue = this.filterOption[index]
    const newValue = { ...oldValue }
    newValue.value = e
    this.$set(this.filterOption, index, newValue)
  }

  setMinNumber(index: number, e: any) {
    const oldValue = this.filterOption[index]
    const newValue = { ...oldValue }
    newValue.value.min = e
    this.$set(this.filterOption, index, newValue)
  }
  setMaxNumber(index: number, e: any) {
    const oldValue = this.filterOption[index]
    const newValue = { ...oldValue }
    newValue.value.max = e
    this.$set(this.filterOption, index, newValue)
  }
  setMinInput(index: number) {
    const oldValue = this.filterOption[index]
    const newValue = { ...oldValue }
    newValue.options[1].text = oldValue.value.min
    this.$set(this.filterOption, index, newValue)
  }
  addNewFilterSelect() {
    this.haveAdd = true
    const findBlock = this.filterOption.find((e: any) => e.type === -1)
    // console.log(findBlock)
    if (findBlock) {
      this.$message.error('请先选择筛选条件再添加')
      return
    }
    this.filterOption.push({ title: [], type: -1 })
    this.setInputOptions()
  }

  created() {
    this.$httpRequester
      .get('qualification/selQualificationAll')
      .then((r: any) => {
        r.forEach((e: any, i: number) => {
          this.qualificationOptions.push({
            title: e.qualificationName,
            label: e.qualificationName,
            value: e.qualificationName + i,
            key: e.qualificationName + i,
            selectable: false,
            checkable: false,
            children: e.qualificationLevels.map((q: any) => {
              return {
                label: e.qualificationName + q.level,
                title: q.level,
                value: q.id,
                key: q.id
              }
            })
          })
        })
      })
  }
  mounted() {
    this.$httpRequester.get('filter/queryFilterCriteria').then((r: any) => {
      r.forEach((e: any) => {
        e.input = JSON.parse(e.input)
        e.disabled = false
      })
      this.inputOptions = r
    })
    this.$nextTick(() => {
      this.areaOptions = homeModule.areaList
    })
  }
}
