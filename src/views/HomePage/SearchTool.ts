import { Button, Icon } from 'ant-design-vue'
import { Component, Vue } from 'vue-property-decorator'
import SearchFilter from './SearchFilter.vue'

@Component({
  components: {
    aButton: Button,
    aIcon: Icon,
    SearchFilter
  }
})
export default class SearchTool extends Vue {
  $refs!: {
    searchRef: any
  }
  isExpend = true
  setFilterOption(filter: any) {
    this.$refs.searchRef.setFilterOptionFromModal(filter as any)
  }
  setFilterOptions(options: any) {
    this.$emit('findFilterTableData', options)
  }
  collaspeOrExpend() {
    this.isExpend = !this.isExpend
  }
}
