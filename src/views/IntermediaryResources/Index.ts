import {
  Select,
  Table,
  Popover,
  Tooltip,
  Drawer,
  TreeSelect,
  Pagination
} from 'ant-design-vue'
import notFind from '@/assets/image/no_data_block.png'
import { detailModule } from '@/store/modules/detail'
import { Vue, Component, Watch } from 'vue-property-decorator'

@Component({
  components: {
    aSelect: Select,
    aTable: Table,
    aSelectOption: Select.Option,
    aPopover: Popover,
    aDrawer: Drawer,
    aTooltip: Tooltip,
    aTreeSelect: TreeSelect,
    aPagination: Pagination
  }
})
export default class IntermediaryResources extends Vue {
  notFind = notFind
  hideNotFind = false
  showNotFind = false
  instructions = [
    {
      info: '资质服务商分析地区目前只开放江苏省，其他省份的数据正在分析研究中'
    },
    { info: '现资质服务商数据不包含发证机关为住房和城乡建设部' },
    { info: '时间范围是指资质取得的时间' }
  ]
  filterOptions: any = { monthNum: 3, category: 1, codeList: [] }
  detailsData = { pageNum: 1, pageSize: 10, qualificationName: '' }
  qualificationCategory: any = '总承包二级资质'
  qualificationsList = [
    { value: 1, label: '总承包二级资质' },
    { value: 2, label: '总承包三级资质' },
    {
      value: 3,
      label: '专业承包一级资质'
    },
    { value: 4, label: '专业承包其他等级资质' }
  ]
  qualificationsListMap = new Map<number, any>([
    [1, '总承包二级资质'],
    [2, '总承包三级资质'],
    [3, '专业承包一级资质'],
    [4, '专业承包其他等级资质']
  ])
  timeRangeList = [
    { value: 3, label: '3个月以内' },
    { value: 6, label: '6个月以内' }
  ]
  codeData: any = []
  codeDefault: any = []
  SHOW_PARENT = (TreeSelect as any).SHOW_PARENT
  qualificationsColumns = [
    {
      title: '序号',
      dataIndex: 'index'
    },
    {
      dataIndex: 'qualificationName',
      slots: { title: 'customTitle' },
      scopedSlots: { customRender: 'name' }
    },
    // {  dataIndex: 'title', width: 120, scopedSlots: { customRender: 'title' }, },
    {
      title: '数量',
      dataIndex: 'num'
    },
    {
      title: '操作',
      scopedSlots: { customRender: 'operate' },
      width: 80
    }
  ]
  Columns = [
    {
      title: '序号',
      dataIndex: 'index',
      width: '50px'
    },
    {
      title: '地区',
      dataIndex: 'areaName',
      width: '60px'
    },
    {
      title: '公司名称',
      dataIndex: 'companyName',
      width: '250px',
      slots: { title: 'companyName' },
      scopedSlots: { customRender: 'companyName' }
    },
    {
      title: '法定代表人',
      dataIndex: 'legalPerson',
      width: '90px'
    },
    {
      title: '资质类别及等级',
      dataIndex: 'qualification',
      width: '250px'
    },
    {
      title: '取得时间',
      dataIndex: 'approvalTime',
      width: '100px'
    }
  ]
  regionCode: any = []
  qualificationsData = []
  detailsListData: any = []
  prevDisabled = true
  nextDisabled = false
  show = false
  totalSize = 0
  qualificationNum = ''
  qualificationDesignation = ''
  isClickFindBtn = false
  myChart: any = null

  itemRender(current: any, type: any, originalElement: any) {
    if (type === 'prev') {
      originalElement.children = undefined
      originalElement.text = ' 上一页 '
      // originalElement.data.class = originalElement.data.class + " red"
      return originalElement
    } else if (type === 'next') {
      originalElement.children = undefined
      originalElement.text = ' 下一页 '
      // originalElement.data.class = originalElement.data.class + " red"
      return originalElement
    }
    return originalElement
  }

  onChange(pageNum: any) {
    const current = this.$refs['heightBox'] as any
    if (current != null) {
      current.scrollIntoView()
    }
    const params = {
      category: this.filterOptions.category,
      codeList: this.regionCode,
      monthNum: this.filterOptions.monthNum,
      pageNum: pageNum,
      pageSize: this.detailsData.pageSize,
      qualificationName: localStorage.getItem('qualificationName')
    }
    this.detailsList(params)
  }

  listDataChange() {
    if (this.filterOptions.codeList[0] === this.codeDefault[0]) {
      const region: any = []
      this.codeData.forEach((item: any) => {
        region.push(item.value)
        item.children.forEach((d: any) => {
          region.push(d.value)
        })
      })
      this.regionCode = region
    } else {
      this.regionCode = this.filterOptions.codeList
    }
    const params = {
      monthNum: this.filterOptions.monthNum,
      category: this.filterOptions.category,
      codeList: this.regionCode
    }
    this.$httpRequester
      .post('intermediary/pool/query/statistics', params)
      .then((res: any) => {
        if (JSON.stringify(res) === '{}') {
          this.showNotFind = true
          this.hideNotFind = false
          this.qualificationsData = []
          this.detailsListData = []
          return
        } else {
          this.showNotFind = false
          this.hideNotFind = true
          this.options.xAxis[0].data = res.xAxisData
          this.options.series[0].data = res.seriesData
          this.viewDetails(res.xAxisData[0])
          localStorage.setItem('qualificationName', res.xAxisData[0])
          this.qualificationNum = res.seriesData[0]
          this.qualificationDesignation = res.xAxisData[0]
          // this.option.xAxis.data=res.xAxisData
          // this.option.series.data=res.seriesData
          // this.detailsData.qualificationName = res[0].qualificationName
          // const params = {
          //     category: this.filterOptions.category,
          //     codeList: this.regionCode,
          //     monthNum: this.filterOptions.monthNum,
          //     pageNum: 1,
          //     pageSize: 30,
          //     qualificationName: this.detailsData.qualificationName,
          // }
          // this.detailsList(params)
          // res.forEach((item: any, index: any) => {
          //     item.index = index + 1
          // })
          // this.qualificationsData = res
        }
      })
  }

  notFilter() {
    return false
  }

  detailsList(params: any) {
    this.$httpRequester
      .post('intermediary/pool/query/details', params)
      .then((res: any) => {
        const list = res.records
        list.forEach((item: any, index: any) => {
          item.index = index + 1
          item.legalPerson =
            item.legalPerson === null ? '---' : item.legalPerson
          item.approvalTime = this.$moment(item.approvalTime).format(
            'yyyy-MM-DD'
          )
        })
        this.detailsListData = list
        this.qualificationNum = res.total
        const length = res.total
        this.totalSize = res.total
        if (length > 10) {
          this.show = true
          const pageNum = res.current
          const totalNum = res.pages
          this.detailsData.pageNum = pageNum
          if (pageNum === 1) {
            this.prevDisabled = true
            this.nextDisabled = false
          } else if (pageNum === totalNum) {
            this.prevDisabled = false
            this.nextDisabled = true
          } else {
            this.prevDisabled = false
            this.nextDisabled = false
          }
        } else {
          this.show = false
        }
        this.isClickFindBtn = false
      })
  }

  handleChangeQualifications(val: any) {
    this.qualificationCategory = this.qualificationsListMap.get(val)
  }

  getClientList() {
    this.listDataChange()
  }

  renewFilter() {
    this.filterOptions.monthNum = 3
    this.filterOptions.category = 1
    this.filterOptions.codeList = this.codeDefault
  }

  getPopupContainer() {
    return document.getElementById('intermediaryFilter')
  }

  viewDetails(item: any) {
    // const current = this.$refs['heightBox'] as any
    // if (current != null && !this.isClickFindBtn) {
    //     current.scrollIntoView()
    // }
    this.detailsData.pageNum = 1
    this.detailsData.pageSize = 10
    const params = {
      category: this.filterOptions.category,
      codeList: this.regionCode,
      monthNum: this.filterOptions.monthNum,
      pageNum: this.detailsData.pageNum,
      pageSize: this.detailsData.pageSize,
      qualificationName: item
    }

    this.detailsList(params)
  }

  //上一页
  /*  prevData() {
        const current = this.$refs['heightBox'] as any
        if (current != null) {
          current.scrollIntoView()
        }
        this.detailsData.pageNum = this.detailsData.pageNum - 1
        const params = {
          category: this.filterOptions.category,
          codeList: this.regionCode,
          monthNum: this.filterOptions.monthNum,
          pageNum: this.detailsData.pageNum,
          pageSize: this.detailsData.pageSize,
          qualificationName: localStorage.getItem('qualificationName'),
        }
        this.detailsList(params)
      }*/
  //下一页
  /*  nextData() {
        const current = this.$refs['heightBox'] as any
        if (current != null) {
          current.scrollIntoView()
        }

        this.detailsData.pageNum = this.detailsData.pageNum + 1
        const params = {
          category: this.filterOptions.category,
          codeList: this.regionCode,
          monthNum: this.filterOptions.monthNum,
          pageNum: this.detailsData.pageNum,
          pageSize: this.detailsData.pageSize,
          qualificationName: localStorage.getItem('qualificationName')

        }
        this.detailsList(params)
      }*/

  code() {
    this.$httpRequester.get('regionCode/query', {}).then((res: any) => {
      res.forEach((item: any) => {
        if (item.level === 1) {
          this.filterOptions.codeList.push(item.areaCode)
          this.codeDefault = this.filterOptions.codeList
          const obj = {
            title: item.areaName,
            value: item.areaCode,
            key: item.areaCode,
            children: []
          }
          this.codeData.push(obj)
          this.$nextTick(() => {
            this.listDataChange()
          })
        } else {
          const arr = {
            title: item.areaName,
            value: item.areaCode,
            key: item.areaCode
          }
          this.codeData.forEach((d: any) => {
            d.children.push(arr)
          })
        }
      })
    })
  }
  companyNameSearch(companyName: any) {
    detailModule.setCompanyNameToFilter(companyName)
    // this.$emit('setCompanyNameToFilter', companyName)
    this.$router.push({
      name: 'receipt'
    })
  }

  options: any = {
    color: ['#3398DB'],
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        // 坐标轴指示器，坐标轴触发有效
        type: 'line'
        // 默认为直线，可选为：'line' | 'shadow'
      }
    },
    grid: {
      width: 'auto',
      height: 'auto',
      left: 50,
      right: 50,
      bottom: 30,
      top: 30,
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        data: [],
        axisTick: {
          alignWithLabel: true
        },
        axisLabel: {
          interval: 0,
          rotate: 40
        },
        triggerEvent: true
      }
    ],
    yAxis: [
      {
        type: 'value',
        axisLabel: {
          interval: 0
        },
        min: 0,
        scale: true /*按比例显示*/,
        triggerEvent: true
      }
    ],
    series: [
      {
        name: '资质总数',
        type: 'bar',
        barWidth: '25%',
        data: [],
        itemStyle: {
          normal: {
            label: {
              show: true,
              position: 'top',
              textStyle: {
                color: '#000000'
              }
            }
          }
        }
      }
    ]
  }

  @Watch('options', { immediate: true, deep: true })
  handler(newVal: any, oldVal: any) {
    if (this.myChart) {
      if (newVal) {
        this.myChart.setOption(newVal)
      } else {
        this.myChart.setOption(oldVal)
      }
    }
  }

  drawLine() {
    //基于准本好的DOM，初始化echarts实例
    this.myChart = (this as any).$echarts.init(
      document.getElementById('myHomeChart')
    )
    this.myChart.setOption(this.options)
    const _that = this
    this.myChart.on('click', function(params: any) {
      if (params.componentType == 'xAxis') {
        // console.log("单击了"+params.value+"x轴标签");
        _that.viewDetails(params.value)
        localStorage.setItem('qualificationName', params.value)
        _that.qualificationDesignation = params.value
      } else {
        _that.viewDetails(params.name)
        localStorage.setItem('qualificationName', params.name)
        _that.qualificationNum = params.value
        _that.qualificationDesignation = params.name
      }
    })
  }

  @Watch('hideNotFind', { deep: true })
  hideHandler(newVal: any) {
    if (newVal) {
      this.$nextTick(() => {
        this.drawLine()
      })
    }
  }

  mounted() {
    this.code()
    this.$nextTick(() => {
      const dom = document.getElementById('areaTree') as any
      const areaTreeInput = dom.getElementsByTagName('input')[0] as any
      const areaTreeFather = areaTreeInput.parentElement
      areaTreeFather.removeChild(areaTreeInput)
    })
  }

  destroyed() {
    localStorage.removeItem('qualificationName')
  }

  // updated() {
  //     // const dom = this.$refs['heightBox'] as any
  //     this.$nextTick(() => {
  //         setTimeout(() => {
  //             document.documentElement.scrollTop = div.scrollHeight;
  //             console.log(document.documentElement.scrollTop)
  //         }, 500);
  //     });
  // }
  // updated() {
  // var div = this.$refs['heightBox'] as any;
  //
  //     //当页面的的数据发生变化的时候,获取右边的盒子的高度,
  //     // console.log(dom.getBoundingClientRect().top)
  // //     //获取与浏览器顶部的距离,当页面数据发生变化时,显示到页面顶部
  // }
}
