import { Select, Table, Popover, Tooltip, Drawer } from 'ant-design-vue'
import { Vue, Component } from 'vue-property-decorator'
import QA from '@/assets/image/knowledgeWarehouse/ic_news_question.png'
import policy from '@/assets/image/knowledgeWarehouse/ic_news_folder.png'
import gov from '@/assets/image/knowledgeWarehouse/ic_news_government.png'
import readMore from '@/assets/image/knowledgeWarehouse/ic_arrow_right_green.png'
import topBanner from '@/assets/image/knowledgeWarehouse/topBanner.jpg'
import buttonBanner from '@/assets/image/knowledgeWarehouse/buttonBanner.png'
import urlIcon from '@/assets/image/knowledgeWarehouse/SPRC.png'
import {universalModule} from "@/store/modules/universal";

@Component({
  components: {
    aSelect: Select,
    aTable: Table,
    aSelectOption: Select.Option,
    aPopover: Popover,
    aDrawer: Drawer,
    aTooltip: Tooltip
  }
})
export default class HomePage extends Vue {
  QA = QA
  policy = policy
  topBanner = topBanner
  buttonBanner = buttonBanner
  gov = gov
  urlIcon = urlIcon
  readMore=readMore
  leftDataList = []
  rightDataList = []
  urlData = [
    { title: '国家政务服务平台', url: 'http://www.gjzwfw.gov.cn' },
    {
      title: '中华人民共和国中央人民政府',
      url: 'http://www.gov.cn'
    },
    {
      title: '中华人民共和国住房和城乡建设部',
      url: 'http://www.mohurd.gov.cn/'
    },
    { title: '国家工程建设标准化信息网', url: 'http://www.ccsn.org.cn/' },
    {
      title: '住房和城乡建设部职业资格注册中心',
      url: 'http://www.pqrc.org.cn/'
    },
    {
      title: '全国建筑工人管理服务信息平台',
      url: 'http://jzgr.mohurd.gov.cn/'
    },
    {
      title: '国家企业信用信息公示系统',
      url: 'http://www.gsxt.gov.cn/index.html'
    },
    { title: '中国执行信息公开网', url: 'http://zxgk.court.gov.cn/' },
    { title: '中华人民共和国民政部', url: 'http://www.mca.gov.cn/' }
  ]
  open(link: any) {
    const shell = (window as any).shell
    // console.log(shell)
    if (shell) {
      shell.openExternal(link)
    }else {
      window.open(link)
    }

  }
  leftDetails(id: any) {
    this.$router.push({
      path: '/longna/knowledgeWarehouse/PolicyDocDetails',
      query: { id: id }
    })
  }

  queAndQns(id: any) {
    this.$router.push({
      path: '/longna/knowledgeWarehouse/QueAndQnsDetails',
      query: { id: id }
    })
  }

  more() {
    const history = {name: 'policyDocList', title: '政策文件'}
    universalModule.changeHistoryListItem(history)
    this.$router.push('/longna/knowledgeWarehouse/policyDocList')
  }

  moreQA() {
    const history = {name: 'QueAndQnsList', title: '资质问答'}
    universalModule.changeHistoryListItem(history)
    this.$router.push('/longna/knowledgeWarehouse/QueAndQnsList')
  }
  getSimpleText(html: any) {
    const regExp = new RegExp('<.+?>', 'g')
    return '<div>' + html.replace(regExp, '') + '</div>'
  }
  leftList() {
    this.$httpRequester
      .post('notice/selKnowledge', {
        pageNum: 1,
        pageSize: 15,
        noticeTypeEnum: '0'
      })
      .then((res: any) => {
        res.data.forEach((item: any) => {
          item.sendTime = this.$moment(item.sendTime).format('yyyy-MM-DD')
        })
        this.leftDataList = res.data
        this.rightList()
      })
  }

  rightList() {
    this.$httpRequester
      .post('notice/selKnowledge', {
        pageNum: 1,
        pageSize: 5,
        noticeTypeEnum: '3'
      })
      .then((res: any) => {
        res.data.forEach((item: any) => {
          item.sendTime = this.$moment(item.sendTime).format('yyyy-MM-DD')
        })
        res.data.forEach((item: any) => {
          item.content = this.getSimpleText(item.content)
        })
        this.rightDataList = res.data
      })
  }

  mounted() {
    this.leftList()
  }
}
