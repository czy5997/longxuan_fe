import {
    Input,
    Select,
    DatePicker,
    Table,
    Popover,
    Modal,
    Tooltip,
    Drawer,
    TreeSelect,
    Breadcrumb
} from 'ant-design-vue'
import follow from './Communicate.vue'
import {Vue, Component} from 'vue-property-decorator'
import {userModule} from '@/store/modules/user'
import {detailModule} from '@/store/modules/detail'
import notFind from '@/assets/image/no_data_block.png'
import BlockData from '@/components/BlockData.vue'
import {universalModule} from '@/store/modules/universal'
import staffSelect from './StaffSelect.vue'

@Component({
    components: {
        aTreeSelect: TreeSelect,
        aTreeSelectNode: TreeSelect.TreeNode,
        aModal: Modal,
        aInput: Input,
        aDateRange: DatePicker.RangePicker,
        aSelect: Select,
        aSelectOption: Select.Option,
        aTable: Table,
        aPopover: Popover,
        aDrawer: Drawer,
        aTooltip: Tooltip,
        BlockData,
        staffSelect,
        follow,
        aBreadcrumb: Breadcrumb,
        aBreadcrumbItem: Breadcrumb.Item
    },
    filters: {
        filter20: (value: string) => {
            if (value.length > 20) {
                return value.substring(0, 20) + '...'
            } else {
                return value
            }
        }
    }
})
export default class FollowUp extends Vue {
    $refs!: {
        communicate: any
        searchQua: any
        assignModal: any
        statusFormModal: any
        companyDrawer: any
    }
    followPhoneList: any = []
    followId = ''
    filterOptions: any = {
        enterpriseName: ''
        // followStatus: '',
    }
    subordinateArr = [] as any
    followStaff: any = []
    isLead = userModule.isLead
    notFind = notFind
    isSuperAdmin = userModule.roleLevel === 1
    isSales = userModule.roleLevel < 3
    pageSize = 10
    pageNum = 1
    dateRanger: any = [null, null]
    statusMap = new Map<any, any>([
        ['INTENTION', '有意向'],
        ['NO_INTENTION', '无意向'],
        ['HANG_UP', '挂断/打错了/同行'],
        ['RESIGNATION', '辞职/公司倒闭'],
        ['NO_DEAL', '未成交'],
        ['DEAL', '已成交']
    ])
    columns = [
        {
            title: '序号', dataIndex: 'index', width: '72px', align: 'left'
        },
        {
            title: '企业信息',
            dataIndex: 'companyInfo',
            width: '226px',
            align: 'left',
            scopedSlots: {
                customRender: 'companyInfo'
            }
        },
        {title: '联系人', dataIndex: 'contactsPerson', width: '118px', align: 'left'},
        {title: '联系方式', dataIndex: 'contactPhone', width: '125px', align: 'left'},
        {
            title: '跟进人',
            dataIndex: 'followUserName',
            width: '125px', align: 'left'
        },
        {
            title: '跟进状态',
            dataIndex: 'followStatus',
            width: '100px',
            scopedSlots: {customRender: 'followStatus'}, align: 'left'
        },
        {
            title: '跟进记录',
            dataIndex: 'remarks',
            width: '180px',
            scopedSlots: {customRender: 'followRemarks'}, align: 'left'
        },
        {
            title: '状态更新时间',
            dataIndex: 'updateTime',
            // sorter: (a: any, b: any) =>
            //   new Date(a.updateTime).getTime() - new Date(b.updateTime).getTime(),
            // sortDirections: ['descend', 'ascend'],
            width: 150,
            align: 'left'
        },
        {
            title: '操作',
            width: '220px',
            dataIndex: 'operate',
            scopedSlots: {customRender: 'operate'},
            align: 'left'

        }
    ]
    tableData = []
    total = 0
    status = -1

    get pagination() {
        return {
            total: this.total,
            pageSize: this.pageSize,
            current: this.pageNum,
            showSizeChanger: false,
            showQuickJumper: true,
            /*onChange: (pageNum: number) => {
                                  this.changePageNum(pageNum)
                              },*/
            onShowSizeChange: (page: any, pageSize: any) => {
                this.pageSize = pageSize
                this.pageNum = page
            },
            onChange: (page: any, pageSize: any) => {
                //仅页码改变才触发,页面大小修改不会触发
                // console.log('onChange' + 'pageSize ' + pageSize + ' page' + page)
                this.pageSize = pageSize
                this.pageNum = page
                //this.changePageNum()
            }
        }
    }

    getPopupContainer() {
        return document.getElementById('clientFilter')
    }

    changePageNum() {
        this.getClientList()
    }

    disabledEndDate(endValue: any) {
        const currentDate = new Date().getTime()
        return endValue.valueOf() > currentDate
    }

    handleTableChange(pagination: any, filters: any, sorter: any) {
        // console.log("handleTableChange" + " pagesize " + pagination.pageSize + " pageNum " + pagination.pageNum);
        this.pageSize = pagination.pageSize
        this.getClientList()
    }

    getNextDate(date: any, day = 1, format = '{y}-{m}-{d}') {
        if (date) {
            const nDate = new Date(date)
            nDate.setDate(nDate.getDate() + day)

            const formatObj = {
                y: nDate.getFullYear(),
                m: nDate.getMonth() + 1,
                d: nDate.getDate()
            } as any
            return format.replace(/{([ymd])+}/g, (result, key: any) => {
                const value = formatObj[key]
                return value.toString().padStart(2, '0')
            })
        } else {
            throw new Error('getNextDate:错误的参数')
        }
    }

    getClientList() {
        console.log('this.followStaff ', this.followStaff)
        const params = {
            // ...this.filterOptions,
            enterpriseName: this.filterOptions.enterpriseName,
            followUserId: this.followStaff || [],
            pageNum: this.pageNum,
            pageSize: this.pageSize,
            startDate: this.dateRanger[0]
                ? this.$moment(this.dateRanger[0]).format('yyyy-MM-DD')
                : '',
            endDate: this.dateRanger[1]
                ? this.getNextDate(
                    this.$moment(this.dateRanger[1]).format('yyyy-MM-DD'),
                    1
                )
                : ''
        }
        this.$httpRequester
            .post('bm/clue/selBmEnterpriseCluePage', params)
            .then((r: any) => {
                r.data.forEach((e: any, i: number) => {
                    e.companyInfo = {}
                    e.index = i + 1
                    e.followStatus = this.statusMap.get(e.followStatus)
                    e.companyInfo = {
                        companyName: e.companyName,
                        legalPerson: e.legalPerson
                    }
                    e.updateTime = this.$moment(e.updateTime).format(
                        'YYYY-MM-DD HH:mm:ss'
                    )
                })
                this.tableData = r.data
                this.total = r.totalSize
            })
    }

    renewFilter() {
        if (this.$refs.searchQua) {
            this.$refs.searchQua.clearFollowStaffId()
        }
        if (userModule.roleLevel === 4) {
            this.followStaff = [userModule.userId]
        } else {
            this.followStaff = []
        }
        this.filterOptions = {
            enterpriseName: ''
            // followStatus: '',
        }
        this.status = -1
        this.dateRanger = [null, null]
    }

    followStaffChange(val: any) {
        if (val) {
            this.followStaff = val
        } else {
            this.followStaff = []
        }
    }

    goToDetail(item: any) {
        this.$router.push({
            name: 'talentsDetail',
            query: {
                enterpriseId: item.enterpriseId
            }
        })
    }

    goToFollowList(item: any) {
        this.$router.push({
            name: 'talentsDetail',
            query: {
                enterpriseId: item.enterpriseId,
                tabIndex: '4'
            }
        })
    }

    addNewAssign(item: any) {
        this.followId = item.enterpriseId
        this.followPhoneList = [item.contactPhone]
        this.$refs.communicate.showModal(item.contactPhone)
    }

    confirmFollow(data: any) {
        const params = {
            ...data,
            enterpriseId: this.followId
        }
        // console.log(params)
        this.$httpRequester
            .post('bm/follow/addFollowRecord', params)
            .then(() => {
                this.$message.success('跟进成功')
                this.$refs.communicate.modalHandleCancel()
                this.getClientList()
            })
            .catch(() => {
                this.$refs.communicate.modalHandleCancel()
                this.getClientList()
            })
    }

    async getFollowUserId() {
        const params = {
            companyId: userModule.companyId
        }
        this.followStaff = []
        this.$httpRequester
            .get('member/listAllByDepartment', params)
            .then((r: any) => {
                r.forEach((e: any) => {
                    e.subordinateOrg.forEach((i: any) => {
                        i.subordinateOrg.forEach((d: any) => {
                            const userId = userModule.userId as any
                            if (d.id === parseInt(userId)) {
                                this.subordinateArr = i.subordinateOrg
                            }
                        })
                    })
                })

                this.subordinateArr.forEach((item: any) => {
                    this.followStaff.push(JSON.stringify(item.id))
                })
            })
    }

    mounted() {
        if (userModule.roleLevel === 3 || userModule.isLead) {
            this.getFollowUserId()
            setTimeout(() => {
                this.getClientList()
            }, 200)
        } else if (userModule.roleLevel === 4) {
            this.followStaff = [userModule.userId]
            this.getClientList()
        } else {
            this.getClientList()
        }
    }
}
