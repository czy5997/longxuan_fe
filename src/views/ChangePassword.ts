import { Component, Vue } from 'vue-property-decorator'
import ChangePwd from '@/views/Registered/ChangePwd.vue'

@Component({
  components: {
    ChangePwd
  }
})
export default class ChangePassword extends Vue {}
