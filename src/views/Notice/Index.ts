import {
  Select,
  Table,
  Popover,
  Pagination,
  Tooltip,
  Drawer,
  Input,
  TreeSelect,
  DatePicker
} from 'ant-design-vue'
import notFind from '@/assets/image/no_data_block.png'
import { Vue, Component } from 'vue-property-decorator'
import TreeCheckboxSingleSelect from '@/components/TreeCheckboxSingleSelect.vue'

import { universalModule } from '@/store/modules/universal'
@Component({
  components: {
    aSelect: Select,
    aTable: Table,
    aSelectOption: Select.Option,
    aPopover: Popover,
    aPagination: Pagination,
    aDrawer: Drawer,
    aTooltip: Tooltip,
    aDateRange: DatePicker.RangePicker,
    aRangePicker: DatePicker.RangePicker,
    aTreeSelect: TreeSelect,
    aTreeSelectNode: TreeSelect.TreeNode,
    aInput: Input,
    TreeCheckboxSingleSelect
  }
})
export default class Index extends Vue {
  notFind = notFind
  filterData = {
    current: 1,
    notice: 1,
    qualifications: 1 as number | any,
    region: undefined as any,
    opinion: undefined as any,
    approval: undefined as any,
    releaseTime: null,
    companyName: undefined as any
  }

  get qualificationOptions() {
    return universalModule.typeQualificationOptions
  }

  get fakeQualification() {
    return universalModule.fakeQualificationOptions
  }
  totalSize = 0
  reallyQua = null
  defaultPageSize = 25
  time = ''
  tableData = []
  areaName = ''
  title: any = '公示'
  noticeList = [
    { value: 1, label: '公示' },
    { value: 2, label: '公告' },
    {
      value: 3,
      label: '资质变更'
    },
    { value: 4, label: '控股公司' }
  ]
  qualificationsList = [
    { value: 1, label: '建筑企业资质分立' },
    { value: 2, label: '建筑施工企业资质审核' },
    {
      value: 3,
      label: '施工企业安全生产许可证审核'
    }
  ]
  noticeListMap = new Map<number, any>([
    [1, '公示'],
    [2, '公告'],
    [3, '资质变更'],
    [4, '控股公司']
  ])
  explainText = {
    explain:
      '说明：公示出来的公司在资质申报、分立，新办安许等信息，您可以根据审批意见处理公司业务:<br/>1. 审批意见为“同意”，您可以联系收购企业的资质,<br/>2. 审批意见为“不同意“，您可以联系企业为其办理申报或者新办安许等业务',
    source: '3. 数据来源：江苏省建设厅、四川省建设厅、云南建设厅'
  }
  isChoice = false
  isChoiceOpinion = false
  regionList = [
    { value: 1, label: '江苏省' },
    { value: 2, label: '四川省' },
    { value: 3, label: '云南省' }
  ]
  regionListMap = new Map<number, any>([
    [1, '江苏省'],
    [2, '四川省'],
    [3, '云南省']
  ])
  opinionList = [
    { value: 1, label: '公示-通过' },
    { value: 0, label: '公示-不通过' }
  ]
  // ,{value:3,label:"公告-通过"},{value:4,label:"公告-不通过"}
  columns: any = []
  //   qualificationOptions = [] as any
  defaultExpend = [] as any
  itemRender(current: any, type: any, originalElement: any) {
    if (type === 'prev') {
      originalElement.children = undefined
      originalElement.text = ' 上一页 '
      // originalElement.data.class = originalElement.data.class + " red"
      return originalElement
    } else if (type === 'next') {
      originalElement.children = undefined
      originalElement.text = ' 下一页 '
      // originalElement.data.class = originalElement.data.class + " red"
      return originalElement
    }
    return originalElement
  }

  onChange(current: any) {
    this.filterData.current = current
    this.listData()
  }
  getPopupQualifications() {
    return document.getElementById('qualifications')
  }
  getPopupReleaseEnterpriseName() {
    return document.getElementById('EnterpriseName')
  }

  getPopupRegion() {
    return document.getElementById('region')
  }

  getPopupContainer() {
    return document.getElementById('filterBox')
  }
  getPopupApproval() {
    return document.getElementById('approval')
  }

  getPopupReleaseTimeDom() {
    return document.getElementById('releaseTimeDom')
  }

  holdingData(params: any) {
    this.$httpRequester
      .post('holding/relation/query', params)
      .then((res: any) => {
        this.tableData = res.data
        this.totalSize = res.totalSize
        this.noticeChangeTable(4, null)
      })
  }

  publicityData(params: any) {
    this.$httpRequester
      .post('qualification/publicity/query', params)
      .then((res: any) => {
        res.records.forEach((item: any) => {
          item.releaseTime = this.$moment(item.releaseTime).format('yyyy-MM-DD')
        })
        this.tableData = res.records
        this.totalSize = res.total
        this.noticeChangeTable(1, params.qualificationOrSafety)
      })
  }

  noticeData(params: any) {
    this.$httpRequester
      .post('qualification/notic/query', params)
      .then((res: any) => {
        res.records.forEach((item: any) => {
          item.releaseTime = this.$moment(item.releaseTime).format('yyyy-MM-DD')
        })
        this.tableData = res.records
        this.totalSize = res.total
        this.noticeChangeTable(2, params.qualificationOrSafety)
      })
  }

  qualificationData(params: any) {
    this.$httpRequester
      .post('qualification/bedone/query', params)
      .then((res: any) => {
        res.records.forEach((item: any) => {
          item.releaseTime = this.$moment(item.releaseTime).format('yyyy-MM-DD')
        })
        this.tableData = res.records
        this.totalSize = res.total
        this.noticeChangeTable(3, null)
      })
  }

  listData() {
    if (this.filterData.region === undefined) {
      this.areaName = ''
    } else {
      this.areaName = this.regionListMap.get(this.filterData.region)
    }
    const paramsData = {
      pageNum: this.filterData.current,
      pageSize: this.defaultPageSize,
      qualificationOrSafety: this.filterData.qualifications,
      qualificationType: this.filterData.opinion,
      //   qualificationType: this.reallyQua,
      approvalStatus: this.filterData.approval,
      areaName: this.areaName,
      startTime: this.time[0] ? this.time[0] : '',
      endTime: this.time[1] ? this.time[1] : '',
      companyName: this.filterData.companyName
    }
    if (this.filterData.notice === 1) {
      this.publicityData(paramsData)
    } else if (this.filterData.notice === 2) {
      this.noticeData(paramsData)
    } else if (this.filterData.notice === 3) {
      const params = {
        pageNum: this.filterData.current,
        pageSize: this.defaultPageSize,
        areaName: this.areaName,
        qualificationType: this.reallyQua,
        startTime: this.time[0] ? this.time[0] : '',
        endTime: this.time[1] ? this.time[1] : '',
        companyName: this.filterData.companyName
      }
      this.qualificationData(params)
    } else if (this.filterData.notice === 4) {
      const params = {
        pageNum: this.filterData.current,
        pageSize: this.defaultPageSize,
        areaName: this.areaName,
        startTime: this.time[0] ? this.time[0] : '',
        endTime: this.time[1] ? this.time[1] : '',
        holdingCompanyName: this.filterData.companyName
      }
      this.holdingData(params)
    }
  }

  getClientList() {
    this.filterData.current = 1
    this.listData()
  }

  renewFilter() {
    this.isChoice = false
    this.isChoiceOpinion = false
    this.filterData.notice = 1
    this.reallyQua = null
    this.filterData.qualifications = 1
    this.filterData.region = undefined
    this.filterData.opinion = undefined
    this.filterData.approval = undefined
    this.filterData.releaseTime = null
    this.time = ''
    this.filterData.companyName = undefined
  }

  disabledDate(current: any) {
    // Can not select days before today and today
    return current && current > this.$moment()
  }

  noticeChangeTable(notice: number, qualifications: any) {
    this.noticeChangeText(notice)
    if (notice === 1) {
      if (qualifications === 1) {
        this.columns = [
          { title: '时间', dataIndex: 'releaseTime', width: '120px' },
          { title: '地区', dataIndex: 'areaName', width: '100px' },
          {
            title: '申报类别',
            dataIndex: 'declarationCategory',
            width: '100px'
          },
          { title: '申请企业名称', dataIndex: 'companyName',width:300 },
          { title: '变更企业', dataIndex: 'changeCompanyName' },
          {
            title: '申请资质类别及等级',
            dataIndex: 'qualificationType',
          },
          { title: '审批意见', dataIndex: 'approvalOpinions', width: '200px' }
        ]
      } else if (qualifications === 2) {
        this.columns = [
          { title: '时间', dataIndex: 'releaseTime', width: '120px' },
          { title: '地区', dataIndex: 'areaName', width: '70px' },
          { title: '企业名称', dataIndex: 'companyName' },
          {
            title: '申报类别',
            dataIndex: 'declarationCategory',
            width: '150px'
          },
          {
            title: '申请资质类别及等级',
            dataIndex: 'qualificationType',
          },
          { title: '审批意见', dataIndex: 'approvalOpinions', width: '250px' }
        ]
      } else if (qualifications === 3) {
        this.columns = [
          { title: '时间', dataIndex: 'releaseTime', width: '120px' },
          { title: '地区', dataIndex: 'areaName', width: '70px' },
          { title: '企业名称', dataIndex: 'companyName' },
          {
            title: '申请资质类别及等级',
            dataIndex: 'qualificationType',
          },
          { title: '审批意见', dataIndex: 'approvalOpinions', width: '300px' }
        ]
      }
    } else if (notice === 2) {
      if (qualifications === 1) {
        this.columns = [
          { title: '时间', dataIndex: 'releaseTime', width: '120px' },
          { title: '地区', dataIndex: 'areaName', width: '70px' },
          {
            title: '申报类别',
            dataIndex: 'declarationCategory',
            width: '100px'
          },
          { title: '申请企业名称', dataIndex: 'companyName' },
          { title: '变更企业', dataIndex: 'changeCompanyName'},
          {
            title: '申请资质类别及等级',
            dataIndex: 'qualificationType',
          },
          { title: '审批决定', dataIndex: 'approvalOpinions', width: '200px' }
        ]
      } else if (qualifications === 2) {
        this.columns = [
          { title: '时间', dataIndex: 'releaseTime', width: '120px' },
          { title: '地区', dataIndex: 'areaName', width: '70px' },
          { title: '企业名称', dataIndex: 'companyName' },
          {
            title: '申请资质类别及等级',
            dataIndex: 'qualificationType',
          },
          { title: '审批决定', dataIndex: 'approvalOpinions', width: '250px' }
        ]
      } else if (qualifications === 3) {
        this.columns = [
          { title: '时间', dataIndex: 'releaseTime', width: '120px' },
          { title: '地区', dataIndex: 'areaName', width: '70px' },
          { title: '企业名称', dataIndex: 'companyName' },
          {
            title: '申请资质类别及等级',
            dataIndex: 'qualificationType',
          },
          { title: '审批决定', dataIndex: 'approvalOpinions', width: '200px' }
        ]
      }
    } else if (notice === 3) {
      this.columns = [
        { title: '时间', dataIndex: 'releaseTime', width: '120px' },
        { title: '地区', dataIndex: 'areaName', width: '70px' },
        { title: '申请企业名称', dataIndex: 'companyName'},
        { title: '变更企业', dataIndex: 'changeCompanyName' },
        { title: '变更内容', dataIndex: 'changeContent', width: '250px' }
      ]
    } else if (notice === 4) {
      this.columns = [
        { title: '变更时间', dataIndex: 'changeDate', width: '120px' },
        { title: '地区', dataIndex: 'areaName', width: '100px' },
        {
          title: '被控股公司名称',
          dataIndex: 'holdingCompanyName',
        },
        {
          title: '公司股东(企业法人类型)',
          dataIndex: 'shareholderOrgName',
          width: '250px'
        }
      ]
    }
  }

  noticeChangeText(val: any) {
    this.title = this.noticeListMap.get(val)
    switch (val) {
      case 1:
        this.explainText = {
          explain:
            '说明：公示出来的公司在资质申报、分立，新办安许等信息，您可以根据审批意见处理公司业务:<br/>1. 审批意见为“同意”，您可以联系收购企业的资质,<br/>2. 审批意见为“不同意“，您可以联系企业为其办理申报或者新办安许等业务',
          source: '3. 数据来源：江苏省建设厅、四川省建设厅、云南建设厅'
        }
        break
      case 2:
        this.explainText = {
          explain:
            '说明：公告出来的公司在资质申报、分立，新办安许等信息，您可以根据审批意见处理公司业务：<br/>1. 审批意见为“同意”，您可以联系收购企业的资质<br/>2. 审批意见为“不同意“，您可以联系企业为其办理申报或者新办安许等业务',
          source: '3. 数据来源：江苏省建设厅、四川省建设厅、云南建设厅'
        }
        break
      case 3:
        this.explainText = {
          explain:
            '说明：1. 公司资质变更完成列出来的企业,你可以联系变更企业收货',
          source:
            '2. 数据来源：江苏省建设厅、四川建设厅、云南建设厅、国家企业信用信息公示系统'
        }
        break

      case 4:
        this.explainText = {
          explain: '说明：1. 有发生控股关系,说明这些公司的资质可能会发生变更',
          source:
            '2. 数据来源：江苏省建设厅、四川省建设厅、云南省建设厅、国家企业信用信息公示系统'
        }
    }
  }

  noticeChange(val: any) {
    this.filterData.notice = val
    switch (val) {
      case 1:
        this.filterData.qualifications = 1
        this.opinionList = [
          { value: 1, label: '公示-通过' },
          { value: 0, label: '公示-不通过' }
        ]
        this.isChoice = false
        this.isChoiceOpinion = false
        this.filterData.region = undefined
        this.filterData.opinion = undefined
        this.filterData.approval = undefined
        this.filterData.releaseTime = null
        this.filterData.companyName = undefined
        this.time = ''
        break
      case 2:
        this.filterData.qualifications = 1
        this.opinionList = [
          { value: 1, label: '公告-通过' },
          { value: 0, label: '公告-不通过' }
        ]
        this.isChoice = false
        this.isChoiceOpinion = false
        this.empty()
        break
      case 3:
        this.filterData.qualifications = undefined
        this.isChoice = true
        this.isChoiceOpinion = false
        this.empty()
        break
      case 4:
        this.filterData.qualifications = undefined
        this.isChoice = true
        this.isChoiceOpinion = true
        this.empty()
    }
  }

  empty() {
    this.reallyQua = null
    this.filterData.region = undefined
    this.filterData.opinion = undefined
    this.filterData.approval = undefined
    this.filterData.releaseTime = null
    this.time = ''
    this.filterData.companyName = undefined
  }

  handleChangeQualifications(value: any) {
    this.filterData.qualifications = value
    value === 3 ? (this.isChoiceOpinion = true) : (this.isChoiceOpinion = false)
  }

  regionChange(value: any) {
    this.filterData.region = value
  }

  opinionChange(value: any) {
    this.filterData.opinion = value
  }
  approvalChange(value: any) {
    this.filterData.approval = value
  }
  releaseTimeChange(date: any, dateString: any) {
    this.time = dateString
    // this.time = this.$moment(dateString).format('yyyy-MM-DD')
  }

  getQualificationOptions() {
    // this.qualificationOptions = []
    this.$httpRequester
      .get('qualification/selQualificationAll')
      .then((r: any) => {
        let qua = [] as any
        r.forEach((e: any, i: number) => {
          this.defaultExpend.push(e.qualificationName + i)
          this.qualificationOptions.push({
            title: e.qualificationName,
            label: e.qualificationName,
            value: e.qualificationName + i,
            key: e.qualificationName + i,
            selectable: false,
            checkable: false,
            children: e.qualificationLevels.map((q: any) => {
              return {
                selectable: true,
                checkable: true,
                disabled: false,
                label: e.qualificationName + q.level,
                title: q.level,
                value: q.id,
                key: q.id
              }
            })
          })
        })
        qua = this.$deepCopy(this.qualificationOptions)
      })
  }

  selectQualification(value: any) {
    this.reallyQua = value ? value.value : null
  }

  mounted() {
    if (!this.fakeQualification.length || !this.qualificationOptions.length) {
      universalModule.getTypeQualification()
      universalModule.getQualification()
    }
    this.$nextTick(() => {
      //   this.getQualificationOptions()
      this.getClientList()
    })
  }
}
