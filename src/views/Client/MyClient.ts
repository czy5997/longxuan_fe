import {
  Input,
  Select,
  DatePicker,
  Table,
  Popover,
  Modal,
  FormModel,
  Tooltip,
  Drawer,
  TreeSelect,
    Pagination
} from 'ant-design-vue'
import { Vue, Component } from 'vue-property-decorator'
import { userModule } from '@/store/modules/user'
import { detailModule } from '@/store/modules/detail'
import EditableCell from '@/components/EditableCell.vue'
import Assign from '../HomePage/Assign.vue'
import DetailIndex from '../Detail/Index.vue'
import notFind from '@/assets/image/no_data_block.png'
import BlockData from '@/components/BlockData.vue'
import { universalModule } from '@/store/modules/universal'
import staffSelect from './StaffSelect.vue'

@Component({
  components: {
    aTreeSelect: TreeSelect,
    aTreeSelectNode: TreeSelect.TreeNode,
    aModal: Modal,
    aFormModel: FormModel,
    aFormModelItem: FormModel.Item,
    aInput: Input,
    aDateRange: DatePicker.RangePicker,
    aSelect: Select,
    aSelectOption: Select.Option,
    aTable: Table,
    aPopover: Popover,
    aDrawer: Drawer,
    aTooltip: Tooltip,
    aPagination:Pagination,
    EditableCell,
    Assign,
    DetailIndex,
    BlockData,
    staffSelect
  },
  filters: {
    filter20: (value: string) => {
      if (value!==null&&value.length > 20) {
        return value.substring(0, 20) + '...'
      } else {
        return value || ''
      }
    }
  }
})
export default class MineClient extends Vue {
  $refs!: {
    searchQua: any
    assignModal: any
    statusFormModal: any
    companyDrawer: any
  }
  filterOptions: any = {
    enterpriseName: '',
    // followStatus: '',
    followUpUserId: []
  }
  sourceText = ['', '收货查询', '出货查询']
  sourceOptions = [
    { label: '收货查询', value: '1' },
    { label: '出货查询', value: '2' }
  ]
  clueSources = [] as any
  fakeQualification: any = []
  followStaff: any = []
  statusDisplay = false
  isLead = userModule.isLead
  notFind = notFind
  isSuperAdmin = userModule.roleLevel === 1
  isCompanyAdmin = userModule.roleLevel === 2
  isSales = userModule.roleLevel < 3
  pageSize = 10
  pageNum = 1
  dateRanger: any = [null, null]
  columns = [
    // { title: '序号', dataIndex: 'idDisplay', width: 70 },
    { title: '区域', dataIndex: 'areaName', width: '90px',         align: 'left'
    },
    {
      title: '企业全称',
      dataIndex: 'enterpriseName',
      width: '250px',
      scopedSlots: { customRender: 'goToCompanyDetail' },
      align: 'left'

    },
    {
      title: '沟通资质',
      scopedSlots: { customRender: 'qualificationItemsText' },
      dataIndex: 'qualificationItemsText',
      align: 'left'

      // width: '225px'
    },
    {
      title: '联系人',
      dataIndex: 'contacts',
      width: '85px',
         align: 'left'

  // scopedSlots: { customRender: 'phoneList' },
    },

    {
      title: '联系电话',
      dataIndex: 'phoneText',
      width: '135px',
      align: 'left'

      // scopedSlots: { customRender: 'phoneList' },
    },
    {
      title: '跟进状态',
      dataIndex: 'followStatus',
      width: '70px',
      scopedSlots: { customRender: 'followStatus' }
    },
    {
      title: '跟进记录',
      scopedSlots: { customRender: 'followRemarks' },
      dataIndex: 'remarks',
      width: '140px',
      align: 'left'
    },
    {
      title: '跟进人',
      dataIndex: 'memberName',
      width: '130px',
      align: 'left'
    },
    {
      title: '更新时间',
      dataIndex: 'updateTime',
      align: 'left',
      // sorter: (a: any, b: any) =>
      //   new Date(a.updateTime).getTime() - new Date(b.updateTime).getTime(),
      // sortDirections: ['descend', 'ascend'],
      width: '120px'
    },
    {
      title: '数据来源',
      dataIndex: 'sourceText',
      width: '90px'
    }
  ]
  tableData = []
  total = 0
  status = -1
  dateRangeBtns = [
    { title: '今日', value: 'today' },
    { title: '昨日', value: 'yesterday' },
    { title: '本周', value: 'week' }
  ]
  dateRangeIndex = -1
  staffList: any = []
  userSelect = []

  get pagination() {
    return {
      total: this.total,
      pageSize: this.pageSize,
      current: this.pageNum,
      showTotal: (total: number) => {
        return `共${total}条数据`
      },
      showSizeChanger: false,
      /*onChange: (pageNum: number) => {
                      this.changePageNum(pageNum)
                  },*/
      onShowSizeChange: (page: any, pageSize: any) => {
        this.pageSize = pageSize
        this.pageNum = page
      },
      onChange: (page: any, pageSize: any) => {
        //仅页码改变才触发,页面大小修改不会触发
        // console.log('onChange' + 'pageSize ' + pageSize + ' page' + page)
        this.pageSize = pageSize
        this.pageNum = page
        //this.changePageNum()
      }
    }
  }

  cssJudge = {
    green: 'INTENTION',
    yellow: 'UNCONFIRMED',
    blue: 'DEAL'
  }
  clientDetail = false
  selectedRowKeys: any = []
  receiveList: any = []
  statusList = [
    { value: 'INTENTION', text: '有意向', disabled: false },
    { value: 'NO_INTENTION', text: '无意向', disabled: false },
    { value: 'DEAL', text: '已成交', disabled: false },
    { value: 'NO_DEAL', text: '未成交', disabled: false },
    { value: 'UNCONFIRMED', text: '未确认', disabled: true }
  ]
  showCompanyDetailDrawer = false

  get rowSelection() {
    return {
      selectedRowKeys: this.selectedRowKeys,
      onChange: this.selectRows,
      getCheckboxProps: (record: any) => {
        return {
          props: {
            checked: this.selectedRowKeys.includes(record.enterpriseId)
          }
        }
      }
    }
  }

  formModel = {
    status: ''
  }
  qualificationOptions: any = []
  selectStatus = []

  getPopupContainer() {
    return document.getElementById('clientFilter')
  }

  showStatusModal() {
    if (!this.receiveList.length) {
      this.$message.error('请先选择线索后再更改状态')
      return
    }
    this.statusDisplay = true
  }

  changeItemsStatus() {
    this.$refs.statusFormModal.validate((val: boolean) => {
      // console.log(val)
      if (val) {
        const params = this.receiveList.map((e: any) => {
          return {
            enterpriseId: e.enterpriseId,
            enterpriseName: e.enterpriseName,
            followStatus: this.formModel.status
          }
        })
        this.$httpRequester
          .post('followRecord/addFollowRecord', params)
          .then(() => {
            this.$message.success('操作成功')
            this.closeStatusModal()
            this.getClientList()
          })
      } else {
        return val
      }
    })
  }

  closeStatusModal() {
    this.formModel.status = ''
    this.statusDisplay = false
  }

  selectRows(key: any, rows: any) {
    this.selectedRowKeys = key
    // console.log(rows)
    this.receiveList = rows
  }

  receiveEnterprise() {
    if (!this.receiveList.length) {
      this.$message.error('请先选择线索后再分配')
      return
    }
    this.$refs.assignModal.showModal()
  }

  assignSuccess() {
    this.receiveList = []
    this.getClientList()
  }

  onCellChange(value: any, item: any) {
    const params = {
      enterpriseId: item.enterpriseId,
      enterpriseName: item.enterpriseName,
      followStatus: value
    }
    this.$httpRequester
      .post('followRecord/addFollowRecord', [params])
      .then((r: any) => {
        // console.log(r)
        this.$message.success('操作成功')
        this.getClientList()
      })
  }

  closeCompanyDetailDrawer() {
    this.showCompanyDetailDrawer = false
  }

  goToCompanyDetail(item: any) {
    this.showCompanyDetailDrawer = true
    detailModule.setEnterpriseInfo(item)
    this.$nextTick(() => {
      this.$refs.companyDrawer.setBasicInfo()
    })
  }

  goToFollowList(item: any) {
    this.showCompanyDetailDrawer = true
    detailModule.setEnterpriseInfo(item)
    this.$nextTick(() => {
      this.$refs.companyDrawer.setBasicInfo(4)
    })
  }
  changePageNum(pageNum: number) {
    this.pageNum = pageNum
    this.getClientList()
  }
  disabledEndDate(endValue: any) {
    const currentDate = new Date().getTime()
    return endValue.valueOf() > currentDate
  }

  setDateRange(index: number, value: string) {
    if (this.dateRangeIndex === index) {
      this.dateRangeIndex = -1
      this.dateRanger = []
    } else {
      switch (value) {
        case 'today':
          this.dateRanger = [this.$moment(), this.$moment()]
          break
        case 'yesterday':
          this.dateRanger = [
            this.$moment().add(-1, 'days'),
            this.$moment().add(-1, 'days')
          ]
          break
        case 'week':
          this.dateRanger = [this.$moment().startOf('week'), this.$moment()]
          break
        default:
          break
      }
      this.dateRangeIndex = index
    }
  }

  selectDateRange() {
    this.dateRangeIndex = -1
  }

  setStatus(value: any) {
    if (value === this.status) {
      this.status = -1
      this.filterOptions.followStatus = ''
    } else {
      this.status = value
      this.filterOptions.followStatus = this.status
    }
  }

  userChange(value: any) {
    // console.log(value)
    this.filterOptions.followUpUserId = [value]
  }

  getStaffListForDepentment() {
    const params = {
      companyId: userModule.companyId
    }
    this.staffList = []
    this.$httpRequester
      .get('member/listAllByDepartment', params)
      .then((r: any) => {
        r.forEach((e: any, i: number) => {
          this.staffList.push({
            id: e.id || -1,
            children: e.subordinateOrg,
            subordinateOrg: e.subordinateOrg,
            title: e.name || '超级管理员',
            label: e.subordinateOrg,
            key: e.name ? e.name + i : '超级管理员' + i
          })
        })

        function setArrayToChildren(item: any, id: number) {
          if (item.subordinateOrg) {
            item.children = [] as any
            item.subordinateOrg.forEach((e: any, j: number) => {
              item.children.push({
                subordinateOrg: e.subordinateOrg,
                key: e.name ? e.name + id : '超级管理员' + id,
                title: e.name || '超级管理员',
                label: e.subordinateOrg,
                children: e.subordinateOrg || []
              })
              setArrayToChildren(e, j)
            })
          } else {
            item.children = []
            item.key = item.id + item.name
            item.title = item.name
            item.label = item.subordinateOrg
          }
        }

        function setArrayValue(item: any) {
          let value = ''

          function loopValue(i: any) {
            i.children.forEach((e: any, j: any) => {
              if (e.subordinateOrg) {
                e.value = 'level' + j + ',' + setArrayValue(e)
                loopValue(e)
              } else {
                e.value = 'min,' + e.id
                value += e.id + ','
              }
            })
          }

          loopValue(item)
          return value
        }

        this.staffList.forEach((e: any) => {
          setArrayToChildren(e, e.id)
        })
        this.staffList.forEach((e: any) => {
          e.value = 'root,' + setArrayValue(e)
        })
      })
  }

  showClientDescription() {
    this.clientDetail = true
  }

  closeClientDescription() {
    this.clientDetail = false
  }

  getLeadStaff() {
    const params = {
      upleadId: userModule.userId
    }
    this.$httpRequester.get('member/listAll', params).then((r: any) => {
      this.staffList = r.map((e: any) => {
        return {
          value: e.id,
          label: e.memberName,
          key: e.id
        }
      })
    })
  }

  //设置分页大小与过滤字段(改变后的页码及每页条数修改后都会触发)
  handleTableChange(pagination: any, filters: any, sorter: any) {
    // console.log("handleTableChange" + " pagesize " + pagination.pageSize + " pageNum " + pagination.pageNum);
    this.pageSize = pagination.pageSize
    this.getClientList()
  }
  beforeGetList() {
    if (this.$refs.searchQua) {
      this.$refs.searchQua.checkOutSelect()
    }
    this.pageNum = 1
    this.pageSize = 10
    this.getClientList()
  }
  getClientList() {
    const params = {
      // ...this.filterOptions,
      enterpriseName: this.filterOptions.enterpriseName,
      followUpUserIds: this.followStaff,
      clueSources: this.clueSources ? parseInt(this.clueSources, 10) : 0,
      pageNum: this.pageNum,
      pageSize: this.pageSize,
      startTime: this.dateRanger[0]
        ? this.$moment(this.dateRanger[0]).format('yyyy-MM-DD')
        : '',
      endTime: this.dateRanger[1]
        ? this.$moment(this.dateRanger[1]).format('yyyy-MM-DD')
        : ''
    }
    this.$httpRequester
      .post('followRecord/selMyCustom', params)
      .then((r: any) => {
        r.data.forEach((e: any, i: number) => {
          e.quaLength=e.qualificationItems.split(',').length
          e.idDisplay = this.pageSize * (this.pageNum - 1) + i + 1
          e.qualificationItemsText = e.qualificationItems
          e.phoneText = e.phone
          e.followStatusCode = e.followStatus
          e.followStatus = this.statusList.find(s => {
            return s.value === e.followStatus
          })?.text
          e.sourceText = this.sourceText[e.clueSources]
          e.remarks = e.remarks || ''
          //   e.ph/oneList.length > 1 ? e.phoneList[0] + '...' : e.phoneList[0]
          // e.phoneListText = e.phoneList.join(',')
        })
        this.tableData = r.data
        this.total = r.totalSize
      })
  }

  renewFilter() {
    if (this.$refs.searchQua) {
      this.$refs.searchQua.clearFollowStaffId()
    }
    this.filterOptions = {
      enterpriseName: '',
      // followStatus: '',
      followUpUserId: []
    }
    this.clueSources = []
    this.followStaff = []
    this.userSelect = []
    this.status = -1
    this.dateRangeIndex = -1
    this.dateRanger = [null, null]
    this.getClientList()
  }

  // 取消
  handleCancel() {
    this.clientDetail = false
  }

  followStaffChange(val: any) {
    if (val) {
      this.followStaff = val
    } else {
      this.followStaff = []
    }
  }

  setDepartmentStaff(option: any) {
    this.filterOptions.followUpUserId = []
    if (option.length) {
      option.forEach((o: any) => {
        const value = o.value.split(',')
        value.splice(0, 1)
        const filterValue = value.filter((e: any) => {
          return e
        })
        this.filterOptions.followUpUserId.push(...filterValue)
      })
    }
  }

  mounted() {
    this.$nextTick(() => {
      this.getClientList()
      if (userModule.roleLevel <= 3 || userModule.isLead) {
        this.getStaffListForDepentment()
        universalModule.getTypeQualification()
        universalModule.getQualification()
      }
    })
  }
}
