import {
  Input,
  Select,
  DatePicker,
  Table,
  Popover,
  Modal,
    Pagination,
    Button
} from 'ant-design-vue'
import { Vue, Component } from 'vue-property-decorator'
import EditableCell from '@/components/EditableCell.vue'
// import Operate from '@/components/Operate.vue'
import { homeModule } from '@/store/modules/home'

@Component({
  components: {
    aInput: Input,
    aDateRange: DatePicker.RangePicker,
    aSelect: Select,
    aSelectOption: Select.Option,
    aTable: Table,
    aPopover: Popover,
    aModal: Modal,
    aPagination:Pagination,
    EditableCell,
    // Operate,
    aButton:Button
  }
})
export default class Message extends Vue {
  pageSize = 10
  pageNum = 1
  type: any = []
  dateRanger: any = [null, null]
  time = ''
  typeBtns = [
    { title: '系统公告', value: 1 },
    { title: '提示消息', value: 2 }
    // { title: '公示公告', value: 3 },
  ]
  columns = [
    { title: '公告类型', dataIndex: 'noticeType', width: '88px',align:'left' },
    // {
    //   title: '附件',
    //   dataIndex: 'file',
    //   width: 100,
    //   align:'center',
    //   scopedSlots: { customRender: 'download' }
    // },
    { title: '公告标题', dataIndex: 'title' , width: '240px',  align:'left', ellipsis: true,
    },
    {
      title: '公告内容',
      dataIndex: 'content',
      width: '424px',
      align:'left',
      scopedSlots: { customRender: 'detail' },
    },
    { title: '发送时间',  width: '180px',
      align:'left', dataIndex: 'sendTime' },
    {
      title: '状态',
      dataIndex: 'readStatusText',
      width: '72px',
      align:'left',
      scopedSlots: { customRender: 'readStatus' }
    },
    {
      title: '操作',
      width: '88px',
      align:'left',
      scopedSlots: { customRender: 'operate' }
    }
  ]
  modalTitle = ''
  downloadFiles = []
  detail = ''
  sourceUrl = ''
  tableData = []
  total = 0
  noticeType = -1
  btns = [
    {
      label: '查看详情',
      type: 'link',
      color:'#1890FF',
      click: this.noticeDetailDisplay.bind(this)
    }
  ]
  get pagination() {
    return {
      total: this.total,
      pageSize: this.pageSize,
      current: this.pageNum,
      showTotal: (total: number) => {
        return `共${total}条数据`
      },
      showSizeChanger: false,
      onChange: (pageNum: number) => {
        this.changePageNum(pageNum)
      }
    }
  }
  previewOrDownloadFile(file: any) {
    // console.log(file)
    if (file.type === 'doc' || file.type === 'exc') {
      this.$httpRequester
        .download('upload/file/get', { path: file.url })
        .then((r: any) => {
          const blob = new Blob([r])
          const link = document.createElement('a')
          const firstIndex = file.url.indexOf('/')
          const lastIndex = file.url.lastIndexOf('.')
          const fileName = file.url.substring(firstIndex + 1, lastIndex)
          //  const time = this.$moment().format('YYYY-MM-DD HH:mm:ss')
          link.href = window.URL.createObjectURL(blob)
          link.download = `${fileName}${file.suffix}`
          link.setAttribute('target', 'view_window')
          // document.appendChild(link)
          link.click()
          window.URL.revokeObjectURL(link.href)
        })
    } else {
      window.open(`${apiUrl}upload/file/get?path=${file.url}`)
    }
  }
  showDetail = false
  closeMessgeModal() {
    this.showDetail = false
    this.getMessageList()
  }
  noticeDetailDisplay(item: any) {
    console.log('详情', item)
    this.$httpRequester
      .get('notice/reviewed', { noticeId: item.id })
      .then(() => {
        this.showDetail = true
        this.modalTitle = item.title
        this.detail = item.content
        this.downloadFiles = item.downloadFiles
        this.time = item.sendTime
        this.sourceUrl = item.sourceUrl
        // console.log(this.detail)
        homeModule.getNotReadStatus()
        homeModule.getReadMessageList()
      })
  }
  changePageNum(pageNum: number) {
    this.pageNum = pageNum
    this.getMessageList()
  }
  disabledEndDate(endValue: any) {
    const currentDate = new Date().getTime()
    return endValue.valueOf() > currentDate
  }
  selectDateRange(e: any) {
    console.log(e)
  }
  setStatus(value: any) {
    this.noticeType = value
    this.getMessageList()
    //  this.filterOptions.followStatus = this.status
  }
  readAllMessage() {
    this.$httpRequester.get('notice/reviewed', { noticeId: '' }).then(() => {
      this.getMessageList()
      this.$message.success('全部已读')
      homeModule.getNotReadStatus()
      homeModule.getReadMessageList()
    })
  }
  getMessageList() {
    if (this.noticeType === -1) {
      this.type = ['1', '2']
    } else {
      this.type = []
      this.type.push(JSON.stringify(this.noticeType))
    }
    const params = {
      noticeType: this.type,
      pageNum: this.pageNum,
      pageSize: this.pageSize,
      startTime: this.dateRanger[0]
        ? this.$moment(this.dateRanger[0]).format('yyyy-MM-DD')
        : '',
      endTime: this.dateRanger[1]
        ? this.$moment(this.dateRanger[1]).format('yyyy-MM-DD')
        : ''
    }
    this.$httpRequester.post('notice/selMsgList', params).then((r: any) => {
      const imageTypes = ['.jpg', '.png', '.PNG', '.JPG']
      const docTypes = ['.doc', '.docx', '.pdf']
      const excel = ['.xlsx', '.xls']
      r.data.forEach((e: any) => {
        e.readStatusText = e.readStatus ? '已读' : '未读'
        // e.contentText = e.content
        // if (e.content) {
        //   e.contentText =
        //     e.content.length > 40
        //       ? e.content
        //           .substring(0, 39)
        //           .replace(/<[^>]+>/g, '')
        //           .replace(/[ | ]*\n/g, '\n')
        //           .replace(/&nbsp;/gi, '') + '...'
        //       : e.content
        //           .replace(/<[^>]+>/g, '')
        //           .replace(/[ | ]*\n/g, '\n')
        //           .replace(/&nbsp;/gi, '')
        // } else {
        //   e.contentText = ''
        // }
        // const isShared = e.title.indexOf('发布的共享信息')
        // const index = e.content.indexOf('去查看 ＞＞')
        // e.contentText = e.content
        // console.log(e.content)
        // if (index > 0) {
        //   if (isShared > 0) {
        //     e.contentText =
        //       e.content.substring(0, index) +
        //       '<a id="viewShared">去查看 ＞＞</a>'
        //   } else {
        //     e.contentText =
        //       e.content.substring(0, index) +
        //       '<a id="viewBuying">去查看 ＞＞</a>'
        //   }
        // }
        e.downloadFiles = []
        e.files = e.enclosureUrl ? e.enclosureUrl.split(',') : []
        e.files.forEach((f: any) => {
          const fileNameIndex = f.lastIndexOf('_')
          let fileName = ''
          if (fileNameIndex > 0) {
            fileName = f.substr(0, fileNameIndex)
          } else {
            fileName = f
          }
          const sufffixIndex = fileName.lastIndexOf('.')
          const suffix = fileName.substr(sufffixIndex, fileName.length)
          // console.log(suffix)
          const image = imageTypes.find((image: any) => image === suffix)
          const doc = docTypes.find((doc: any) => doc === suffix)
          const exc = excel.find((ex: any) => ex === suffix)
          if (image) {
            e.downloadFiles.push({
              type: 'image',
              icon: 'file-image',
              suffix,
              title: '图片',
              url: f
            })
          } else if (doc) {
            e.downloadFiles.push({
              type: 'doc',
              icon: 'file-word',
              suffix,
              title: '文档',
              url: f
            })
          } else if (exc) {
            e.downloadFiles.push({
              type: 'exc',
              icon: 'file-excel',
              suffix,
              title: '表格',
              url: f
            })
          }
        })
      })
      this.tableData = r.data
      this.total = r.totalSize
    })
  }
  clickMessageDetail(e: any) {
    const target = e.target
    const id = target.dataset.id
    if (target.id === 'viewShared') {
      this.closeMessgeModal()
      this.$emit('closeDrawer')
      if (this.$route.name !== 'mySellDetail') {
        this.$router.push({
          name: 'mySellDetail',
          query: {
            id
          }
        })
      }
    }

    // if (target.id === 'viewBuying') {
    //   this.closeMessgeModal()
    //   this.$emit('closeDrawer')
    //   if (this.$route.name !== 'buying') {
    //     this.$router.push({
    //       name: 'buying',
    //     })
    //   }
    // } else if (target.id === 'viewShared') {
    //   this.closeMessgeModal()
    //   this.$emit('closeDrawer')
    //   if (this.$route.name !== 'shared') {
    //     this.$router.push({
    //       name: 'shared',
    //     })
    //   }
    // }
  }

  renewFilter() {
    this.noticeType = -1
    this.dateRanger = [null, null]
    this.getMessageList()
  }

  mounted() {
    // this.getStaffList()
    this.getMessageList()
  }
}
