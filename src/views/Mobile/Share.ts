import { Component, Vue } from 'vue-property-decorator'
import zizhiPng from '@/assets/image/zizhi.png'
import notFind from '@/assets/image/no_data_block.png'
import logo from '@/assets/image/logo/share_logo.png'
import qr from '@/assets/image/sidetool/offical_qr_code.jpg'

import MetaInfo from 'vue-meta'
import { parse } from 'qs'

@Component({
  metaInfo(): MetaInfo {
    return {
      title: '企业资质信息',
      meta: [
        {
          name: 'viewport',

          content:
            'width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no'
        }
      ]
    }
  }
})
export default class Sahre extends Vue {
  qr = qr
  enterpriseInfo = null
  enterpriseId = ''
  enteyDate = ''
  notFind = notFind
  logo = logo
  weekTime = 7 * 24 * 60 * 60 * 100
  overTime = false
  zizhiPng = zizhiPng
  uuid = ''
  riskInfo: any = []
  currentTime = ''
  riskDetail = [
    {
      title: '无风险',
      type: 'no-risk risk',
      detail: '企业经营状况良好'
    },
    {
      title: '低风险',
      type: 'no-risk risk',
      detail: '企业存在工商异常、行政处罚等信息'
    },
    {
      title: '中风险',
      type: 'medium-risk risk',
      detail: '企业存在被执行信息'
    },
    {
      title: '高风险',
      type: 'hight-risk risk',
      detail: '企业存在失信被执行信息'
    },
    {}
  ]
  getEnterpriseInfo() {
    this.$httpRequester
      .get('enterpriseClue/share', {
        // enterpriseId: this.enterpriseId,
        uuid: this.uuid
      })
      .then((r: any) => {
        this.enterpriseInfo = r
        // console.log('分享',r.riskInfoVO)
        this.riskInfo = [
          { title: '失信被执行', number: r.riskInfoVO.dishonesty },
          { title: '被执行人信息', number: r.riskInfoVO.executed },
          { title: '行政处罚', number: r.riskInfoVO.administrative },
          { title: '工商异常', number: r.riskInfoVO.business }
          // { title: '裁判文书', number: r.riskInfoVO.judgment },
        ]
      })
  }

  mounted() {
    this.enterpriseId = this.$route.query.enterpriseId as string
    this.enteyDate = this.$route.query.dateTime as string
    this.uuid = this.$route.query.uuid as string
    const currentTime = new Date().getTime()
    this.currentTime = this.$moment(currentTime).format('yyyy-MM-DD')
    const differTime = currentTime - parseInt(this.enteyDate, 10)
    this.$nextTick(() => {
      this.getEnterpriseInfo()
      if (differTime > this.weekTime) {
        this.overTime = true
      }
    })
  }
}
