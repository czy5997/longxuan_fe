import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { HttpRequester } from './httpRequester/http'
import { FormValidate } from './utils/formValidate'
import { SetQuery } from './utils/setRouterQuery'
import { message, Modal, Button, Icon } from 'ant-design-vue'
import 'moment/locale/zh-cn'
import 'devtools-detect/index'
import moment from 'moment'
import Meta from 'vue-meta'
import { Component } from 'vue-property-decorator'
import './beforeLogin'
import './assets/font/font.css'
import 'tailwindcss/tailwind.css'
const echarts = require('echarts')
Vue.prototype.$echarts = echarts
Component.registerHooks([
  'beforeRouteEnter',
  'beforeRouteLeave',
  'beforeRouteUpdate'
])
moment.locale('zh-cn')
Vue.config.productionTip = false
Vue.prototype.$httpRequester = new HttpRequester()
message.config({
  duration: 2,
  maxCount: 1,
  top: `50%`
})
let timer = null as any
Vue.prototype.$debounce = (fn: any, params: any, time: any) => {
  if (timer) {
    clearTimeout(timer)
  }
  timer = setTimeout(() => {
    fn(params)
    timer = null as any
  }, time)
}
Vue.prototype.$message = message
Vue.prototype.$setQuery = new SetQuery()
Vue.prototype.$moment = moment
Vue.prototype.$deepCopy = function(obj: any) {
  const newobj = obj.constructor === Array ? [] : ({} as any)
  if (typeof obj !== 'object') {
    return
  }
  for (const i in obj) {
    newobj[i] = typeof obj[i] === 'object' ? this.$deepCopy(obj[i]) : obj[i]
  }
  return newobj
}

Vue.prototype.$emitP = function(topic: any, varargs: any) {
  return new Promise((resolve, reject) => {
    const arr = Array.from(arguments)
    let invoked = false
    const promiseLike = {
      resolve: (val: any) => {
        if (!invoked) {
          invoked = true
          resolve(val)
        }
      },
      reject: (val: any) => {
        if (!invoked) {
          invoked = true
          reject(val)
        }
      }
    }
    arr.push(promiseLike)
    this.$emit.apply(this, arr)
  })
}

Vue.component(Button.name, Button)
Vue.component(Icon.name, Icon)
Vue.prototype.$apiUrl = apiUrl
Vue.prototype.$formValidate = new FormValidate()
Vue.use(Modal)
Vue.use(Meta)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
