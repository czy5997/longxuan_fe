import { HttpRequester } from '@/httpRequester/http'
import { FormValidate } from '@/utils/formValidate'

import { message } from 'ant-design-vue'
import moment from 'moment'
import { SetQuery } from '@/utils/setRouterQuery'
declare module 'vue/types/vue' {
  interface Vue {
    $httpRequester: HttpRequester
    $moment: typeof moment
    $apiUrl: string
    $formValidate: FormValidate
    $setQuery: SetQuery
    $deepCopy: any
    $emitP: any
    $debounce: any
    //  $message: Message
  }
}
