import Vue from 'vue'
import Vuex from 'vuex'
import UserModule from './modules/user'
import HomeModule from './modules/home'
import DetailModule from './modules/home'
import UniversalModule from './modules/universal'
import TalentsModule from './modules/talents'
import ShareholderModule from './modules/shareholder'

Vue.use(Vuex)

export interface LongnaState {
  user: UserModule
  home: HomeModule
  detail: DetailModule
  universal: UniversalModule
  talents: TalentsModule
  shareholder: ShareholderModule
}

export default new Vuex.Store<LongnaState>({})
