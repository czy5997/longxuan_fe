import store from '../index'
import { HttpRequester } from '../../httpRequester/http'
import { designList } from '@/components/designQualification.js'
import { areaJSON } from '@/utils/area.js'

import {
  VuexModule,
  Module,
  Mutation,
  Action,
  getModule
} from 'vuex-module-decorators'

const http = new HttpRequester()
@Module({ dynamic: true, store, name: 'universalModule' })
export default class UniversalModule extends VuexModule {
  //   enterpriseItem = {} as any
  qualificationOptions = [] as any
  areaOptions = [] as any
  areaCityOptions = [] as any
  typeQualificationOptions = [] as any
  designQualificationOptions = [] as any
  fakeQualificationOptions = [] as any
  historyList = [] as any
  //   @Mutation
  //   setEnterpriseInfo(item: any) {
  //     this.enterpriseItem = item
  //   }
  //   @Mutation
  //   changeEnterpriseKey(params: any) {
  //     const { prop, key } = params
  //     this.enterpriseItem[prop] = key
  //     this.enterpriseItem = Object.assign({}, this.enterpriseItem)
  //   }
  @Mutation
  clearHistoryList() {
    this.historyList = []
  }
  @Mutation
  changeHistoryListItem(item: any) {
    const userId = localStorage.getItem('userId') as any
    const tabHistory = localStorage.getItem('history') as any
    localStorage.setItem('lastUserId', userId)
    // console.log(tabHistory)
    if (tabHistory) {
      this.historyList = JSON.parse(tabHistory)
    }
    const find = this.historyList.findIndex((e: any) => {
      return e.name === item.name
    })

    // if (item.path === '/homepage') {
    //   return
    // }
    if (find > -1) {
      this.historyList[find] = item
    } else {
      this.historyList.push(item)
    }
    localStorage.setItem('history', JSON.stringify(this.historyList))
  }
  @Mutation
  removeThisTab(index: any) {
    const tabHistory = localStorage.getItem('history')
    if (tabHistory) {
      this.historyList = JSON.parse(tabHistory)
    } //  刷新重新获取当前history
    this.historyList.splice(index, 1)
    localStorage.setItem('history', JSON.stringify(this.historyList))
  }
  @Mutation
  setQualification(qua: any) {
    this.qualificationOptions = []
    qua.forEach((e: any, i: number) => {
      //  this.defaultExpend.push(e.qualificationName + i)
      this.qualificationOptions.push({
        title: e.qualificationName,
        label: e.qualificationName,
        value: e.qualificationName + i,
        key: e.qualificationName + i,
        selectable: false,
        checkable: false,
        children: e.qualificationLevels.map((q: any) => {
          return {
            selectable: true,
            checkable: true,
            disabled: false,
            label: e.qualificationName + q.level,
            title: q.level,
            value: q.id,
            key: q.id
          }
        })
      })
    })
  }
  @Mutation
  setAreaOptions(area: any) {
    const gangAoTai = [810000, 820000, 710000]
    this.areaOptions = []
    this.areaCityOptions = []
    // 去除港澳台
    const areaOption = areaJSON
    const needSpecial = true
    areaOption.forEach((option: any) => {
      if (needSpecial) {
        if (gangAoTai.includes(option.areaCode)) {
          this.areaOptions.push({
            label: option.areaName,
            value: option.areaCode,
            disabled: true
          })
          this.areaCityOptions.push({
            label: option.areaName,
            value: option.areaCode,
            cityList: option.cityList,
            disabled: true
          })
        } else {
          this.areaOptions.push({
            label: option.areaName,
            value: option.areaCode,
            disabled: false
          })
          this.areaCityOptions.push({
            label: option.areaName,
            value: option.areaCode,
            cityList: option.cityList,
            disabled: false
          })
        }
      } else {
        this.areaOptions.push({
          label: option.areaName,
          value: option.areaCode,
          disabled: false
        })
        this.areaCityOptions.push({
          label: option.areaName,
          value: option.areaCode,
          cityList: option.cityList,
          disabled: false
        })
      }
    })
  }

  @Mutation
  setTypeQualification(r: any) {
    this.typeQualificationOptions = r.map((e: any, i: any) => {
      return {
        title: e.qualificationType,
        searchTitle: e.qualificationType,
        key: i,
        isExpend: i === 0,
        children: e.qualificationList.map((q: any) => {
          return {
            title: q.qualificationName,
            searchTitle: q.qualificationName,
            key: q.qualificationName,
            // checkable: true,
            children: q.qualificationLevels.map((level: any) => {
              return {
                id: level.id,
                searchTitle: level.level,
                title: level.level,
                disabled: false
              }
            }),
            value: []
          }
        })
      }
    })
  }

  @Mutation
  setDesignQualification(r: any) {
    this.designQualificationOptions = r.map((e: any, i: any) => {
      if (e.qualificationType === '专业资质') {
        let haveGroup = [] as any

        designList.forEach((d: any) => {
          const find = e.qualificationList.filter((q: any) => {
            return q.qualificationName.indexOf(d) > -1
          })
          find.forEach((f: any) => {
            f.qualificationNameShow = f.qualificationName.replace(d, '')
          })
          find.unshift({
            qualificationName: d,
            onlyShow: true
          })
          haveGroup = haveGroup.concat(find)
        })
        return {
          title: e.qualificationType,
          searchTitle: e.qualificationType,
          key: i,
          isExpend: i === 0,
          isSpecial: true,
          children: haveGroup.map((q: any) => {
            if (q.onlyShow) {
              return {
                title: q.qualificationName,
                key: q.qualificationName,
                searchTitle: q.qualificationName,
                onlyShow: true,
                children: [],
                value: []
              }
            } else {
              return {
                title: q.qualificationNameShow,
                key: q.qualificationName,
                searchTitle: q.qualificationName,
                // checkable: true,
                children: q.qualificationLevels.map((level: any) => {
                  return {
                    id: level.id,
                    title: level.level,
                    searchTitle: level.level,
                    disabled: false
                  }
                }),
                value: []
              }
            }
          })
        }
      } else {
        return {
          title: e.qualificationType,
          searchTitle: e.qualificationType,
          key: i,
          isExpend: i === 0,
          children: e.qualificationList.map((q: any) => {
            return {
              title: q.qualificationName,
              key: q.qualificationName,
              searchTitle: q.qualificationName,
              // checkable: true,
              children: q.qualificationLevels.map((level: any) => {
                return {
                  id: level.id,
                  title: level.level,
                  searchTitle: level.level,
                  disabled: false
                }
              }),
              value: []
            }
          })
        }
      }
    })
  }

  @Mutation
  setFakeQualification(r: any) {
    this.fakeQualificationOptions = []
    r.forEach((e: any) => {
      e.qualificationLevels.forEach((q: any) => {
        this.fakeQualificationOptions.push({
          label: e.qualificationName + q.level,
          value: q.id
        })
      })
    })
  }

  @Action
  getQualification() {
    http.get('qualification/selQualificationAll').then((r: any) => {
      this.setFakeQualification(r)
      this.setQualification(r)
    })
  }
  // @Action
  // getAreaOptions(needSpecial: any) {
  //   // console.log('areaJSON', areaJSON)
  //   Promise.all([]).then(() => {
  //     this.setAreaOptions({ needSpecial, areaJSON })
  //   })
  //   // http.get('regionCode/selList').then((r: any) => {
  //   // })
  // }
  @Action
  getTypeQualification() {
    http.get('qualification/selQualificationAllByIndustry').then((r: any) => {
      r.forEach((e: any) => {
        if (e.qualificationIndustry.indexOf('设计') > -1) {
          this.setDesignQualification(e.qualificationTypeList)
        } else {
          this.setTypeQualification(e.qualificationTypeList)
        }
      })
    })
  }

  @Action
  getDesignAndOtherQualification() {
    http.get('qualification/selQualificationAllByIndustry').then((r: any) => {
      this.setTypeQualification(r[1])
    })
  }
}
export const universalModule = getModule(UniversalModule)
