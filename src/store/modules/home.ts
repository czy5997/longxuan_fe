import store from '../index'
import {
  VuexModule,
  Module,
  Mutation,
  getModule,
  Action
} from 'vuex-module-decorators'
import { HttpRequester } from '@/httpRequester/http'

const httpRequester = new HttpRequester()
@Module({ dynamic: true, store, name: 'homeModule' })
export default class HomeModule extends VuexModule {
  staffList = []
  areaList = []
  messageList = []
  messageTotal = 0
  readList = []
  pageHandleFunction = null as any
  turnOnPhone = false
  defaultQua = null as any
  turnOnItem = null as any
  notTurnOnPhone = false
  rowQuaStatus = {
    onlyDesign: false,
    onlyConstruction: true
  }
  freeTime = 0 // 剩余免费次数

  // 修改免费次数
  @Mutation
  setFreeTime(status: any) {
    this.freeTime = status
  }

  @Mutation
  setRowQuaStatus(status: any) {
    this.rowQuaStatus = status
  }

  @Mutation
  setPageHandleFunction(fun: any) {
    this.pageHandleFunction = fun
  }
  @Mutation
  setTurnOnItem(item: any) {
    this.turnOnItem = item
  }
  @Mutation
  setATurnOnDefaultQua(item: any) {
    this.defaultQua = item
  }
  @Mutation
  changeTurnPhoneStatus(status: boolean) {
    this.turnOnPhone = status
  }
  @Mutation
  changeNotTurnPhoneStatus(status: boolean) {
    this.notTurnOnPhone = status
  }
  @Mutation
  setStaffList(list: any) {
    this.staffList = list.map((l: any) => {
      return {
        label: l.memberName,
        value: l.id
      }
    })
  }
  @Mutation
  setAreaList(list: any) {
    this.areaList = list
  }
  @Mutation
  setMessageList(res: any) {
    this.messageList = res.data.slice(0, 3)
    this.messageTotal = res.totalSize
  }
  @Mutation
  setReadList(res: any) {
    this.readList = res.data.slice(0, 3)
  }

  // 获取免费次数
  @Action
  getFreeTime () {
    httpRequester.get('enterpriseClue/query/free/count').then((r: any) => {
      this.setFreeTime(r)
    })
  }

  @Action
  getNotReadStatus() {
    const params = {
      pageNum: 1,
      pageSize: 10,
      readStatus: false,
      noticeType: ['1', '2']
    }
    httpRequester.post('notice/selMsgList', params).then((r: any) => {
      this.setMessageList(r)
      this.getReadMessageList()
    })
  }
  @Action
  getReadMessageList() {
    const params = {
      pageNum: 1,
      pageSize: 10,
      readStatus: true,
      noticeType: ['1', '2']
    }
    httpRequester.post('notice/selMsgList', params).then((r: any) => {
      this.setReadList(r)
    })
  }
}
export const homeModule = getModule(HomeModule)
