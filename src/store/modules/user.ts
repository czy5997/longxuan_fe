import store from '../index'
import moment from 'moment'
import { VuexModule, Module, Mutation, getModule } from 'vuex-module-decorators'
// import accountN from '@/assets/image/menuIcon/siderBar/account_normal.png'
// import accountS from '@/assets/image/menuIcon/siderBar/account_selected.png'
// import customerN from '@/assets/image/menuIcon/siderBar/customer_normal.png'
// import customerS from '@/assets/image/menuIcon/siderBar/customer_selected.png'
// import knowledgeN from '@/assets/image/menuIcon/siderBar/knowledgebase_normal.png'
// import knowledgeS from '@/assets/image/menuIcon/siderBar/knowledgebase_selected.png'
// import receiveN from '@/assets/image/menuIcon/siderBar/receiving_normal.png'
// import receiveS from '@/assets/image/menuIcon/siderBar/receiving_selected.png'
// import sellN from '@/assets/image/menuIcon/siderBar/sell_normal.png'
// import sellS from '@/assets/image/menuIcon/siderBar/sell_selected.png'
// import shopN from '@/assets/image/menuIcon/siderBar/shop_normal.png'
// import shopS from '@/assets/image/menuIcon/siderBar/shop_selected.png'

import receiving from '@/components/icons/receiving.vue'
import sell from '@/components/icons/sell.vue'
import accountN from '@/components/icons/account.vue'
import customer from '@/components/icons/customer.vue'
import knowledge from '@/components/icons/knowledge.vue'
// import shop from '@/components/icons/shop.vue'
import bankrupt from '@/components/icons/bankrupt.vue'

import gongce from '@/assets/image/menuIcon/gongce.png'
import newIcon from '@/assets/image/menuIcon/NEW.png'

@Module({ dynamic: true, store, name: 'UserModule' })
export default class UserModule extends VuexModule {
  userId = NaN
  account = ''
  name = ''
  companyId = NaN
  roleId = NaN
  roleName = ''
  leadId = NaN
  isLead = false
  companyInfo = null
  companyName = ''
  leadInfo = null
  loginIp = ''
  useAreaName = ''
  fatherMenuList: any = []
  settingMenu: any = []
  childrenMenuList: any = []
  buyVersion: any = ''
  version: any = ''
  buyUserCount: any = ''
  failureTime: any = ''
  noLoginFatherMenuList = [
    {
      title: '收货查询',
      path: '/longna/receipt',
      name: 'receipt',
      isSelected: false,
      icon: receiving,
      // noClickIcon: receiveN,
      children: []
    },
    {
      title: '出货查询',
      path: '/longna/shipment',
      name: 'shipment',
      isSelected: false,
      icon: sell,
      // noClickIcon: sellN,
      slotIcon: gongce,
      children: []
    },
    {
      title: '我的客户',
      path: '/longna/client/processing',
      name: 'client',
      isSelected: false,
      icon: customer,
      // noClickIcon: customerN,
      children: []
    },
    {
      title: '知识库',
      path: '/longna/knowledgeWarehouse/knowledgeWarehouseHomePage',
      name: 'knowledgeWarehouseHomePage',
      isSelected: false,
      icon: knowledge,
      // noClickIcon: knowledgeN,
      children: []
    }
  ]
  allMenus = [
    {
      title: '收货查询',
      path: '/longna/receipt',
      name: 'receipt',
      isSelected: false,
      icon: receiving
      // noClickIcon: receiveN
    },
    {
      title: '出货查询',
      path: '/longna/shipment',
      name: 'shipment',
      isSelected: false,
      icon: sell
      // noClickIcon: sellN,
      // slotIcon: gongce
    },
    {
      title: '我的客户',
      path: '/longna/client/processing',
      name: 'client',
      isSelected: false,
      icon: customer
      // noClickIcon: customerN
    },
    {
      title: '跟进中客户',
      path: '/MyClient',
      name: 'processing',
      isSelect: false,
      isShow: true
    },
    {
      title: '历史记录',
      path: '/MyHistory',
      name: 'clientHistory',
      isSelect: false,
      isShow: true
    },
    {
      title: '资质服务商',
      path: '/longna/IntermediaryResources',
      name: 'IR',
      isSelected: false
    },
    {
      title: '公示公告',
      path: '/longna/Notice',
      name: 'Notice',
      isSelected: false
    },
    {
      title: '破产企业',
      path: '/longna/bankrupt',
      name: 'bankrupt',
      isSelected: false,
      icon: bankrupt,
      slotIcon: newIcon
    },
    {
      title: '知识库',
      path: '/longna/knowledgeWarehouse/knowledgeWarehouseHomePage',
      name: 'knowledgeWarehouseHomePage',
      isSelected: false,
      icon: knowledge
      // noClickIcon: knowledgeN
    },
    {
      title: '设置',
      path: '/longna/setting/user',
      name: 'setting',
      hidden: true,
      isSelected: false
    },
    { title: '用户管理', path: '/user', name: 'userSetting', isSelect: false },
    {
      title: '部门管理',
      path: '/department',
      name: 'department',
      isSelect: false
    },
    {
      title: '组织管理',
      path: '/organization',
      name: 'organizationSetting',
      isSelect: false
    },
    {
      title: '操作日志',
      path: '/operalog',
      name: 'operalogSetting',
      isSelect: false
    },
    {
      title: '企业股权报价',
      path: '/quote',
      name: 'quoteSetting',
      isSelect: false
    },
    {
      title: '企业关注统计',
      path: '/enterprise',
      name: 'EnterpriseFocusStatistics',
      isSelect: false
    }
  ]
  wechatName: any = ''
  subMenus = undefined
  isBindWechat: any = 'fa'
  roleLevel = -1
  qualificationTreeList: any = []
  fakeQualificationList: any = []
  qualificationTypeList: any = []

  @Mutation
  setUserInfo(info: any) {
    this.isLead = info.isLead
    this.userId = info.id
    this.account = info.userAccount
    this.name = info.memberName
    this.companyId = info.companyId
    this.companyName = info.companyName
    this.roleId = info.roleId
    this.leadId = info.upLeadId
    this.loginIp = info.loginIp
    this.companyInfo = info.companyVO
    this.leadInfo = info.upLeadVO
    this.roleLevel = info.roleLevel
    this.fatherMenuList = []
    this.childrenMenuList = []
    this.version = info.buyVersion
    switch (info.buyVersion) {
      case 'V_FREE_TEMP':
        this.buyVersion = '免费体验版'
        break
      case 'V_PERSONAL_VIP':
        this.buyVersion = 'VIP个人版'
        break
      case 'V_ACTIVITY_VIP':
        this.buyVersion = '活动版'
        break
      case 'V_ENTERPRISE_VIP':
        this.buyVersion = 'VIP团队版'
        break
      case 'V_ENTERPRISE_PLUS_VIP':
        this.buyVersion = 'VIP企业版'
        break
      case 'V_TEAM_VIP':
        this.buyVersion = 'VIP集团版'
        break
      default:
        this.buyVersion = ''
        break
    }
    this.buyUserCount = info.buyUserCount + '人'
    this.failureTime = info.failureTime
      ? moment(info.failureTime).format('yyyy年MM月DD日')
      : ''
    this.wechatName = info.wechatName || '-'
    this.isBindWechat = info.wechatName ? '已绑定' : '未绑定'
    const roleNames = ['1', '超级管理员', '公司管理员', '销售经理', '销售人员']
    this.roleName = roleNames[this.roleLevel]
    this.useAreaName = info.useAreaName || ''
    // 找到权限菜单
    this.allMenus.forEach((a: any) => {
      const find = info.menuVOList.find((l: any) => l.menuName === a.title)
      if (find && find.menuParentId === 0) {
        // console.log(a)
        this.fatherMenuList.push({
          ...a,
          id: find.id,
          parentId: find.menuParentId,
          children: []
        })
      } else if (find && find.menuParentId !== 0) {
        this.childrenMenuList.push({
          ...a,
          id: find.id,
          parentId: find.menuParentId
        })
      }
    })
    const userAgent = navigator.userAgent.toLowerCase()
    const isShowChangePwd = userAgent.indexOf(' electron/') > -1
    // 配置权限菜单
    this.fatherMenuList.forEach((e: any) => {
      e.children = []
      const isThisChildren = this.childrenMenuList.filter(
        (c: any) => parseInt(c.parentId, 10) === parseInt(e.id, 10)
      )
      if (isThisChildren.length > 0) {
        // e.name = isThisChildren[0].name
        e.children.push(...isThisChildren)
      }
      if (e.title === '设置') {
        e.children = []
        e.title = '个人中心'
        e.icon = accountN
        // e.noClickIcon = accountN
        const itemsMenu = [
          {
            title: '基本信息',
            icon: 'contacts',
            name: 'UCBasicInfo',
            pathName: 'userCenter/UCBasicInfo',
            key: 0,
            isShow: true
          },
          {
            title: '会员订单',
            icon: 'contacts',
            name: 'memberOrder',
            pathName: 'userCenter/memberOrder',
            key: 1,
            isShow: true
          },
          {
            title: '消息中心',
            icon: 'message',
            name: 'messageCenter',
            pathName: '/longna/userCenter/messageCenter',
            key: 3,
            isShow: true
          },
          {
            title: '设置',
            icon: 'userSetting',
            name: 'userSetting',
            pathName: '/longna/setting/user',
            key: 4,
            isShow: true
          },
          {
            title: '关于小牛建讯',
            icon: 'info-circle',
            name: 'softInfo',
            pathName: 'userCenter/softInfo',
            key: 5,
            isShow: true
          },
          {
            title: '系统配置',
            icon: 'tool',
            name: 'downloadSetting',
            pathName: 'userCenter/downloadSetting',
            key: 6,
            isShow: true
          },
          {
            title: '修改密码',
            icon: 'setting',
            name: 'changePassword',
            pathName: 'userCenter/changePassword',
            key: 7,
            isShow: isShowChangePwd
          }
        ] as any
        if (this.roleLevel !== 1) {
          itemsMenu.splice(5, 1)
        }
        e.children = itemsMenu
      }
      if (e.title === '个人中心') {
        // if(d.title==='设置'){
        this.settingMenu = this.childrenMenuList.filter(
          (c: any) => parseInt(c.parentId, 10) === parseInt(e.id, 10)
        )
      }
    })
    if (
      this.fatherMenuList[this.fatherMenuList.length - 1].title !== '个人中心'
    ) {
      const itemsMenu = {
        name: 'setting',
        title: '个人中心',
        icon: accountN,
        children: [
          {
            title: '基本信息',
            icon: 'contacts',
            name: 'UCBasicInfo',
            pathName: 'userCenter/UCBasicInfo',
            key: 0,
            isShow: true
          },
          {
            title: '消息中心',
            icon: 'message',
            name: 'messageCenter',
            pathName: '/longna/userCenter/messageCenter',
            key: 3,
            isShow: true
          },
          {
            title: '关于小牛建讯',
            icon: 'info-circle',
            name: 'softInfo',
            pathName: 'userCenter/softInfo',
            key: 5,
            isShow: true
          },
          {
            title: '修改密码',
            icon: 'setting',
            name: 'changePassword',
            pathName: 'userCenter/changePassword',
            key: 7,
            isShow: isShowChangePwd
            //fasle 的话不是安装包 是安装包显示修改密码 不是安装包不显示修改密码
          }
        ] as any
      }

      this.fatherMenuList.push(itemsMenu)
    }
    // this.fatherMenuList.push({
    //   title: '破产企业',
    //   path: '/longna/bankrupt',
    //   name: 'bankrupt',
    //   isSelected: false,
    //   children: []
    // })
  }

  @Mutation
  setSubMenus(items: any) {
    this.subMenus = items
  }

  @Mutation
  setQualificationOptions(val: any) {
    this.qualificationTreeList = val
  }

  @Mutation
  setFakeQualificationList(val: any) {
    this.fakeQualificationList = val
  }

  @Mutation
  setQualificationTypeList(val: any) {
    this.qualificationTypeList = val
  }
}
export const userModule = getModule(UserModule)
