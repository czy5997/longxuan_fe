import store from '../index'
import {
  VuexModule,
  Module,
  Mutation,
  getModule,
  Action
} from 'vuex-module-decorators'
import { HttpRequester } from '@/httpRequester/http'

const httpRequester = new HttpRequester()
@Module({ dynamic: true, store, name: 'talentsModule' })
export default class TalentsModule extends VuexModule {
  historyList = [] as any

  @Mutation
  clearTalentHistoryList() {
    this.historyList = []
  }
  @Mutation
  changeTalentHistoryListItem(item: any) {
    console.log('isTalent')
    const userId = localStorage.getItem('userId') as any
    const tabHistory = localStorage.getItem('talentHistory') as any
    localStorage.setItem('lastUserId', userId)
    if (tabHistory) {
      this.historyList = JSON.parse(tabHistory)
    }
    const find = this.historyList.findIndex((e: any) => {
      return e.name === item.name
    })

    if (find > -1) {
      this.historyList[find] = item
    } else {
      this.historyList.push(item)
    }
    localStorage.setItem('talentHistory', JSON.stringify(this.historyList))
  }
  @Mutation
  removeTalentTab(index: any) {
    const tabHistory = localStorage.getItem('talentHistory')
    if (tabHistory) {
      this.historyList = JSON.parse(tabHistory)
    } //  刷新重新获取当前history
    this.historyList.splice(index, 1)
    localStorage.setItem('talentHistory', JSON.stringify(this.historyList))
  }
}
export const talentsModule = getModule(TalentsModule)
