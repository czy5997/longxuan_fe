import store from '../index'
import { VuexModule, Module, Mutation, getModule } from 'vuex-module-decorators'

@Module({ dynamic: true, store, name: 'detailModule' })
export default class DetailModule extends VuexModule {
  enterpriseItem = {} as any
  buyingItem = {} as any
  expriedModalShow = false
  goToVipModalShow = false
  goToVipTeamModalShow = false
  isListQuery = false
  verification = false
  // goToVipTeamModalShow = false
  goToActivityModalShow = false
  // isListQuery = false
  // verification = false
  watchPhoneModalShow = false
  companyNameToFilter = '' as any
  freeOverDateModalShow = false
  isShowLogin = false
  isLoginSuccess = false
  userLoginModal = false
  activityContent = {} // 活动返回信息
  activityModalSide = false // 侧边栏活动弹窗
  lookFollowListCodeData = {} as any

  @Mutation
  setLookFollowListCodeData(status: any) {
    this.lookFollowListCodeData = status
  }
  @Mutation
  changeUserLoginModal(status: any) {
    this.userLoginModal = status
  }
  @Mutation
  setLoginSuccess(item: boolean) {
    this.isLoginSuccess = item
  }
  setShowLoginModal(item: boolean) {
    this.isShowLogin = item
  }

  setFreeOverModalShow(item: boolean) {
    this.freeOverDateModalShow = item
  }

  @Mutation
  setWatchPhoneModalShow(item: boolean) {
    this.watchPhoneModalShow = item
  }
  @Mutation
  setExpriedModalShow(item: boolean) {
    this.expriedModalShow = item
  }
  @Mutation
  setGoToVipModalShow(item: boolean) {
    this.goToVipModalShow = item
  }
  @Mutation
  setGoToActivityModalShow(item: boolean) {
    this.goToActivityModalShow = item
  }
  @Mutation
  setGoToVipTeamModalShow(item: boolean) {
    this.goToVipTeamModalShow = item
  }
  @Mutation
  setListQuery(item: boolean) {
    this.isListQuery = item
  }

  @Mutation
  setVerification(item: boolean) {
    this.verification = item
  }

  @Mutation
  setBuyingItem(item: any) {
    this.buyingItem = item
  }
  @Mutation
  setEnterpriseInfo(item: any) {
    this.enterpriseItem = item
  }
  @Mutation
  changeEnterpriseKey(params: any) {
    const { prop, key } = params
    this.enterpriseItem[prop] = key
    this.enterpriseItem = Object.assign({}, this.enterpriseItem)
  }
  //资质服务商公司名收货页面关键词查询
  @Mutation
  setCompanyNameToFilter(item: any) {
    this.companyNameToFilter = item
  }
  //活动信息
  @Mutation
  setActivityContent(item: any) {
    console.log(item, 'itemitemitemitem')
    this.activityContent = item
  }
  // 侧边栏活动弹窗
  @Mutation
  setActivityModalSide(item: boolean) {
    this.activityModalSide = item
  }
}
export const detailModule = getModule(DetailModule)
