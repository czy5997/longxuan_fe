import store from '../index'
import {
  VuexModule,
  Module,
  Mutation,
  getModule,
  Action
} from 'vuex-module-decorators'
import { HttpRequester } from '@/httpRequester/http'

const httpRequester = new HttpRequester()
@Module({ dynamic: true, store, name: 'shareholderModule' })
export default class ShareholderModule extends VuexModule {
  historyList = [] as any

  @Mutation
  clearShareholderHistoryList() {
    this.historyList = []
  }
  @Mutation
  changeShareholderHistoryListItem(item: any) {
    console.log('isShareholder')
    const userId = localStorage.getItem('userId') as any
    const tabHistory = localStorage.getItem('shareholderHistory') as any
    localStorage.setItem('lastUserId', userId)
    if (tabHistory) {
      this.historyList = JSON.parse(tabHistory)
    }
    const find = this.historyList.findIndex((e: any) => {
      return e.name === item.name
    })

    if (find > -1) {
      this.historyList[find] = item
    } else {
      this.historyList.push(item)
    }
    localStorage.setItem('shareholderHistory', JSON.stringify(this.historyList))
  }
  @Mutation
  removeShareholderTab(index: any) {
    const tabHistory = localStorage.getItem('shareholderHistory')
    if (tabHistory) {
      this.historyList = JSON.parse(tabHistory)
    } //  刷新重新获取当前history
    this.historyList.splice(index, 1)
    localStorage.setItem('shareholderHistory', JSON.stringify(this.historyList))
  }
}
export const shareholderModule = getModule(ShareholderModule)
