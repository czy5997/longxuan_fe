'use strict'
/* global __static */

import path from 'path'
import {
  app,
  protocol,
  BrowserWindow,
  dialog,
  Menu,
  Tray,
  ipcMain,
  session,
  shell
} from 'electron'
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib'
import { autoUpdater } from 'electron-updater'
import installExtension, { VUEJS_DEVTOOLS } from 'electron-devtools-installer'
import { unwatchFile } from 'fs'
const os = require('os')
const isWin7 = os.release().startsWith('6.1')

// win7 下关闭硬件加速
const isDevelopment = process.env.NODE_ENV !== 'production'

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win: any
let isUpdate = false
// console.log(path.join(__static, '/icon.ico'))
// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([
  { scheme: 'app', privileges: { secure: true, standard: true } }
])
let isCheckByUser = false
function updateHandle() {
  //  console.log('??')
  let message = {
    error: { status: -1, msg: '检测更新查询异常' },
    checking: { status: 0, msg: '正在检查更新...' },
    updateAva: { status: 1, msg: '检测到新版本,正在下载,请稍后' },
    updateNotAva: { status: 2, msg: '您现在使用的版本为最新版本,无需更新!' }
  }
  let versionInfo = ''
  autoUpdater.autoDownload = true // 默认
  autoUpdater.setFeedURL('http://cdn.public.xnjx666.com/download/')
  // 检测更新查询异常
  autoUpdater.on('error', function(error) {
    if (isCheckByUser) {
      sendUserUpdateMessage(message.error)
    } else {
      sendUpdateMessage(message.error)
    }
  })
  // 当开始检查更新的时候触发
  autoUpdater.on('checking-for-update', function() {
    if (isCheckByUser) {
      sendUserUpdateMessage(message.error)
    } else {
      sendUpdateMessage(message.checking)
    }
  })
  // 当发现有可用更新的时候触发，更新包下载会自动开始
  autoUpdater.on('update-available', function(info) {
    isUpdate = true
    if (isCheckByUser) {
      sendUserUpdateMessage(message.updateAva)
    } else {
      sendUpdateMessage(message.updateAva)
    }
  })
  // 当发现版本为最新版本触发
  autoUpdater.on('update-not-available', function(info) {
    isUpdate = false
    if (isCheckByUser) {
      sendUserUpdateMessage(message.updateNotAva)
    } else {
      sendUpdateMessage(message.updateNotAva)
    }
  })
  // 更新下载进度事件
  autoUpdater.on('download-progress', function(progressObj) {
    win.webContents.send('downloadProgress', progressObj)
  })
  // 包下载成功时触发
  autoUpdater.on('update-downloaded', function(
    event,
    releaseNotes,
    releaseName,
    releaseDate,
    updateUrl,
    quitAndUpdate
  ) {
    // 收到renderer进程确认更新
    ipcMain.on('updateNow', (e: any, arg: any) => {
      console.log('开始更新')
      // quitAndUpdate()

      autoUpdater.quitAndInstall() // 包下载完成后，重启当前的应用并且安装更新
    })
    // 主进程向renderer进程发送是否确认更新
    win.webContents.send('isUpdateNow', versionInfo)
  })

  ipcMain.on('checkForUpdate', () => {
    // 收到renderer进程的检查通知后，执行自动更新检查
    // autoUpdater.checkForUpdates()
    isCheckByUser = false
    let checkInfo = autoUpdater.checkForUpdates()
    checkInfo.then(function(data) {
      // console.log(data)
      versionInfo = data.updateInfo.version // 获取更新包版本等信息
    })
  })
  // 通过main进程发送事件给renderer进程，提示更新信息
  function sendUpdateMessage(text: any) {
    win.webContents.send('message', text)
  }
  function sendUserUpdateMessage(text: any) {
    win.webContents.send('userCheck', text)
  }
}
// 手动检查

ipcMain.on('checkForUpdateByUser', () => {
  // 收到renderer进程的检查通知后，执行自动更新检查
  // autoUpdater.checkForUpdates()
  autoUpdater.autoDownload = false
  isCheckByUser = true
  // let checkInfo = autoUpdater.checkForUpdates()
  autoUpdater.checkForUpdates()
  // checkInfo.then(function(data) {
  //   console.log(data)
  //   // versionInfo = data.updateInfo.version // 获取更新包版本等信息
  // })
})

function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    // width: 1360,
    // height: 975,
    show: false,
    // eslint-disable-next-line no-undef
    webPreferences: {
      webSecurity: true,
      enableRemoteModule: true,
      preload: path.join(__static, 'preload.js'),
      // Use pluginOptions.nodeIntegration, leave this alone
      // See nklayman.github.io/vue-cli-plugin-electron-builder/guide/security.html#node-integration for more info
      // nodeIntegration: (process.env
      //   .ELECTRON_NODE_INTEGRATION as unknown) as boolean,
      nodeIntegration: false
    },
    icon: path.join(__static, '64X64.png')
  })

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    win.loadURL(process.env.WEBPACK_DEV_SERVER_URL as string)
    if (!process.env.IS_TEST) win.webContents.openDevTools()
  } else {
    createProtocol('app')
    // Load the index.html when not in development
    // win.loadURL('app://./index.html')
    win.loadURL('http://www.xnjx666.com:9000/#/login')
    // win.loadURL('http://192.168.0.108:8099/#/login')
  }
  //win.webContents.openDevTools()

  ipcMain.on('logout', () => {
    console.log('this loagou')
  })
  ipcMain.on('openBrower', () => {
    // console.log('open')
    shell.openExternal('http://www.xnjx666.com/vip')
  })

  ipcMain.on('goBack', () => {
    console.log('go back')
    if (win.webContents.canGoBack()) {
      win.webContents.webContents.goBack()
      // console.log(win.webContents.goBack)
    }
  })
  ipcMain.on('getClientInfo', () => {
    sendNetworkInfo()
    setInterval(() => {
      sendNetworkInfo()
    }, 30 * 60 * 1000)
  })
  win.on('close', (e: any) => {
    if (isUpdate) {
      app.exit()
      win = null
    } else {
      e.preventDefault()
      win.hide()
      // win.minimize()
      // setTray()
    }

    // dialog
    //   .showMessageBox(win as BrowserWindow, {
    //     type: 'info',
    //     title: '关闭',
    //     noLink: true,
    //     defaultId: 0,
    //     cancelId: 2,
    //     message: '确定要关闭吗？',
    //     buttons: ['直接退出', '最小化'],
    //   })
    //   .then((index) => {
    //     if (index.response === 0) {
    //       win = null
    //       app.exit()
    //     } else if (index.response === 1) {
    //       win.minimize()
    //       setTray()
    //     } else {
    //       return
    //     }
    //   })
    // win = null
  })
  win.on('ready-to-show', function() {
    // setTimeout(() => {
    //   win.maximize()
    //   win.show() // 初始化后再显示
    // }, 200)
    win.maximize()
    win.show() // 初始化后再显示
  })
  // win.maximize()
  // win.show() // 初始化后再显示
}

function sendNetworkInfo() {
  const networkInterfaces = os.networkInterfaces()

  win.webContents.send('sendNetworkInfo', networkInterfaces)
}

// updateHandle()

const gotTheLock = app.requestSingleInstanceLock()
if (!gotTheLock) {
  app.quit()
} else {
  app.on('second-instance', event => {
    if (win) {
      if (win.isMinimized()) win.restore()
      win.focus()
    }
  })
  // app.on('ready', () => {
  //   createWindow()
  //   const { Menu } = require('electron')
  //   Menu.setApplicationMenu(null) // 隐藏菜单栏
  // })
  app.commandLine.appendSwitch('--disable-http-cache') //禁止缓存
  app.on('ready', async () => {
    const { Menu } = require('electron')
    Menu.setApplicationMenu(null) // 隐藏菜单栏
    if (isDevelopment && !process.env.IS_TEST) {
      // Install Vue Devtools
      try {
        await installExtension(VUEJS_DEVTOOLS)
      } catch (e) {
        console.error('Vue Devtools failed to install:', e.toString())
      }
    }
    setTray()
    createWindow()
    updateHandle()
  })
}

// Quit when all windows are closed.
app.on('window-all-closed', (e: any) => {
  e.preventDefault()
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})

if (isWin7) {
  app.disableHardwareAcceleration()
}
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
// app.on('ready', async () => {
//   if (isDevelopment && !process.env.IS_TEST) {
//     // Install Vue Devtools
//     try {
//       await installExtension(VUEJS_DEVTOOLS)
//     } catch (e) {
//       console.error('Vue Devtools failed to install:', e.toString())
//     }
//   }
//   createWindow()
// })

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', data => {
      if (data === 'graceful-exit') {
        app.quit()
      }
    })
  } else {
    process.on('SIGTERM', () => {
      app.quit()
    })
  }
}
let appTray: any // 引用放外部，防止被当垃圾回收

// 隐藏主窗口，并创建托盘，绑定关闭事件
function setTray() {
  // 用一个 Tray 来表示一个图标,这个图标处于正在运行的系统的通知区
  //  ，通常被添加到一个 context menu 上.
  // const Menu = electron.Menu
  // const Tray = electron.Tray
  // // 系统托盘右键菜单
  let trayMenuTemplate = [
    {
      // 系统托盘图标目录
      label: '退出',
      click: function() {
        // app.quit()
        app.exit()
        win = null
      }
    },
    {
      label: '显示界面',
      click: function() {
        win.maximize()
        win.show()
      }
    }
  ]
  // 当前目录下的app.ico图标
  let iconPath = path.join(__static, 'win10.png')
  appTray = new Tray(iconPath)
  // 图标的上下文菜单
  const contextMenu = Menu.buildFromTemplate(trayMenuTemplate)
  // 隐藏主窗口
  // win.hide()
  // 设置托盘悬浮提示
  appTray.setToolTip('小牛建讯')
  // 设置托盘菜单
  appTray.setContextMenu(contextMenu)
  // 单击托盘小图标显示应用
  appTray.on('click', function() {
    // 显示主程序
    win.maximize()
    win.show()
    // 关闭托盘显示
    // appTray.destroy()
  })
}
