declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}
interface Window {
  CKEDITOR: any
  ipcRenderer: any
  electronSession: any
}
declare let copyThisText: Function
declare let apiUrl: string
declare let VUE_APP_TYPE: string
declare let BASEURL: string
declare module 'jsencrypt'
declare module 'js-cookie'
declare module '@riophae/vue-treeselect'
declare module '__static'
declare module 'ant-design-vue/lib/locale-provider/zh_CN.js'
declare module '*.svg'
declare module '*.png'
declare module '*.jpg'
declare module '*.jpeg'
declare module '*.gif'
declare module '*.bmp'
declare module '*.tiff'
declare module 'jsonp'
declare const __static: string
declare const QRCode: any
declare module '@/components/designQualification.js'
declare module '@/utils/area.js'
