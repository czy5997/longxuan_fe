export const hide = (total, pageNum) => {
  const totalNum = Math.ceil(total / 10)
  window.document.querySelector('.ant-pagination-jump-next')
    ? (window.document.querySelector(
        '.ant-pagination-jump-next'
      ).style.display = 'none')
    : ''
  if (totalNum >= 10) {
    window.document.querySelector(`.ant-pagination-item-${6}`)
      ? (window.document.querySelector(
          `.ant-pagination-item-${6}`
        ).style.display = 'inline-block')
      : ''
    window.document.querySelector(`.ant-pagination-item-${7}`)
      ? (window.document.querySelector(
          `.ant-pagination-item-${7}`
        ).style.display = 'inline-block')
      : ''
    window.document.querySelector(`.ant-pagination-item-${8}`)
      ? (window.document.querySelector(
          `.ant-pagination-item-${8}`
        ).style.display = 'inline-block')
      : ''
    window.document.querySelector(`.ant-pagination-item-${9}`)
      ? (window.document.querySelector(
          `.ant-pagination-item-${9}`
        ).style.display = 'inline-block')
      : ''
    if (pageNum >= totalNum - 3) {
      window.document.querySelector(`.ant-pagination-item-${totalNum}`)
        ? (window.document.querySelector(
            `.ant-pagination-item-${totalNum}`
          ).style.display = 'inline-block')
        : ''
    } else {
      window.document.querySelector(`.ant-pagination-item-${totalNum}`)
        ? (window.document.querySelector(
            `.ant-pagination-item-${totalNum}`
          ).style.display = 'none')
        : ''
    }
  } else {
    if (pageNum > 5 && pageNum <= totalNum) {
      window.document.querySelector(`.ant-pagination-item-${pageNum}`)
        ? (window.document.querySelector(
            `.ant-pagination-item-${pageNum}`
          ).style.display = 'inline-block')
        : ''
    } else {
      window.document.querySelector(`.ant-pagination-item-${6}`)
        ? (window.document.querySelector(
            `.ant-pagination-item-${6}`
          ).style.display = 'none')
        : ''
      window.document.querySelector(`.ant-pagination-item-${7}`)
        ? (window.document.querySelector(
            `.ant-pagination-item-${7}`
          ).style.display = 'none')
        : ''
      window.document.querySelector(`.ant-pagination-item-${8}`)
        ? (window.document.querySelector(
            `.ant-pagination-item-${8}`
          ).style.display = 'none')
        : ''
      window.document.querySelector(`.ant-pagination-item-${9}`)
        ? (window.document.querySelector(
            `.ant-pagination-item-${9}`
          ).style.display = 'none')
        : ''
    }
  }
}

