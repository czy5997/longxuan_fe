import { Button, Icon, Input, Select } from 'ant-design-vue'
import { Component, Vue, Prop } from 'vue-property-decorator'

@Component({
  components: {
    aButton: Button,
    aIcon: Icon,
    aInput: Input,
    aSelect: Select,
    aSelectOption: Select.Option
  }
})
export default class EditableCell extends Vue {
  @Prop() text: any
  @Prop() displayText?: string
  @Prop({
    default() {
      return {
        green: '0',
        yellow: '1',
        blue: '2'
      }
    }
  })
  cssJudge: any

  @Prop({
    default() {
      return [
        { value: '0', text: '未处理' },
        { value: '1', text: '已处理' }
      ]
    }
  })
  statusList: any

  @Prop({ default: true, type: Boolean }) isCanEdit?: boolean

  editable = false
  value = this.text

  handleChange(e: any) {
    // const value = e.target.value;
    // this.value = value;
    // console.log('handleChange')
  }
  check() {
    this.editable = false
    if (this.value === this.text) {
      // console.log('选择未改变')
      return
    } else {
      this.$emit('change', this.value)
    }
  }
  edit() {
    if (!this.isCanEdit) {
      return
    }
    this.value = this.text
    // console.log(this.statusList)
    this.editable = true
  }
}
