import { Component, Vue } from 'vue-property-decorator'
import { userModule } from '@/store/modules/user'
import { homeModule } from '@/store/modules/home'
import logo from '@/assets/svg/logo.svg'
import { Dropdown, Menu, Icon, Carousel, Badge, Drawer } from 'ant-design-vue'
import MesDrawer from '@/views/Message/Index.vue'
@Component({
  components: {
    MesDrawer,
    AIcon: Icon,
    ADropdown: Dropdown,
    AMenu: Menu,
    ACarousel: Carousel,
    AMenuItem: Menu.Item,
    AMenuDivider: Menu.Divider,
    ABadge: Badge,
    ADrawer: Drawer,
  },
})
export default class TopTool extends Vue {
  logo = logo
  visible = false
  get buyVersion() {
    return userModule.buyVersion
  }
  get userName() {
    return userModule.name
  }
  get failureTime() {
    return userModule.failureTime
  }
  get userId() {
    return userModule.userId
  }
  isSuperAdmin = userModule.roleLevel === 1
  companyName = userModule.companyName
  isAdmin = userModule.roleLevel === 2
  isSales = userModule.roleLevel === 4
  isLead = userModule.isLead
  get messageList() {
    return homeModule.messageTotal
      ? (homeModule.messageList as any)
      : (homeModule.readList as any)
  }
  get messageTotal() {
    return homeModule.messageTotal
  }
  goToVip() {
    this.$router.push({
      name: 'vip',
      query: {
        fromUrl: this.$route.name,
      },
    })
  }
  getPopupContainer() {
    return document.getElementById('userInfo')
  }
  clickMenuItem() {
    this.$router.push({
      name: 'changePassword',
      query: {
        pId: '5',
      },
    })
  }
  goToMessageCenter() {
    // this.visible = true
    if (this.$route.name === "messageCenter") {
      return;
    }
    this.$router.push({ name: "messageCenter" });
  }
  onClose() {
    this.visible = false
  }
  //   getAnnouncementList() {
  //     const params = {
  //       currentPage: 1,
  //       pageSize: 1,
  //     }
  //     /* eslint-disable */
  //     this.$httpRequester.get('notice/selManagerList', params).then((r: any) => {
  //       this.messageList = r.data
  //     })
  //   }
  logOut() {
    this.$httpRequester.post('user/logout').then(() => {
      localStorage.removeItem('userId')
      localStorage.removeItem(this.userId.toString())
      this.$router.push('/login')
    })
  }
  async mounted() {
    homeModule.getNotReadStatus()
    // homeModule.getReadMessageList()
  }
}
