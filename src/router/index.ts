import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import App from '../App.vue'
import { MetaInfo, MetaInfoComputed } from 'vue-meta/types/vue-meta'

declare module 'vue/types/options' {
  interface ComponentOptions<V extends Vue> {
    metaInfo?: MetaInfo | MetaInfoComputed | undefined
  }
}
Vue.use(VueRouter)
const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: App,
    // redirect: '/longna/shipment',
    children: [
      {
        // 全部是小牛的文件
        path: 'longna',
        component: () =>
          import(/* webpackChunkName: "longna" */ '../views/Layout.vue'),
        children: [
          {
            path: '/cannotfind',
            name: 'page404',
            component: () =>
              import(/* webpackChunkName: "login" */ '../views/404.vue')
          },
          {
            path: 'receipt',
            name: 'receipt',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/Receipt/Index.vue'
              ),
            meta: { title: '收货查询', needAuth: false }
          },
          {
            path: 'detail/:id',
            name: 'detail',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/Detail/Index.vue'
              ),
            meta: { title: '公司详情', needAuth: false }
          },
          {
            path: 'client',
            name: 'client',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/Client/Index.vue'
              ),
            meta: { title: '我的客户', needAuth: true },
            children: [
              {
                path: 'processing',
                name: 'processing',
                component: () =>
                  import(
                    /* webpackChunkName: "longna" */ '../views/Client/MyClient.vue'
                  ),
                meta: { title: '跟进中客户', needAuth: false }
              },
              {
                path: 'history',
                name: 'clientHistory',
                component: () =>
                  import(
                    /* webpackChunkName: "longna" */ '../views/Client/MyHistory.vue'
                  ),
                meta: { title: '历史记录', needAuth: false }
              }
            ]
          },
          {
            path: 'shipment',
            name: 'shipment',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/Shipment/Index.vue'
              ),
            meta: { title: '出货查询', needAuth: false }
          },
          {
            path: 'followUp',
            name: 'followUp',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/FollowUp/Index.vue'
              ),
            meta: { title: '跟进资源', needAuth: false }
          },
          {
            path: 'message',
            name: 'message',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/Message/Index.vue'
              ),
            meta: { title: '消息中心', needAuth: false }
          },
          {
            path: 'IntermediaryResources',
            name: 'IR',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/IntermediaryResources/Index.vue'
              ),
            meta: { title: '资质服务商', needAuth: false }
          },
          {
            path: 'Notice',
            name: 'Notice',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/Notice/Index.vue'
              ),
            meta: { title: '公示公告', needAuth: false }
          },
          {
            path: 'knowledgeWarehouse',
            name: 'KW',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/KnowledgeWarehouse/Index.vue'
              ),
            children: [
              {
                path: 'knowledgeWarehouseHomePage',
                name: 'knowledgeWarehouseHomePage',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/KnowledgeWarehouse/HomePage.vue'
                  ),
                meta: { title: '知识库', needAuth: false }
              },
              {
                path: 'policyDocList',
                name: 'policyDocList',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/KnowledgeWarehouse/PolicyDocList.vue'
                  ),
                meta: { title: '政策文件', needAuth: false }
              },
              {
                path: 'QueAndQnsList',
                name: 'QueAndQnsList',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/KnowledgeWarehouse/QueAndQnsList.vue'
                  ),
                meta: { title: '资质问答', needAuth: false }
              },
              {
                path: 'PolicyDocDetails',
                name: 'PolicyDocDetails',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/KnowledgeWarehouse/PolicyDocDetails.vue'
                  ),
                meta: { title: '知识库', needAuth: false }
              },
              {
                path: 'QueAndQnsDetails',
                name: 'QueAndQnsDetails',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/KnowledgeWarehouse/QueAndQnsDetails.vue'
                  ),
                meta: { title: '知识库', needAuth: true }
              }
            ]
          },
          {
            path: 'userCenter',
            name: 'userCenter',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/UserCenter/UserCenterLayout.vue'
              ),
            meta: { title: '用户中心', needAuth: false },
            children: [
              {
                path: 'basicInfo',
                name: 'UCBasicInfo',
                component: () =>
                  import(
                    /* webpackChunkName: "longna" */ '../views/UserCenter/BasicInfo.vue'
                  ),
                meta: { title: '基本信息', needAuth: false }
              },
              {
                path: 'memberOrder',
                name: 'memberOrder',
                component: () =>
                  import(
                    /* webpackChunkName: "longna" */ '../views/Setting/MemberOrder.vue'
                  ),
                meta: { title: '会员订单', needAuth: false }
              },
              {
                path: 'SellDetail',
                name: 'mySellDetail',
                component: () =>
                  import(
                    /* webpackChunkName: "longna" */ '../views/QR/Shared/Detail.vue'
                  ),
                meta: { title: '发布详情', needAuth: false }
              },
              {
                path: 'messageCenter',
                name: 'messageCenter',
                component: () =>
                  import(
                    /* webpackChunkName: "longna" */ '../views/Message/Index.vue'
                  ),
                meta: { title: '消息中心', needAuth: false }
              },
              {
                path: 'companyCount',
                name: 'companyCount',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/Setting/CompanyCount.vue'
                  ),
                meta: { title: '次数使用情况', needAuth: false }
              },
              {
                path: 'downloadSetting',
                name: 'downloadSetting',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/Setting/DownloadSetting.vue'
                  ),
                meta: { title: '系统配置', needAuth: false }
              },
              {
                path: 'softInfo',
                name: 'softInfo',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/UserCenter/SoftInfo.vue'
                  ),
                meta: { title: '关于小牛', needAuth: false }
              },
              {
                path: 'changePassword',
                name: 'changePassword',
                component: () =>
                  import(
                    /* webpackChunkName: "longna" */ '../views/ChangePassword.vue'
                  ),
                meta: { title: '修改密码', needAuth: false }
              }
            ]
          },
          {
            path: 'setting',
            name: 'setting',
            component: () =>
              import(
                /* webpackChunkName: "setting" */ '../views/Setting/Index.vue'
              ),
            children: [
              {
                path: 'user',
                name: 'userSetting',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/Setting/User.vue'
                  ),
                meta: { title: '用户管理', needAuth: true }
              },
              {
                path: 'department',
                name: 'department',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/Setting/Department.vue'
                  ),
                meta: { title: '部门管理', needAuth: true }
              },
              {
                path: 'menu',
                name: 'menuSetting',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/Setting/Menu.vue'
                  ),
                meta: { title: '菜单管理', needAuth: true }
              },
              {
                path: 'announcement',
                name: 'announcementSetting',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/Setting/Announcement.vue'
                  ),
                meta: { title: '公告管理', needAuth: true }
              },
              {
                path: 'advice',
                name: 'adviceSetting',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/Setting/Advice.vue'
                  ),
                meta: { title: '意见反馈', needAuth: true }
              },

              {
                path: 'enterprise',
                name: 'EnterpriseFocusStatistics',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/Setting/EnterpriseFocusStatistics.vue'
                  ),
                meta: { title: '企业关注统计', needAuth: true }
              },
              {
                path: 'Role',
                name: 'roleSetting',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/Setting/Role.vue'
                  ),
                meta: { title: '角色管理', needAuth: true }
              },
              {
                path: 'quote',
                name: 'quoteSetting',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/Setting/Quote.vue'
                  ),
                meta: { title: '企业股权报价', needAuth: true }
              },
              {
                path: 'organization',
                name: 'organizationSetting',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/Setting/Organization.vue'
                  ),
                meta: { title: '组织管理', needAuth: true }
              },
              {
                path: 'operalog',
                name: 'operalogSetting',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/Setting/OperaLog.vue'
                  ),
                meta: { title: '操作日志', needAuth: true }
              },
              {
                path: 'certification',
                name: 'enterpriseCertification',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/Setting/Certification.vue'
                  ),
                meta: { title: '企业认证', needAuth: true }
              },
              {
                path: 'advManage',
                name: 'advManageSetting',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/Setting/AdvManage.vue'
                  ),
                meta: { title: '广告管理', needAuth: true }
              },
              {
                path: 'selfReleaseNeeds',
                name: 'selfReleaseNeedsSetting',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/Setting/selfRelease.vue'
                  ),
                meta: { title: '自营发布需求', needAuth: true }
              },
              {
                path: 'dynamic',
                name: 'dynamic',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/Setting/OfficialWebsite/Dynamic.vue'
                  ),
                meta: { title: '小牛动态', needAuth: true }
              },
              {
                path: 'recommendQualifications',
                name: 'recommendQualifications',
                component: () =>
                  import(
                    /* webpackChunkName: "setting" */ '../views/Setting/OfficialWebsite/RecommendQualifications.vue'
                  ),
                meta: { title: '推荐资质', needAuth: true }
              },
              {
                path: 'detail',
                name: 'certificationDetail',
                component: () =>
                  import(
                    /* webpackChunkName: "longna" */ '../views/QR/Shared/Detail.vue'
                  ),
                meta: { title: '发布详情', needAuth: false }
              },
              {
                path: 'paltformShare',
                name: 'paltformShare',
                component: () =>
                  import(
                    /* webpackChunkName: "longna" */ '../views/QR/Shared/ExcellentShare.vue'
                  ),
                meta: { title: '发布资源', needAuth: false }
              }
            ]
          },
          // 破产企业
          {
            path: 'bankrupt',
            name: 'bankrupt',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/bankrupt/Index.vue'
              )
          },
          {
            path: 'bankruptDetail',
            name: 'bankruptDetail',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/bankrupt/Detail.vue'
              )
          }
        ],
        meta: { type: 'zizhi' }
      },
      {
        // 这是人才
        path: 'talents',
        component: () =>
          import(
            /* webpackChunkName: "longna" */ '../views/TalentsPages/Layout.vue'
          ),
        children: [
          {
            path: 'gaps',
            name: 'gaps',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/TalentsPages/TalentsGaps/Index.vue'
              )
          },
          {
            path: 'talentsDetail',
            name: 'talentsDetail',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/TalentsPages/TalentsGaps/Detail.vue'
              ),
            meta: { belong: 'gaps' }
          },
          {
            path: 'myClient',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/TalentsPages/MyClient/ClientIndex.vue'
              ),
            children: [
              {
                path: 'followUpEnterprise',
                name: 'followUpEnterprise',
                component: () =>
                  import(
                    /* webpackChunkName: "longna" */ '../views/TalentsPages/MyClient/FollowUp.vue'
                  )
              },
              {
                path: 'historicalRecords',
                name: 'historicalRecords',
                component: () =>
                  import(
                    /* webpackChunkName: "longna" */ '../views/TalentsPages/MyClient/HistoricalRecords.vue'
                  )
              }
            ]
          }
        ],
        meta: { type: 'rencai' }
      },
      {
        // 这是股权
        path: 'shareholder',
        component: () =>
          import(
            /* webpackChunkName: "longna" */ '../views/ShareholderPage/Layout.vue'
          ),
        children: [
          {
            path: 'sell',
            name: 'sell',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/ShareholderPage/Sell/Index.vue'
              )
          },
          {
            path: 'buy',
            name: 'buy',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/ShareholderPage/buy/Index.vue'
              )
          },
          {
            path: 'myBuying',
            name: 'myBuying',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/ShareholderPage/myBuy/MyBuyResources.vue'
              ),
            meta: { title: '我求购的资源', needAuth: false }
          },
          {
            path: 'mySell',
            name: 'mySell',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/ShareholderPage/mySell/MySellResources.vue'
              ),
            meta: { title: '我出售的资源', needAuth: false }
          },
          {
            path: 'mySell/excellentShare',
            name: 'excellentShare',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/ShareholderPage/mySell/ExcellentShare.vue'
              ),
            meta: { title: '发布资源', needAuth: false }
          },
          {
            path: 'myBuying/editBuyingInfo',
            name: 'editBuyingInfo',
            component: () =>
              import(
                /* webpackChunkName: "longna" */ '../views/ShareholderPage/myBuy/Edit.vue'
              ),
            meta: { title: '发布求购信息', needAuth: false }
          }
        ],
        meta: { type: 'shareholder' }
      }
    ]
  },
  // route level code-splitting
  // this generates a separate chunk (about.[hash].js) for this route
  {
    path: '/login',
    name: 'login',
    component: () =>
      import(/* webpackChunkName: "login" */ '../views/Login.vue'),
    meta: { title: '小牛建讯', needAuth: false },
    children: [
      {
        path: '/registered',
        name: 'registered',
        component: () =>
          import(
            /* webpackChunkName: "login" */ '../views/Registered/Index.vue'
          ),
        meta: { title: '小牛建讯', needAuth: false }
      }
    ]
  },
  {
    path: '/vip',
    name: 'vip',
    component: () => import(/* webpackChunkName: "login" */ '../views/Vip.vue'),
    meta: { title: '充值vip', needAuth: false }
  },

  {
    path: '/enterprise/share',
    name: 'share',
    component: () =>
      import(/* webpackChunkName: "login" */ '../views/Mobile/Share.vue'),
    meta: { title: '小牛建讯', needAuth: false }
  }
  // {
  //   path: '*',
  //   redirect: '/cannotfind',
  // }
]

const router = new VueRouter({
  routes,
  scrollBehavior(to: any, from: any, savedPosition: any) {
    return { x: 0, y: 0 }
  }
})

export default router
