import qs from 'qs'
import { Interceptors } from './interceptors'
import { message, Modal } from 'ant-design-vue'
import router from '@/router/index'
import { detailModule } from '@/store/modules/detail'

export class HttpRequester {
  public axios: any
  public modal: any

  constructor() {
    // 获取axios实例ss
    this.axios = new Interceptors().getInterceptors()
  }

  /**
   * get请求
   * @param params  参数
   * @param url   地址
   * @param jwt   是否token校验
   */
  public get(url: string, params: any = {}, jwt = true) {
    for (const key in params) {
      if (Object.prototype.hasOwnProperty.call(params, key)) {
        const element = params[key] as any
        if (typeof element === 'string') {
          params[key] = params[key].trim()
        }
      }
    }
    return new Promise((resolve, reject) => {
      this.axios
        .get(url, {
          params,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        })
        .then((res: any) => {
          this.resultHandle(res, resolve, reject)
        })
        .catch((err: any) => {
          reject(err.message || err)
        })
    })
  }

  public download(url: string, params = {}, jwt = true) {
    return new Promise((resolve, reject) => {
      this.axios
        .get(url, {
          params,
          headers: { 'Content-Type': 'application/json' },
          responseType: 'blob'
        })
        .then((res: any) => {
          const _that = this
          if (res.type === 'application/json') {
            const reader = new FileReader() as any
            reader.readAsText(res, 'utf-8')
            reader.onload = function() {
              const jsondata = JSON.parse(reader.result)
              _that.resultHandle(jsondata, resolve, reject)
            }
          } else {
            resolve(res)
          }
        })
        .catch((err: any) => {
          reject(err.message || err)
        })
    })
  }

  /**
   * post请求
   * @param url   地址
   * @param params  参数
   * @param jwt   是否token校验
   */
  public post(
    url: string,
    params: any = {},
    dataFormat = 'application/json',
    jwt = true
  ) {
    for (const key in params) {
      if (Object.prototype.hasOwnProperty.call(params, key)) {
        const element = params[key] as any
        if (typeof element === 'string') {
          params[key] = params[key].trim()
        }
      }
    }
    let data = {}
    if (dataFormat === 'application/x-www-form-urlencoded') {
      data = qs.stringify(params)
    } else {
      data = params
    }
    return new Promise((resolve, reject) => {
      this.axios
        .post(url, data, {
          headers: { 'Content-Type': dataFormat }
        })
        .then((res: any) => {
          this.resultHandle(res, resolve, reject)
        })
        .catch((err: any) => {
          reject(err.message || err)
        })
    })
  }

  /**
   * post请求
   * @param url   地址
   * @param params  参数
   * @param jwt   是否token校验
   */
  public upload(
    url: string,
    params = {},
    dataFormat = 'multipart/form-data;charset=UTF-8',
    jwt = true
  ) {
    let data = {}
    if (dataFormat === 'application/x-www-form-urlencoded') {
      data = qs.stringify(params)
    } else {
      data = params
    }
    return new Promise((resolve, reject) => {
      this.axios
        .post(url, data, {
          headers: { 'Content-Type': dataFormat }
        })
        .then((res: any) => {
          this.resultHandle(res, resolve, reject)
        })
        .catch((err: any) => {
          reject(err.message || err)
        })
    })
  }

  public put(
    url: string,
    params = {},
    dataFormat = 'application/json',
    jwt = true
  ) {
    let data = {}
    if (dataFormat === 'application/x-www-form-urlencoded') {
      data = qs.stringify(params)
    } else {
      data = params
    }
    return new Promise((resolve, reject) => {
      this.axios
        .put(url, data, {
          headers: { 'Content-Type': dataFormat }
        })
        .then((res: any) => {
          this.resultHandle(res, resolve, reject)
        })
        .catch((err: any) => {
          reject(err.message || err)
        })
    })
  }

  public delete(
    url: string,
    params = {},
    dataFormat = 'application/json',
    jwt = true
  ) {
    let data = {}
    if (dataFormat === 'application/x-www-form-urlencoded') {
      data = qs.stringify(params)
    } else {
      data = params
    }
    return new Promise((resolve, reject) => {
      this.axios
        .delete(url, {
          data,
          headers: { 'Content-Type': dataFormat }
        })
        .then((res: any) => {
          this.resultHandle(res, resolve, reject)
        })
        .catch((err: any) => {
          reject(err.message || err)
        })
    })
  }

  /**
   *
   * @param res
   * @param resolve
   */
  public resultHandle(res: any, resolve: any, reject: any) {
    if (parseInt(res.code, 10) === 200 || !res.code) {
      // console.log()
      resolve(res.data)
    } else if (
      parseInt(res.code, 10) === 2001 ||
      parseInt(res.code, 10) === 2011
    ) {
      router.push('/login')
      message.warn(res.msg)
      reject({ notLogin: true })
    } else if (parseInt(res.code, 10) === 3008) {
      detailModule.setVerification(true)
      return
    } else if (parseInt(res.code, 10) === 2005) {
      detailModule.setExpriedModalShow(true)
      return
    } else {
      this.errorHandle(res, reject)
    }
  }

  /**
   * 服务端状态处理,例如中断性异常,退出异常等等(与拦截器http握手状态注意区分,一般都能分清楚吧)
   * @param res
   */
  public errorHandle(res: any, reject: any) {
    const specialCode = [
      5003,
      5004,
      5005,
      5013,
      5010,
      5012,
      2007,
      2006,
      3008,
      5017
    ]

    const showModalCode = [5008, 2005, 5029]

    const activityCode = [
      5014,
      5015,
      5017,
      5018,
      5019,
      5020,
      5021,
      5022,
      5024,
      5025,
      5026,
      5028,
      5030,
      5031,
      5032,
    ]
    if (parseInt(res.code, 10) === 4004) {
      reject('noualification')
      return
    }
    if (
      specialCode.find((e: any) => {
        return e === parseInt(res.code, 10)
      })
    ) {
      reject(res)
      return
    }

    if (showModalCode.includes(parseInt(res.code))) {
      let title = ''
      let okText = ''
      if (parseInt(res.code) === 2005) {

        title = '你的VIP已到期'
        okText = '我要升级'
      } else if (parseInt(res.code) === 5008) {
        title = '领取次数已用完'
        okText = '我要升级'
      } else {
        title = '领取次数已用完'
        okText = '好的'
      }

      Modal.info({
        title,
        content: res.msg,
        okText,
        onOk: () => {
          window.open("http://www.xnjx666.com/vip");
        }
      })
      return
    }

    // 处理活动状态码
    if (activityCode.includes(parseInt(res.code))) {
        if (res.code == 5014 ||
            res.code == 5015 ||
            res.code == 5024 ||
            res.code == 5028 ||
            res.code == 5019 ||
            res.code == 5020 ||
            res.code == 5021 ||
            res.code == 5022 ||
            res.code == 5030 ||
            res.code == 5031 ||
            res.code == 5032) {
                console.log(res, 'resresresres')
                detailModule.setActivityContent(res)
                detailModule.setGoToActivityModalShow(true)
            }
            reject(res)
        return
    }

    if (res.msg.indexOf('BadPaddingException') > -1) {
      location.reload()
      return
    }
    message.warn(res.msg) // 统一弹服务端提示,我们提示统一由服务端提供
    // 状态码判断
    switch (res.status) {
      case -102:
        break
      case -152:
        break
      default:
      // console.log(other);
    }
    reject(res)
  }
}
