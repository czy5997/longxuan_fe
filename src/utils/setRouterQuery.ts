import router from '@/router/index'

export class SetQuery {
  public route: any
  public query: any
  public addQuery(route: any, query: any) {
    this.route = route
    this.query = query
    if (this.query) {
      let path = ''
      for (const key in this.query) {
        if (Object.prototype.hasOwnProperty.call(this.query, key)) {
          const queryItem = this.query[key]
          path += `${key}=${queryItem}&`
        }
      }
      router.replace('?' + path)
    } else {
      const path = this.route.path
      router.replace(path)
    }
  }
}
