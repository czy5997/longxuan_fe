/**
 * 验证给定的日期是否在指定的天数内
 * @param {*} time // 需要验证的日期
 * @param {*} day // 指定的天数
 * @returns 是否在指定范围
 */
export const isTimeBeyondExpectation = (time = '', day = 7) => {
  if (!time) return false
  // 处理需要验证的日期
  const timed = new Date(time.replace(/-/g, '/')).getTime()
  // 当前时间
  const nowTime = new Date().getTime()
  // 指定天数
  const dayTamp = Number(day) * 60 * 60 * 24 * 1000
  // 时间差
  const badTamp = nowTime - dayTamp

  if (badTamp < timed) {
    return true
  } else {
    return false
  }
}
