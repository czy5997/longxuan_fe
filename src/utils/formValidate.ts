export class FormValidate {
  public form: any
  public propsArr: any
  public listValidata(form: any, props: any, func: any) {
    this.form = form
    if (Object.prototype.toString.call(props) === '[object Array]') {
      // 判断是否为数组
      this.propsArr = props
    } else {
      this.propsArr = Object.keys(props) as []
    }
    const isValuidate = this.formItemValidata(this.propsArr[0]) // 主要就是通过循环查询属性数组在表单里面的验证情况
    if (!isValuidate && this.propsArr.length > 0) {
      this.propsArr.splice(0, 1) // 删除第一个
      this.listValidata(this.form, this.propsArr, func)
    }
    if (!this.propsArr.length && isValuidate) {
      func(true)
    }
    // propsArr.some((e: any) => {
    //   // console.log(e)
    //   let err = false
    //   form.validateField(e, (val: any) => {
    //     err = !!val
    //   })
    //   // console.log(err)
    //   return err
    // })
  }
  private formItemValidata(prop: any) {
    // console.log(prop)
    let err = true
    this.form.validateField(prop, (val: any) => {
      err = !!val
    })
    return err
  }
}
