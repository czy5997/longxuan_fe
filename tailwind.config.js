module.exports = {
  mode: 'jit',
  purge: [
    './src/**/*.html',
    './src/**/*.{vue,js,ts,jsx,tsx}',
    './src/**/*/*.{vue,js,ts,jsx,tsx}'
  ],
  darkMode: false,
  theme: {
    extend: {
      colors: {
        tipBlue: '#1890FF',
        tipOrange: '#ff7f28',
        textPrimary: '#282828',
        textLight: '#686868',
        sysPrimary: '#00ab94',
        textTip: '#A0A6AB',
        textClicked: '#a2a2a2',
        bgLight: '#f3f6f8'
      }
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
}
